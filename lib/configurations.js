/**
 * GLOBAL CONFIGURATION SYSTEM
 * all configuration will reflect to app itself
 */

module.exports = {
    MODULE_NAME:"Biomark",
    SOURCE:"./src/",
    DEVELOPMENT:"./release/development/",
    PRODUCTION:"./release/production/",
    STAGING:{
        dev:{
            api:"https://bm-dev-api.biomarking.com/",
            socket:"wss://bm-dev-api.biomarking.com/cable"
        },
    }
};