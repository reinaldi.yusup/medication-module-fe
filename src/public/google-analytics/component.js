(function(){

    "use strict";

    angular
      .module("Biomark")
      .component('googleAnalytics',{
          templateUrl:"/public/google-analytics/view.html",
          controller:"googleAnalyticsController",
        })
})();