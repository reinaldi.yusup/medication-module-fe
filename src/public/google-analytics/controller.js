(function(){

    "use strict";
    
    angular
      .module("Biomark")
      .controller("googleAnalyticsController", googleAnalyticsController);

      googleAnalyticsController.$inject=[];
      
      function googleAnalyticsController(){
          this.$onInit = function(){
          this.portal_type = '%PORTAL_TYPE%';

          switch(this.portal_type) {
              case 'innoquest':
                  this.g_src = "/public/google-analytics/innoquest.html"
                  break;
                  default:
                      this.g_src = "/public/google-analytics/gribbles.html"
                      break;
           }
          }
      }
})();