(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("patientFormModal",{
            bindings:{
                request:"=",
                closeModal:"=",
                isEdit:"=",
                editData:"=",
                patientId:"="
            },
            templateUrl:"/public/patient-form-modal/view.html",
            controller:"patientFormModalController"
          },
        )
})();