(function(){
  "use strict";

  angular
    .module("Biomark")
    .controller("patientFormModalController",patientFormModalController);

    patientFormModalController.$inject = ["HttpServer", "EOrderService", "$timeout", "PopupModalService","$state","$scope"];

    function patientFormModalController(HttpServer, EOrderService, $timeout, PopupModalService, $state, $scope){
      var vm = this;
      vm.firstNameError = "";
      vm.lastNameError = "";
      vm.addressError = "";
      vm.postalCodeError = "";
      vm.contactNumberError = "";
      vm.mobileNumberError = "";
      vm.emailError = "";
      vm.occupationError = "";
      vm.nextOfKinNameError = "";
      vm.dobError = "";
      vm.expiryDateError = "";
      vm.payorNameError = "";
      vm.policyNumberError = "";

      vm.genderFlag = false;
      vm.dobFlag = false;
      vm.eDateFlag = false;
      vm.nationalityFlag = false;
      vm.identificationTypeFlag = false;
      vm.maritalStatusFlag = false;
      vm.languageFlag = false;
      vm.mobileNumberFlag = false;
      vm.contactNumberFlag = false;

      vm.result_sent_modal = false;
      vm.hideNextButton = false;
      vm.payorSelfPay = "Self Pay";
      vm.payorInsurer = "Insurer";
      vm.payorOthers = "Others";

      vm.countries = [];

      vm.hideModal = function() {
        vm.patient_data = {};
        console.log("vm.patient_data.bith_date -> "+vm.patient_data.birth_date);
        vm.contactNumberCode = "";
        vm.contactNumber = "";
        vm.mobileNumberCode = "";
        vm.mobileNumber = "";
        vm.setValid();
        vm.result_sent_modal = false;
        vm.hideNextButton = false;
        PopupModalService.patient_form_modal_state(false, {})
      }

      vm.backButton = function() {
        vm.hideNextButton = false;
      }

      vm.email_regex = new RegExp("^[a-zA-Z0-9._-]+[@][a-zA-Z0-9.-]+[.][a-zA-Z]{2,4}$");
      vm.character_regex = new RegExp("^[a-zA-Z-\/@ ]{1,50}$");
      vm.number_regex = new RegExp("^[0-9]{7,13}$");
      vm.postal_code_regex = new RegExp("^[0-9]{1,6}$");
      vm.address_regex = new RegExp("^[a-zA-Z0-9 $&+,:;=?@#|'<>.^*()%!-\/]{5,350}$");
      vm.policy_number_regex = new RegExp("^[a-zA-Z0-9\-]{1,25}$");

      vm.dateOptions = {
        maxDate: new Date()
      }

      vm.expiry_dateOptions = {
        minDate: new Date()
      }

      vm.checkFirstName = function() {
        vm.firstNameValidation = "is-valid";
        if(vm.character_regex.test(vm.patient_data.first_name) === false) {
          vm.firstNameValidation = "is-invalid";
          vm.hideNextButton = false;
          vm.firstNameError = "Please enter alphabets only"
        }
      }

      vm.checkLastName = function() {
        vm.lastNameValidation = "is-valid";
        if(vm.character_regex.test(vm.patient_data.last_name) === false) {
          vm.lastNameValidation = "is-invalid";
          vm.hideNextButton = false;
          vm.lastNameError = "Please enter alphabets only"
        }
      }

      vm.checkAddress = function() {
        vm.addressValidation = "is-valid";
        if(vm.address_regex.test(vm.patient_data.address) === false) {
          vm.addressValidation = "is-invalid";
          vm.hideNextButton = false;
          vm.addressError = "Please enter 5 to 350 alphabets"
        }
      }

      vm.checkPostalCode = function() {
        vm.postalCodeValidation = "is-valid";
        if(vm.postal_code_regex.test(vm.patient_data.postal_code) === false) {
          vm.postalCodeValidation = "is-invalid";
          vm.hideNextButton = false;
          vm.postalCodeError = "Please enter maximum 6 digits only"
        }
      }

      vm.checkContactNumber = function() {
        console.log("vm.contactNumber -> "+vm.contactNumber);
        console.log("vm.contactNumberCode -> "+vm.contactNumberCode);
        vm.contactNumberFlag = false;
        if(vm.contactNumber !== undefined && vm.contactNumberCode === undefined) {
          vm.contactNumberFlag = true;
          vm.hideNextButton = false;
          vm.contactNumberError = "Please select calling code";
        }
        else if(vm.contactNumber === undefined && vm.contactNumberCode !== undefined) {
          vm.contactNumberFlag = true;
          vm.hideNextButton = false;
          vm.contactNumberError = "Please enter contact number";
        }
        else if(vm.number_regex.test(vm.contactNumber) === false) {
          vm.contactNumberFlag = true;
          vm.hideNextButton = false;
          vm.contactNumberError = "Please enter 7 to 13 digits only"
        }
        else {
          vm.patient_data.contact = vm.contactNumberCode +" "+ vm.contactNumber;
          console.log("contact -> "+vm.patient_data.contact);
        }
      }

      vm.checkMobileNumber = function() {
        vm.mobileNumberFlag = false;
        if(vm.number_regex.test(vm.mobileNumber) === false) {
          vm.mobileNumberFlag = true;
          vm.hideNextButton = false;
          vm.mobileNumberError = "Please enter 7 to 13 digits only"
        }
        vm.patient_data.mobile = vm.mobileNumberCode +" "+ vm.mobileNumber;
        console.log("contact -> "+vm.patient_data.mobile);
      }

      vm.checkEmail = function() {
        vm.emailValidation = "is-valid";
        if(vm.email_regex.test(vm.patient_data.email_address) === false) {
          vm.emailValidation = "is-invalid";
          vm.hideNextButton = false;
          vm.emailError = "Please enter valid email"
        }
      }

      vm.checkOccupation = function() {
        vm.occupationValidation = "is-valid";
        if(vm.character_regex.test(vm.patient_data.occupation) === false) {
          vm.occupationValidation = "is-invalid";
          vm.occupationError = "Please enter alphabets only"
        }
      }

      vm.checkKinName = function() {
        vm.nextOfKinNameValidation = "is-valid";
        if(vm.character_regex.test(vm.patient_data.nok) === false) {
          vm.nextOfKinNameValidation = "is-invalid";
          vm.nextOfKinNameError = "Please enter alphabets only"
        }
      }

      vm.checkMaritalStatus = function() {
        console.log("Marital status -> "+vm.maritalStatus);
      }

      vm.checkPayor = function() {
        vm.payorValue = vm.patient_data.payor;
      }

      vm.checkPayorName = function() {
        vm.payorNameValidation = "is-valid";
        if(vm.character_regex.test(vm.patient_data.payor_name) === false) {
          vm.payorNameValidation = "is-invalid";
          vm.result_sent_modal = false;
          vm.payorNameError = "Please enter alphabets only"
        }
      }

      vm.checkPolicyNumber = function() {
        vm.policyNumberValidation = "is-valid";
        if(vm.policy_number_regex.test(vm.patient_data.policy_no) === false) {
          vm.policyNumberValidation = "is-invalid";
          vm.result_sent_modal = false;
          vm.policyNumberError = "Please enter valid policy number";
        }
      }


      // vm.checkDay = function() {
      //   console.log("Days -> "+vm.day);
      //   vm.dobFlag = false;
      //   const currentDate = new Date();
      //   const currentYear = currentDate.getFullYear();
      //   console.log("Current Year -> "+currentYear);
      //   const minYear = currentYear - 1021;
      //   console.log("vm.year -> "+vm.year);
      //   console.log("minYear -> "+minYear);
      //   if(vm.year > currentYear || vm.day < 1 || vm.day > 31 || vm.year < minYear) {
      //     vm.dobFlag = true;
      //     vm.dobError = "Please enter correct date";
      //   }
      // }

      // vm.checkYear = function() {
      //   console.log("Yearssss -> "+vm.year);
      //   vm.checkDay();
      //   vm.patient_data.birth_date = vm.day + "/" + vm.month + "/" + vm.year;
      //   console.log("DOB -> "+vm.patient_data.birth_date);
      //   vm.ageCalculator();
      // }

      // vm.checkExpiryDay = function() {
      //   console.log("Day -> "+vm.eday);
      //   vm.eDateFlag = false;
      //   const currentDate = new Date();
      //   const currentYear = currentDate.getFullYear();
      //   const minYear = currentYear - 1021;
      //   if(vm.eday < 1 || vm.eday > 31 || vm.eyear > currentYear + 5 || vm.eyear < minYear ) {
      //     vm.eDateFlag = true;
      //     vm.expiryDateError = "Please enter correct date";
      //   }
      // }

      // vm.checkExpiryYear = function() {
      //   console.log("Years -> "+vm.eyear);
      //   vm.checkExpiryDay();
      // }

      // vm.checkEhnicity = function() {
      //   console.log("checkEhnicity -> "+vm.patient_data.ethnic);
      // }

      vm.setValid = function() {
        console.log("Set valid");
        vm.firstNameValidation = "is-valid";
        vm.lastNameValidation = "is-valid";
        vm.genderValidation = "is-valid";
        vm.identificationNumberValidation = "is-valid";
        vm.addressValidation = "is-valid";
        vm.postalCodeValidation = "is-valid";
        vm.identificationTypeValidation = "is-valid";
        vm.mobileNumberValidation = "is-valid";
        vm.nationalityValidation = "is-valid";
        vm.emailValidation = "is-valid";

        vm.maritalStatusValidation = "is-valid";
        vm.languageValidation = "is-valid";
        vm.payorValidation = "is-valid";
        vm.selectInsurerValidation = "is-valid";
        vm.payorNameValidation = "is-valid";
        vm.policyNumberValidation = "is-valid";

        vm.identificationTypeFlag = false;
        vm.dobFlag = false;
        vm.mobileNumberFlag = false;
        vm.contactNumberFlag = false;
        vm.nationalityFlag = false;
        vm.eDateFlag = false;
        vm.genderFlag = false;
        vm.maritalStatusFlag = false;
        vm.languageFlag = false;
      }

      vm.getNationalityData = function() {
        console.log("Edit data -> "+JSON.stringify(vm.editData));
        console.log("IsEdit -> "+JSON.stringify(vm.isEdit));
        HttpServer
        .get("admin/clinics/get_countries")
        .response( success);
        function success(success) {
          console.log(success);
          Array.prototype.push.apply(vm.countries, success.countries);
        }
      }

      vm.updatePatient = function() {
        vm.result_sent_modal = true;   
        vm.eDateFlag = false;
        vm.maritalStatusFlag = false;
        vm.languageFlag = false;
        vm.setValid();
        // if(vm.patient_data.martial_status === undefined || vm.patient_data.martial_status === "") {
        //   vm.maritalStatusValidation = "is-invalid";
        //   vm.result_sent_modal = false;
        //   console.log("marital status");
        // }
        if(vm.patient_data.martial_status === undefined || vm.patient_data.martial_status === "" || vm.patient_data.martial_status === null) {
          vm.maritalStatusFlag = true;
          vm.result_sent_modal = false;
        }
        // if(vm.patient_data.language === undefined || vm.patient_data.language === "") {
        //   vm.languageValidation = "is-invalid";
        //   vm.result_sent_modal = false;
        //   console.log("language");
        // }
        if(vm.patient_data.language === undefined || vm.patient_data.language === "" || vm.patient_data.language === null) {
          vm.languageFlag = true;
          vm.result_sent_modal = false;
          console.log("language");
        }
        if(vm.patient_data.payor === undefined || vm.patient_data.payor === "" || vm.patient_data.payor === null) {
          vm.payorValidation = "is-invalid";
          vm.result_sent_modal = false;
          console.log("payor");
        }
        if(vm.patient_data.payor === vm.payorInsurer) {
          if(vm.patient_data.insurer === undefined || vm.patient_data.insurer === "" || vm.patient_data.insurer === null) {
            vm.selectInsurerValidation = "is-invalid";
            vm.result_sent_modal = false;
            console.log("selectInsurer");
          }
          if(vm.patient_data.policy_no === undefined || vm.patient_data.policy_no === "" || vm.patient_data.policy_no === null) {
            vm.policyNumberValidation = "is-invalid";
            vm.result_sent_modal = false;
            vm.policyNumberError = "Please enter policy number";
            console.log("policyNumber");
          }
          if(vm.expiry_date === undefined || vm.expiry_date === "" || vm.expiry_date === null) {
            vm.eDateFlag = true;
            vm.result_sent_modal = false;
            vm.expiryDateError = "Please enter expiry date"
            console.log("Expiry date");
          }else {
            vm.patient_data.policy_expiry_date = vm.expiry_date.getDate() + "/" + ( vm.expiry_date.getMonth() + 1 ) + "/" + vm.expiry_date.getFullYear();
            console.log("Expiry date -> "+vm.patient_data.policy_expiry_date);
          }
          // if(vm.eday === undefined || vm.emonth === undefined || vm.eyear === undefined ||
          //   vm.eday === "" || vm.emonth === "" || vm.eyear === "") {
          //   vm.eDateFlag = true;
          //   vm.result_sent_modal = false;
          //   vm.expiryDateError = "Please enter expiry date"
          //   console.log("Expiry date");
          // }else {
          //  vm.patient_data.policy_expiry_date = vm.eday + "/" + vm.emonth + "/" + vm.eyear;
          //  console.log("Expiry date -> "+vm.patient_data.policy_expiry_date);
          // }
        }
        if(vm.patient_data.payor === vm.payorOthers) {
          if(vm.patient_data.payor_name === undefined || vm.patient_data.payor_name === "" || vm.patient_data.payor_name === null) {
            vm.payorNameValidation = "is-invalid";
            vm.result_sent_modal = false;
            vm.payorNameError = "Please enter payor name";
            console.log("payor name");
          }
          if(vm.patient_data.policy_no === undefined || vm.patient_data.policy_no === "" || vm.patient_data.policy_no === null) {
            vm.policyNumberValidation = "is-invalid";
            vm.result_sent_modal = false;
            vm.policyNumberError = "Please enter policy number";
            console.log("policy number");
          }
          if(vm.expiry_date === undefined || vm.expiry_date === "" || vm.expiry_date === null) {
            vm.eDateFlag = true;
            vm.result_sent_modal = false;
            vm.expiryDateError = "Please enter expiry date"
          }else {
            vm.patient_data.policy_expiry_date = vm.expiry_date.getDate() + "/" + ( vm.expiry_date.getMonth() + 1 ) + "/" + vm.expiry_date.getFullYear();
            console.log("Expiry date -> "+vm.patient_data.policy_expiry_date);
          }
          // if(vm.eday === undefined || vm.emonth === undefined || vm.eyear === undefined ||
          //   vm.eday === "" || vm.emonth === "" || vm.eyear === "") {
          //   vm.eDateFlag = true;
          //   vm.result_sent_modal = false;
          //   vm.expiryDateError = "Please enter expiry date"
          //   console.log("Expiry day");
          // }else {
          //  vm.patient_data.policy_expiry_date = vm.eday + "/" + vm.emonth + "/" + vm.eyear;
          //  console.log("Expiry date -> "+vm.patient_data.policy_expiry_date);
          // }
        }
        console.log("After Result sent modal -> "+vm.result_sent_modal);
        console.log("Patient Data : "+JSON.stringify(vm.patient_data));
        console.log("vm.result_sent_modal -> "+vm.result_sent_modal);
        if(vm.result_sent_modal === true) {
          vm.update_profile();
        }
      }

      vm.addPatient = function() {
        vm.result_sent_modal = true;   
        vm.maritalStatusFlag = false;
        vm.eDateFlag = false;
        vm.languageFlag = false;
        vm.setValid();
        // if(vm.patient_data.martial_status === undefined || vm.patient_data.martial_status === "") {
        //   vm.languageFlag = true;
        //   vm.maritalStatusValidation = "is-invalid";
        //   vm.result_sent_modal = false;
        //   console.log("marital status");
        // }
        if(vm.patient_data.martial_status === undefined || vm.patient_data.martial_status === "" || vm.patient_data.martial_status === null) {
          vm.maritalStatusFlag = true;
          vm.result_sent_modal = false;
          console.log("marital status");
        }
        // if(vm.patient_data.language === undefined || vm.patient_data.language === "") {
        //   vm.languageFlag = true;
        //   vm.languageValidation = "is-invalid";
        //   vm.result_sent_modal = false;
        //   console.log("language");
        // }
        if(vm.patient_data.language === undefined || vm.patient_data.language === "" || vm.patient_data.language === null) {
          vm.languageFlag = true;
          vm.result_sent_modal = false;
          console.log("language");
        }
        if(vm.patient_data.payor === undefined || vm.patient_data.payor === "" || vm.patient_data.payor === null) {
          vm.payorValidation = "is-invalid";
          vm.result_sent_modal = false;
          console.log("payor");
        }
        if(vm.patient_data.payor === vm.payorInsurer) {
          if(vm.patient_data.insurer === undefined || vm.patient_data.insurer === "" || vm.patient_data.insurer === null) {
            vm.selectInsurerValidation = "is-invalid";
            vm.result_sent_modal = false;
            console.log("selectInsurer");
          }
          if(vm.patient_data.policy_no === undefined || vm.patient_data.policy_no === "" || vm.patient_data.policy_no === null) {
            vm.policyNumberValidation = "is-invalid";
            vm.result_sent_modal = false;
            vm.policyNumberError = "Please enter policy number";
            console.log("policyNumber");
          }
          if(vm.expiry_date === null || vm.expiry_date === undefined || vm.expiry_date === "") {
            vm.eDateFlag = true;
            vm.result_sent_modal = false;
            vm.expiryDateError = "Please enter expiry date"
          }else {
            vm.patient_data.policy_expiry_date = vm.expiry_date.getDate() + "/" + ( vm.expiry_date.getMonth() + 1 ) + "/" + vm.expiry_date.getFullYear();
            console.log("Expiry date -> "+vm.patient_data.policy_expiry_date);
          }
          // if(vm.eday === undefined || vm.emonth === undefined || vm.eyear === undefined ||
          //   vm.eday === "" || vm.emonth === "" || vm.eyear === "") {
          //   vm.eDateFlag = true;
          //   vm.result_sent_modal = false;
          //   vm.expiryDateError = "Please enter expiry date"
          //   console.log("Expiry date");
          // }else {
          //  vm.patient_data.policy_expiry_date = vm.eday + "/" + vm.emonth + "/" + vm.eyear;
          //  console.log("Expiry date -> "+vm.patient_data.policy_expiry_date);
          // }
        }
        if(vm.patient_data.payor === vm.payorOthers) {
          if(vm.patient_data.payor_name === undefined || vm.patient_data.payor_name === "" || vm.patient_data.payor_name === null) {
            vm.payorNameValidation = "is-invalid";
            vm.result_sent_modal = false;
            vm.payorNameError = "Please enter payor name";
            console.log("payor name");
          }
          if(vm.patient_data.policy_no === undefined || vm.patient_data.policy_no === "" || vm.patient_data.policy_no === null) {
            vm.policyNumberValidation = "is-invalid";
            vm.result_sent_modal = false;
            vm.policyNumberError = "Please enter policy number";
            console.log("policy number");
          }
          if(vm.expiry_date === undefined || vm.expiry_date === "" || vm.expiry_date === null) {
            vm.eDateFlag = true;
            vm.result_sent_modal = false;
            vm.expiryDateError = "Please enter expiry date"
            console.log("Expiry day");
          }else {
            vm.patient_data.policy_expiry_date = vm.expiry_date.getDate() + "/" + ( vm.expiry_date.getMonth() + 1 ) + "/" + vm.expiry_date.getFullYear();
            console.log("Expiry date -> "+vm.patient_data.policy_expiry_date);
           }
          // if(vm.eday === undefined || vm.emonth === undefined || vm.eyear === undefined ||
          //   vm.eday === "" || vm.emonth === "" || vm.eyear === "") {
          //   vm.eDateFlag = true;
          //   vm.result_sent_modal = false;
          //   vm.expiryDateError = "Please enter expiry date"
          //   console.log("Expiry day");
          // }else {
          //  vm.patient_data.policy_expiry_date = vm.eday + "/" + vm.emonth + "/" + vm.eyear;
          //  console.log("Expiry date -> "+vm.patient_data.policy_expiry_date);
          // }
        }
        console.log("After Result sent modal -> "+vm.result_sent_modal);
        console.log("Patient Data : "+JSON.stringify(vm.patient_data));
        console.log("vm.result_sent_modal -> "+vm.result_sent_modal);
        if(vm.result_sent_modal === true) {
          vm.create_profile();
        }
      }

      vm.getData = function() {
        vm.hideNextButton = true;
        vm.dobFlag = false;
        vm.mobileNumberFlag = false;
        vm.contactNumberFlag = false;
        vm.genderFlag = false;
        vm.nationalityFlag = false;
        vm.identificationTypeFlag = false;
        vm.setValid();
        if(vm.patient_data.first_name === undefined || vm.patient_data.first_name === "" || vm.patient_data.first_name === null) {
          vm.firstNameValidation = "is-invalid";
          vm.hideNextButton = false;
          vm.firstNameError = "Please enter first name";
        }
        if(vm.patient_data.last_name === undefined || vm.patient_data.last_name === "" || vm.patient_data.last_name === null) {
          vm.lastNameValidation = "is-invalid";
          vm.hideNextButton = false;
          vm.lastNameError = "Please enter last name"
        }
        // if(vm.patient_data.gender_id === undefined || vm.patient_data.gender_id === "") {
        //   vm.genderValidation = "is-invalid";
        //   vm.hideNextButton = false;
        // }
        if(vm.patient_data.gender_id === undefined || vm.patient_data.gender_id === "" || vm.patient_data.gender_id === null) {
            vm.genderFlag = true;
            vm.hideNextButton = false;
        }
        if(vm.patient_data.ic_number === undefined || vm.patient_data.ic_number === "" || vm.patient_data.ic_number === null) {
          vm.identificationNumberValidation = "is-invalid";
          vm.hideNextButton = false;
        }
        if(vm.patient_data.address === undefined || vm.patient_data.address === "" || vm.patient_data.address === null) {
          vm.addressValidation = "is-invalid";
          vm.hideNextButton = false;
          vm.addressError = "Please enter address"
        }
        if(vm.patient_data.postal_code === undefined || vm.patient_data.postal_code === "" || vm.patient_data.postal_code === null) {
          vm.postalCodeValidation = "is-invalid";
          vm.hideNextButton = false;
          vm.postalCodeError = "Please enter postal code"
        }
        if(vm.patient_data.ic_type === undefined || vm.patient_data.ic_type === "" || vm.patient_data.ic_type === null) {
          vm.identificationTypeFlag = true;
            vm.hideNextButton = false;
        }
        if(vm.patient_data.country_id === undefined || vm.patient_data.country_id === "" || vm.patient_data.country_id === null) {
          vm.nationalityFlag = true;
          vm.hideNextButton = false;
        }
        if(vm.mobileNumber === undefined || vm.mobileNumber === "" || vm.mobileNumber === null) {
          vm.mobileNumberFlag = true;
          vm.hideNextButton = false;
          vm.mobileNumberError = "Please enter mobile number";
        }
        if(vm.mobileNumberCode === undefined || vm.mobileNumberCode === "" || vm.mobileNumberCode === null) {
          vm.mobileNumberFlag = true;
          vm.hideNextButton = false;
          vm.mobileNumberError = "Please select calling code";
        }
        if(vm.patient_data.email_address === undefined || vm.patient_data.email_address === "" || vm.patient_data.email_address === null) {
          vm.emailValidation = "is-invalid";
          vm.hideNextButton = false;
          vm.emailError = "Please enter email";
        }

        // if(vm.day === undefined || vm.month === undefined || vm.year === undefined ||
        //    vm.day === "" || vm.month === "" || vm.year === "") {
        //    vm.dobFlag = true;
        //    vm.hideNextButton = false;
        //    vm.dobError = "Please enter date of birth"
        // }else {
        //   vm.patient_data.birth_date = vm.day + "/" + vm.month + "/" + vm.year;
        //   console.log("DOB -> "+vm.patient_data.birth_date);
        //   vm.ageCalculator();
        // }
        if(vm.dob_date === undefined || vm.dob_date === null || vm.dob_date === "") {
          vm.dobFlag = true;
          vm.hideNextButton = false;
          vm.dobError = "Please select date of birth"
        }else {
            vm.patient_data.birth_date = vm.dob_date.getDate() + "/" + (vm.dob_date.getMonth() + 1) + "/" + vm.dob_date.getFullYear();
            console.log("DOB -> "+vm.patient_data.birth_date);
            vm.ageCalculator();
        }

      }

      vm.close_release_notice = function () {
        vm.result_sent_modal = false;
        vm.hideNextButton = false;
        vm.hideModal();
      }

      vm.update_profile = function() {
        vm.result_sent_modal = false;
        console.log("Update profile");
        console.log("URL -> "+"v2/doctor/patient/"+vm.patientId+"/update_patient");
        console.log("vm.patient_data -> "+JSON.stringify(vm.patient_data));
        HttpServer.post_v2("v2/doctor/patient/"+vm.patientId+"/update_patient",{profile: vm.patient_data}).then(
          function (res) {
             console.log(res);
             vm.result_sent_modal = true;
          }
        );
      }

      vm.create_profile = function(){
        vm.result_sent_modal = false;
        console.log("Patientdata -> "+JSON.stringify(vm.patient_data));
        // $s
        HttpServer
        .post("v2/doctor/patient/create_patient",{profile: vm.patient_data})
        .response( success);
        function success(res){
          console.log(res);
          if(res.status == 200) {
            vm.result_sent_modal = true;
          }     
        }
        function error(error) {
          console.log("Error -> "+error);
        }

        // vm.mapPatientName()
        // console.log(vm.patient_data)
        // HttpServer
        //   .post("api/v1/patient/profiles",{profile: vm.patient_data})
        //   .response( success );

        //   function success(res){
        //     vm.save_ok = true;
        //     $timeout(function() {
        //       vm.create_message = true;
        //       vm.hideModal();
        //       EOrderService.update_verify_patient_information(res.data.attributes);
        //     }, 3000);
        //   }
        //   vm.create_message = false;
      }

      // vm.mapPatientName = function() {
      //   var splitted_name = vm.patient_data.patient_name.split(' ')
      //   vm.patient_data.first_name = splitted_name[0]
      //   vm.patient_data.last_name = splitted_name.slice(1, splitted_name.length).join(' ')
        // vm.patient_data.birth_date = "01/05/1993"
        // vm.patient_data.country_id = "+65"
      // }

      vm.ageCalculator = function(){
        console.log("vm.patient_data.birth_date -> "+vm.patient_data.birth_date);
        // var date_birth = new Date(vm.patient_data.birth_date); 
        var date_birth = new Date(vm.patient_data.birth_date); 
        console.log("Date.now() -> "+Date.now());
        console.log("date_birth -> "+ date_birth.getTime());
        var ageDifMs = Date.now() - date_birth.getTime();
        var ageDate = new Date(ageDifMs);
        console.log("AgeDate -> "+ageDate.getUTCFullYear()); 
        var age =  Math.abs(ageDate.getFullYear() - 1970);
        vm.patient_data.age = age
        console.log("Age -> "+vm.patient_data.age);
        console.log("Patient data in form modal -> "+vm.editData);
      }

      this.$onInit = function(){
        console.log("Patient data in form modal -> ",vm.editData);
        console.log("Is edit in form modal -> ",vm.isEdit);
        vm.patient_data = {};
        vm.modalState = false
        PopupModalService.patient_form_modal_state = function(modal_state ,other_data){
          vm.isEdit = other_data.isEdit;
          console.log("open");
          console.log("Patient id -> "+vm.patientId);
          vm.dob_date = "";
          vm.expiry_date = "";
          vm.getNationalityData();
          if(vm.isEdit && vm.editData != undefined) {
            console.log("inside edit");
            vm.patient_data.first_name = vm.editData.first_name;
            vm.patient_data.last_name = vm.editData.last_name;
            vm.patient_data.gender_id = vm.editData.gender_id.toString();;
            vm.patient_data.country_id = vm.editData.country_id;
            vm.patient_data.ic_type = vm.editData.ic_type;
            vm.patient_data.ic_number = vm.editData.ic_number;
            vm.patient_data.ethnic = vm.editData.ethnic;
            vm.patient_data.address = vm.editData.address;
            vm.patient_data.postal_code = vm.editData.postal_code;
            vm.patient_data.email_address = vm.editData.email_address;
            vm.patient_data.martial_status = vm.editData.martial_status;
            vm.patient_data.occupation = vm.editData.occupation;
            vm.patient_data.language = vm.editData.language;
            vm.patient_data.religion = vm.editData.religion;
            vm.patient_data.nok = vm.editData.nok;
            vm.patient_data.nok_relationship = vm.editData.nok_relationship;
            vm.patient_data.payor = vm.editData.payor;
            vm.patient_data.insurer = vm.editData.insurer;
            vm.patient_data.payor_name = vm.editData.payor_name;
            vm.patient_data.policy_no = vm.editData.policy_no;
            vm.patient_data.contact = vm.editData.contact;
            vm.patient_data.mobile = vm.editData.mobile;
            // vm.patient_data.policy_expiry_date = vm.editData.policy_expiry_date;
            vm.patient_data.age = vm.editData.age;
            //  vm.patient_data.birth_date = vm.editData.birth_date;

            if(vm.editData.birth_date != null) {
              console.log("dob -> "+vm.editData.birth_date);
              vm.patient_data.birth_date = new Date(vm.editData.birth_date);
              let birthDate = vm.patient_data.birth_date.getDate()+"/"+( vm.patient_data.birth_date.getMonth() + 1 )+"/"+vm.patient_data.birth_date.getFullYear()
              console.log("Birth date -> "+birthDate);
              vm.dob_date = birthDate;
            }

            if(vm.editData.policy_expiry_date != null) {
              console.log("ed -> "+vm.editData.policy_expiry_date);
              vm.patient_data.policy_expiry_date = new Date(vm.editData.policy_expiry_date);
              let expiryDate = vm.patient_data.policy_expiry_date.getDate()+"/"+( vm.patient_data.policy_expiry_date.getMonth() + 1 )+"/"+vm.patient_data.policy_expiry_date.getFullYear()
              console.log("Expiry Date -> "+expiryDate);
              vm.expiry_date = expiryDate;
            }
            

            // vm.patient_data.policy_expiry_date = vm.editData.policy_expiry_date;

            // vm.expiry_date = vm.editData.policy_expiry_date;
            if(vm.editData !== null && vm.editData.contact_info !== null && vm.editData.contact_info !== undefined && vm.editData.contact_info !== "" && vm.editData.contact_info.contact !== null && vm.editData.contact_info.contact !== undefined) {
              const contactArray = vm.editData.contact_info.contact.split(" ");
              vm.contactNumberCode = contactArray[0];
              vm.contactNumber = contactArray[1];
            }
            if(vm.editData != null && vm.editData.mobile != null && vm.editData.mobile != undefined && vm.editData.mobile != "") {
              const mobileArray = vm.editData.mobile.split(" ");
              console.log("mobileArray[0] -> "+mobileArray[0]);
              console.log("mobileArray[1] -> "+mobileArray[1]);
              vm.mobileNumberCode = mobileArray[0];
              vm.mobileNumber = mobileArray[1];
            }
          }
          vm.modalState = modal_state;
          vm.data = other_data;
        }
      }
    }
})();

// Edit data -> {"demographic":{"ethnic":null,"picture":null,"ic_number":"RTX3090TI","first_name":"Gerald Under","birth_date":55,"ic_type":null,"martial_status":null,"occupation":null,"language":null,"religion":null,"nok":null,"nok_relationship":null,"date_of_birth":"1966-09-13","gender":"Female","is_temp":true},"has_diabetes_dashboard":false,"has_hypertension_dashboard":false,"is_temp":true,"is_account_active":true}
