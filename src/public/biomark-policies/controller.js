( function(){

	"use strict";


	angular
		.module("Biomark")
		.controller("bmpoliciesController",bmpoliciesController);

		bmpoliciesController.$inject=["$state","BiomarkConfig"];

		function bmpoliciesController( $state, BiomarkConfig ){
            var hc = this;

			hc.tab_position = 0;

			hc.tabs = [
				{id:0,label:"Terms of Service",model:"profile"},
				{id:1,label:"Data Privacy",model:"contact"},
			]
			hc.country = $state.params.country;
			hc.tabChanged = function(tab){
				hc.tab_position = tab;
				hc.policy = tab == 0 ? BiomarkConfig.tos : BiomarkConfig.privacy;
			}
			hc.$onInit = function(){
				hc.policy = BiomarkConfig.tos;
				BiomarkConfig.meta_listener = function(){
					hc.policy = BiomarkConfig.tos;
				}
			}
		}
})();