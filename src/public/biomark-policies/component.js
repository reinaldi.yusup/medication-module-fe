( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('biomarkPolicies',{
			templateUrl:"/public/biomark-policies/view.html",
			controller:"bmpoliciesController",
		})
})();