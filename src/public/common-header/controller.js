( function(){

	"use strict";


	angular
		.module("Biomark")
		.controller("commonHeaderController",commonHeaderController);

		commonHeaderController.$inject=["$state"];

		function commonHeaderController( $state ){
            
            this.$onInit = function(){
                this.url_state = $state.current.name;
            }
		}
})();