( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('commonHeader',{
			templateUrl:"/public/common-header/view.html",
			controller:"commonHeaderController",
		})
})();