(function () {
	"use strict";

	angular
		.module("Biomark")
		.controller("doctorRegisterController", doctorRegisterController);

	doctorRegisterController.$inject = ["HttpServer", "BiomarkConfig"];

	function doctorRegisterController(HttpServer, BiomarkConfig) {

		var vm = this;
		var letterNumber = /^[\w\-\s]+$/;
		var capitalLetters = /^[A-Z0-9]+$/;
		var numbers = /^[0-9]+$/;
		var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

		vm.success = false;
		vm.data = {country_id: BiomarkConfig.current_country.id};
		vm.fields = {
			clinic_name: false,
			clinic_address: false,
			doctor_name: false,
			hci_code: false,
			email_address: false,
			temp_number: false,
      max_temp_number: false,
      mmc_code: false,
		}

		vm.register = function (user) {
			if (validate_entry(user)) {
				user.contact_number = BiomarkConfig.dial_code + angular.copy(user.temp_number);
        if (user.mmc_code !== undefined) {user.hci_code = user.mmc_code}
        HttpServer.post_v2("v2/doctor/account", { user }).then(function (res) {
					vm.success = true;
					vm.data = {};
				})
			}
		}

		function validate_entry(user) {
			var state = true;
      var mobile_config = BiomarkConfig.mobile_configs.filter(mc => mc.country_id === user.country_id)[0]
      angular.forEach(vm.fields, function (data, key) {
				vm.fields[key] = false;
			});

			if (angular.isUndefined(user.clinic_name) || user.clinic_name == "") {
				vm.fields.clinic_name = true
				state = false;
			} else {
				if (user.clinic_name.length < 5 || user.clinic_name.length > 50) {
					vm.fields.clinic_name = true
					state = false;
				}
			}

			if (angular.isUndefined(user.clinic_address) || user.clinic_address == "") {
				vm.fields.clinic_address = true
				state = false;
			} else {
				if (user.clinic_address.length < 5 || user.clinic_address.length > 80) {
					vm.fields.clinic_address = true
					state = false;
				}
			}

			if (angular.isUndefined(user.doctor_name) || user.doctor_name == "") {
				vm.fields.doctor_name = true
				state = false;
			} else {
				if (user.doctor_name.length < 5 || user.doctor_name.length > 50) {
					vm.fields.doctor_name = true
					state = false;
				}
				if (!user.doctor_name.match(letterNumber)) {
					vm.fields.doctor_name = true
					state = false;
				}
			}

			if (!angular.isUndefined(user.hci_code) && !user.hci_code == "") {
				if (user.hci_code.length != 7) {
					vm.fields.hci_code = true
					state = false;
				}
				if (!user.hci_code.toUpperCase().match(capitalLetters)) {
					vm.fields.hci_code = true
					state = false;
				}
			}

      if (!angular.isUndefined(user.mmc_code) && !user.mmc_code == "") {
				if (user.mmc_code.length != 5) {
					vm.fields.mmc_code = true
					state = false;
				}
			}

			if (angular.isUndefined(user.email_address) || user.email_address == "") {
				vm.fields.email_address = true
				state = false;
			} else {
				if(!user.email_address.match(emailRegex)){
                    vm.fields.email_address = true
                    state = false;
                }
			}
			if (angular.isUndefined(user.temp_number) || user.temp_number == "") {
				vm.fields.temp_number = true
				state = false;
			} else {
				if (user.temp_number.length < mobile_config.mobile_min || user.temp_number.length > mobile_config.mobile_max) {
					vm.fields.temp_number = true
					state = false;
				}
				if (!user.temp_number.match(numbers)) {
					vm.fields.temp_number = true
					state = false;
				}
			}

			return state

		}
	}
})();