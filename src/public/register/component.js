( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("doctorRegister",{
			templateUrl:"/public/%PORTAL_TYPE%/register/view.html",
            controller:"doctorRegisterController"
		})
})();