( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("forgotPasswordFormController", forgotPasswordFormController);

		forgotPasswordFormController.$inject=["HttpServer"];

		function forgotPasswordFormController(HttpServer){
			var vm = this;
			vm.has_error = false;
			vm.sent = false;
			vm.requestResetLink = function(data){
				HttpServer.post_v2("v2/doctor/account/request_reset_password_link",data).then(function(res){
					vm.sent = true;
				},function(err){
					vm.has_error = true;
					vm.error_message = err.data.message;
				});
			}
			
		}
})();
