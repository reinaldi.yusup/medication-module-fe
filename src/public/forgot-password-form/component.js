( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('forgotPasswordForm',{
			templateUrl:"/public/forgot-password-form/view.html",
			controller:"forgotPasswordFormController",
		})
})();