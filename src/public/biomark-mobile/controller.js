(function () {

	"use strict";


	angular
		.module("Biomark")
		.controller("mobileController", mobileController);

	mobileController.$inject = ["BiomarkConfig"];

	function mobileController(BiomarkConfig) {
		var vm = this;

		vm.$onInit = function () {
			vm.is_visible = false;
			vm.countries = BiomarkConfig.countries;
			vm.default = BiomarkConfig.current_country;
			// vm.mobile_placeholder = "2 1234 5678";
			// vm.mobile_regex = "^[0-9]{1,9}$";
			// vm.mobile_max = 9;
			vm.select_dialcode = function () {
				// vm.is_visible = !vm.is_visible;
			}
			vm.onValueChanged = function (data) {
				vm.default = data;
				vm.country = data.id;
				switch (data.code) {
					case "PH":
						vm.mobile_placeholder = "915 123 4567";
						vm.mobile_regex = "^9[0-9]{1,9}$";
						vm.mobile_min = 10;
						vm.mobile_max = 10;
						break;
					case "SG":
						vm.mobile_placeholder = "1234 4567";
						vm.mobile_regex = "^[0-9]{1,8}$";
						vm.mobile_min = 8;
						vm.mobile_max = 8;

						break;
					case "MY":
						vm.mobile_placeholder = "12 1234 5678";
						vm.mobile_regex = "(^[0-9]{1,9})|(^[0-9]{1,10})";
						vm.mobile_min = 8;
						vm.mobile_max = 10;
						break;
					case "ID":
						vm.mobile_placeholder = "12 1234 5678";
						vm.mobile_regex = "(^[0-9]{1,9})|(^[0-9]{1,10})";
						vm.mobile_min = 8;
						vm.mobile_max = 12;
						break;
				}
			}

			switch (vm.default.code) {
				case "PH":
					vm.mobile_placeholder = "915 123 4567";
					vm.mobile_regex = "^9[0-9]{1,9}$";
					vm.mobile_min = 10;
					vm.mobile_max = 10;
					break;
				case "SG":
					vm.mobile_placeholder = "1234 4567";
					vm.mobile_regex = "^[0-9]{1,8}$";
					vm.mobile_min = 8;
					vm.mobile_max = 8;

					break;
				case "MY":
					vm.mobile_placeholder = "12 1234 5678";
					vm.mobile_regex = "(^[0-9]{1,9})|(^[0-9]{1,10})";
					vm.mobile_min = 8;
					vm.mobile_max = 11;
					break;
				case "ID":
					vm.mobile_placeholder = "12 1234 5678";
					vm.mobile_regex = "(^[0-9]{1,9})|(^[0-9]{1,10})";
					vm.mobile_min = 8;
					vm.mobile_max = 12;
					break;
			}
		}
	}

})();