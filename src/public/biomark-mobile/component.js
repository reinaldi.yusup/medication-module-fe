( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('biomarkMobile',{
			bindings:{
				country:"=",
				mobile:"=",
				invalid:"=",
				submitted:"="
			},
			controller:"mobileController",
			templateUrl:"/public/biomark-mobile/view.html"
		})
})();