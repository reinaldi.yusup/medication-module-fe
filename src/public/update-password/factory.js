( function(){
	"use strict";

	angular
		.module("Biomark")
		.factory("passwordScore",passwordScore);
		
		passwordScore.$inject = [];

		function passwordScore(){
            var f = {};
            f.check = function(){
                var compute = zxcvbn.apply(null, arguments);
                return compute && compute.score;
            }
            return f;
		}
})();