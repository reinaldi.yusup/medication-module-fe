( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("updatePassword",{
			templateUrl:"/public/update-password/view.html",
            controller:"updatePasswordController"
		})
})();