( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("updatePasswordController",updatePasswordController);
		
		updatePasswordController.$inject = [ "HttpServer","$state","passwordScore"];

		function updatePasswordController( HttpServer, $state, passwordScore){

            var vm = this;
			vm.showPasswordNew = false;
			vm.showPasswordVerify = false;
			vm.loading = true;
			vm.has_error = false;
			vm.user = {};
			vm.password_updated = false;

			vm.togglePassword = function(toggle){
				switch (toggle) {
					case 'new':
						vm.showPasswordNew = !vm.showPasswordNew;
					break;
					case 'verify':
						vm.showPasswordVerify = !vm.showPasswordVerify;
					break;
				}
				
			}

            vm.$onInit = function(){
				HttpServer.post_v2("v2/doctor/setting/get_reset_authorization",{token:$state.params.token}).then(
					function(res){
						vm.has_error = ( res.data.data == "ok" ) ? false : true;
					},
					function(){
						vm.has_error = true;
					}
				);	
			}
			vm.update_password = function(){
				
			}
			vm.password_change = function(password){
				vm.has_form_error = false;
				vm.score = 0;
				if(password && password.length > 0){
					vm.score = passwordScore.check(password);
				}else{
					vm.score = 0;
				}
			}
			vm.change_password = function( user ){
				vm.has_form_error = false;
				if(user.new_password != user.verify_password){
					vm.has_form_error = true;
					vm.error_message = "Please make sure your passwords match";
					
				}else{
					user.token = $state.params.token;
					HttpServer.post_v2("v2/doctor/setting/reset_password",{user:user}).then(
						function(res){
							vm.password_updated = true;
							// alert("Password updated!");
							// $state.go("login");
						}
					);	
				}

				
			}
		}
})();