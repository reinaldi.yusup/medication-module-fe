(function () {
	'use strict';

	angular.module('Biomark').controller('loginController', loginController);

	loginController.$inject = [
	      '$state',
	      'User',
	      'Personal',
	      'HTTPInterceptor',
	      '$timeout',
	      'HttpServer',
	      'BiomarkConfig',
	];

	function loginController(
	      $state,
	      User,
	      Personal,
	      HTTPInterceptor,
	      $timeout,
	      HttpServer,
        BiomarkConfig
	) {
	      var lc = this;

	      lc.working = false;
	      lc.showPassword = false;
	      lc.version = HttpServer.app_version;

	      lc.stage = 1;

	      lc.togglePassword = function () {
		lc.showPassword = !lc.showPassword;
	      };

	      lc.confirm = function (user) {
		User.confirm(user).then(
	      function (res) {
		User.setup_token(res.data.access_token, function () {
		      location.reload();
		});
		//
		//$state.go("dashboard");
	      },
	      function (res) {
		lc.error_message = res.data.message;
		lc.pass_invalid = true;
		$timeout(function () {
		      lc.invalid_message = true;
		}, 3000);
		lc.invalid_message = false;
	      }
		);
	      };

	      lc.resend_code = function () {
		User.resend_code(lc.user.username).then(
	      function (res) {
		lc.save_ok = true;
		$timeout(function () {
		      lc.update_message = true;
		}, 3000);
		lc.update_message = false;
	      },
	      function (res) {
		if (res.data.error == 'Internal Server Error') {
		      lc.error_message =
			'Attempt limit exceeded, please try again after some time.';
		      lc.pass_invalid = true;
		      $timeout(function () {
			lc.invalid_message = true;
		      }, 3000);
		      lc.invalid_message = false;
		} else {
		      // console.log(res);
		}
	      }
		);

		// function success(res){
		// 	lc.save_ok = true;
		// 	$timeout(function(){
		// 		lc.update_message = true;
		// 	}, 3000);
		// 	lc.update_message = false;
		// }
	      };
	      lc.update_password = function (user) {
		console.log('sss');
		lc.working = true;
		// delete HTTPInterceptor.errorHandler;
		// HTTPInterceptor.errorHandler = function(e){
		// 	alert(e.data.message);
		// 	lc.working = false;
		// 	return false;
		// }
		User.update_password({ user: user }).response(success);

		function success(res) {
	      User.set_token(res.access_token, res.session);

	      $state.go('dashboard');
	      lc.instance_user = false;
		}

		function error(err) {
	      // console.log(err);
		}
	      };

	      lc.login = function (user) {
		user._username = user._username.toUpperCase();
		user.password = angular.copy('0000' + user._password);
    var emailDomain = BiomarkConfig.email_domain
  
		user.username = angular.copy(user._username + emailDomain);
		lc.working = true;

		User.login(user).then(
	      function (res) {
		console.log('login ok');
		lc.working = false;
		if (res.data.message == 'NEW_PASSWORD_REQUIRED') {
		      lc.stage = 2;
		}
		if (res.data.message == 'Authenticated') {
		      //console.log('token after login', res.data.access_token);
		      User.setup_token(res.data.access_token, function () {
            HttpServer.get("v1/doctor/user").response(function(user){
              var permission = user.personal.permissions
              console.log(user.personal)
              if (user.personal.quest.e_consent === false ) {
			          return location.reload();
              }
              if (permission.patient !== undefined) {
                console.log(permission.patient )
                $state.go("dashboard");
              } else if (permission.booking !== undefined) {
                console.log(permission.booking )
                $state.go("bookings");
              } else if (permission.order !== undefined) {
                console.log(permission.order )
                $state.go("eorders/0");
              } else if (permission.consumable !== undefined) {
                console.log(permission.consumable )
                $state.go("consumable");
              } else {
			          location.reload();
              }
            })
			    // location.reload();
		      });
          
		      //
		      //$state.go("dashboard");v
		}
	      },
	      function (res) {
		lc.working = false;
		if (res.data.message == 'Account is not confirmed') {
		      lc.stage = 3;
		} else {
		      lc.error_message = res.data.message;
		      if (
			lc.error_message ==
			'The email address or the password you inputted is incorrect. '
		      ) {
			lc.error_message =
		      'The username or password entered is incorrect. Please try again.';
		      }
		      lc.pass_invalid = true;
		      $timeout(function () {
			lc.invalid_message = true;
		      }, 3000);
		      lc.invalid_message = false;
		}
	      }
		);
	      };

	      lc.close = function () {
		lc.save_ok = false;
		lc.pass_invalid = false;
	      };
	}
      })();
