( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('commonFooter',{
			templateUrl:"/public/common-footer/view.html",
			controller:"commonFooterController",
		})
})();