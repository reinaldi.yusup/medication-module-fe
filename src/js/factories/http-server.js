( function(){
	"use strict";


	angular
		.module("Biomark")
		.service("HttpServer",HttpServer);

		HttpServer.$inject=["$http","BiomarkConfig","$sessionStorage","$state","$q","$localStorage"];


		function HttpServer( $http , BiomarkConfig, $sessionStorage ,$state , $q, $localStorage){
			
			var that = this;
			
			this.app_version = "BM 2.1.0";

			var url = BiomarkConfig.host;
			Object.toparams = function ObjecttoParams(obj) {
				var p = [];
				for (var key in obj) {
					p.push(key + '=' + encodeURIComponent(obj[key]));
				}
				return p.join('&');
			};
			var canceller;

			this.post_with_abort = function( path, data){
				canceller = $q.defer();
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.biomark_token  || 0;
				$http.defaults.headers.common['x-session-token'] = $sessionStorage.session_token  || 0;
				$http.defaults.headers.common['x-biomark-group'] = BiomarkConfig.group_type;
				$http.defaults.headers.common['x-portal-type'] = "%PORTAL_TYPE%";
				return $http.post(url+path,data,{timeout:canceller.promise});
			}
			this.set_order_data = function(data){
				$localStorage.order_data = data;
			}
			this.get_order_data = function(){
				return $localStorage.order_data;
			}

			this.cancel_request = function(){
				if(canceller) canceller.resolve();
			}

			this.set_custom_specimen  = function(data){
				$localStorage.custom_specimen = data;
			}

			this.get_custom_specimen  = function(){
				return $localStorage.custom_specimen;
			}

			this.set_change_specimen  = function(data){
				$localStorage.change_specimen = data;
			}

			this.get_change_specimen  = function(){
				return $localStorage.change_specimen;
			}

			this.set_country_id = function(data){
				$localStorage.country_id_session = data;
			}

			this.get_country_id = function(){
				return $localStorage.country_id_session;
			}

			this.post = function(path,params){
				delete that.request;
				$http.defaults.headers.common['x-product-code'] = "QUEST123"
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.biomark_token  || 0;
				$http.defaults.headers.common['x-session-token'] = $sessionStorage.session_token  || 0;
				$http.defaults.headers.common['x-biomark-group'] = BiomarkConfig.group_type;
				$http.defaults.headers.common['x-portal-type'] = "%PORTAL_TYPE%";
				that.request = $http.post(url+path,params);
				return this;
			}

			this.post_v2 = function(path,params){
				delete that.request;
				$http.defaults.headers.common['x-product-code'] = "QUEST123"
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.biomark_token  || 0;
				$http.defaults.headers.common['x-session-token'] = $sessionStorage.session_token  || 0;
				$http.defaults.headers.common['x-biomark-group'] = BiomarkConfig.group_type;
				$http.defaults.headers.common['x-portal-type'] = "%PORTAL_TYPE%";
				return $http.post(url+path,params);
				// return that.request
			}

      this.get_poc_token = function() {
        let payload = {credential:{username: "jem", password: "123123"}}
				delete that.request;
        let request = $http.post('https://bm-poc-api.biomarking.com/biomark_booking/admin/user/sign_in', payload);
        let token = request.then(function(response) {
          $sessionStorage.biomark_poc_token = response.data.token;
        })
        return token;
      }

      this.post_poc = function(path,params){
				delete that.request;
				$http.defaults.headers.common['x-product-code'] = "QUEST123"
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.biomark_poc_token;
				$http.defaults.headers.common['x-session-token'] = $sessionStorage.session_token  || 0;
				$http.defaults.headers.common['x-biomark-group'] = BiomarkConfig.group_type;
				$http.defaults.headers.common['x-portal-type'] = "%PORTAL_TYPE%";
				that.request = $http.post('https://bm-poc-api.biomarking.com/'+path,params);
				return this;
			}

      this.put_poc = function(path,params){
				delete that.request;
				$http.defaults.headers.common['x-product-code'] = "QUEST123"
				$http.defaults.headers.common['x-session-token'] = $sessionStorage.session_token  || 0;
				$http.defaults.headers.common['x-biomark-group'] = BiomarkConfig.group_type;
				$http.defaults.headers.common['x-portal-type'] = "%PORTAL_TYPE%";
				that.request = $http.put('https://bm-poc-api.biomarking.com/'+path,params);
				return this;
			}

      this.get_poc = function(path){
				delete that.request;
				$http.defaults.headers.common['x-product-code'] = "QUEST123"
				// $http.defaults.headers.common['x-biomark-token'] = $sessionStorage.biomark_token  || 0;
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.biomark_poc_token;
				$http.defaults.headers.common['x-session-token'] = $sessionStorage.session_token  || 0;
				$http.defaults.headers.common['x-biomark-group'] = BiomarkConfig.group_type;
				$http.defaults.headers.common['x-portal-type'] = "%PORTAL_TYPE%";
				that.request = $http.get('https://bm-poc-api.biomarking.com/'+path);
				return this;
			}

      this.get_v2_poc = function(path){
				delete that.request;
				$http.defaults.headers.common['x-product-code'] = "QUEST123"
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.biomark_poc_token;
				$http.defaults.headers.common['x-session-token'] = $sessionStorage.session_token  || 0;
				$http.defaults.headers.common['x-biomark-group'] = BiomarkConfig.group_type;
				$http.defaults.headers.common['x-portal-type'] = "%PORTAL_TYPE%";
        return $http.get('https://bm-poc-api.biomarking.com/'+path);
				// return that.request
			}

			this.get_v2 = function(path){
				delete that.request;
				$http.defaults.headers.common['x-product-code'] = "QUEST123"
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.biomark_token  || 0;
				$http.defaults.headers.common['x-session-token'] = $sessionStorage.session_token  || 0;
				$http.defaults.headers.common['x-biomark-group'] = BiomarkConfig.group_type;
				$http.defaults.headers.common['x-portal-type'] = "%PORTAL_TYPE%";
        return $http.get(url+path);
				// return that.request
			}
			this.get = function(path){
				delete that.request;
				$http.defaults.headers.common['x-product-code'] = "QUEST123"
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.biomark_token  || 0;
				$http.defaults.headers.common['x-session-token'] = $sessionStorage.session_token  || 0;
				$http.defaults.headers.common['x-biomark-group'] = BiomarkConfig.group_type;
				$http.defaults.headers.common['x-portal-type'] = "%PORTAL_TYPE%";
				that.request = $http.get(url+path);
				return this;
			}

			this.set_token = function( token, session ){
				$sessionStorage.biomark_token = token;
				$sessionStorage.session_token = session;
			}
			this.response = function( cb ){

				that.request.then( function(res){
					if(res.status === 200 && res.xhrStatus === "complete"){
						if(cb) cb(res.data);
					}
					else return false;
				},
				function(res){
					// if(res.status === 401){
					// 	$sessionStorage.biomark_token = 0;
					// 	$state.go("login");
					// }else{
					// 	// console.log(res);
					// }
				}
			);
			}
			
		}

})();