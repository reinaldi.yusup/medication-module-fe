( function(){
	"use strict";

	angular
		.module("Biomark")
		.factory("Biomarker",Biomarker);

		Biomarker.$inject=[];

		function Biomarker(){
			var f = {

				data:{
					position:0,
					tabs:[
						{id:0,label:"History"},
						{id:1,label:"Description"},
						// {id:2,label:"Resources"}
					]
				},

				init:init
			};

			//binder of data
			function init( cb ){
				cb(f.data);
			}


			return f;
		}
})();