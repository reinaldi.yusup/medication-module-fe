( function(){
	"use strict";

	angular
		.module("Biomark")
		.factory("User",User)

		User.$inject=["HttpServer","$sessionStorage","$timeout"];

		function User(HttpServer, $sessionStorage, $timeout){
			
			var f ={
				data:{
					is_login:false,
					identity:{}
				},
				init:init,
				login:login,
				logout:logout,
				confirm:confirm,
				resend_code:resend_code,
				set_token: HttpServer.set_token,
				setup_token: setup_token,
				update_password: update_password
			};
			
			function setup_token(token,cb){
				$sessionStorage.biomark_token = token;
				//reloader callback
				//if(cb) cb();

				//added 100ms timeout to allow older devices to set session token
				$timeout(function(){
					if(cb) cb();
				},100);
				// 
			}
			function resend_code( email ){
				return HttpServer.post_v2("v2/users/resend_code",{username:email})
			}
			function confirm(user){
				return HttpServer.post_v2("v2/users/confirm",{user:user})
			}
			function logout(){
				return HttpServer.post("v1/sessions/destroy");
			}
			function update_password( credentials){	
				return HttpServer.post("v1/users/update",credentials);
			}
			function login(user){
				return HttpServer.post_v2("v2/sessions",{session:user})
			}

			function init( cb ){
				cb(f.data);
			}


			return f;
		}
})();