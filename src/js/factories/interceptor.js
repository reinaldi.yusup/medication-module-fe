"use strict";

angular
  .module("Biomark")
  .factory("HTTPInterceptor",HTTPInterceptor)

  HTTPInterceptor.inject=["$state"];

  function HTTPInterceptor( $state , $sessionStorage){
    var f = {
      responseError: function(res) {
        if(f.errorHandler){
          f.errorHandler(res);
        } else{
          switch(res.status){
            case -1:
              alert("UNABLE TO ESTABLISH CONNECTION TO SERVER, TRY AGAIN LATER");
              $sessionStorage.biomark_token = 0;
              break;
            case 401:
              console.log("Unauthorized Access");
              $sessionStorage.biomark_token = 0;
              $state.go("login");
              break;
            case 500:
              alert("SERVER ENCOUNTER TECHNICAL DIFFICULTIES")
              $sessionStorage.biomark_token = 0;
              $state.go("login");
              break;
          }
        }
        
        return res;
      }
    }
    return f;
    // {
    //   // request: function(config) {
    //   //   console.log("request",config)
    //   //   return config;
    //   // },

    //   // requestError: function(res) {
    //   //   console.log("requestError",res)
    //   //   return config;
    //   // },

    //   // response: function(res) {
    //   //   console.log("response",config)
    //   //   return res;
    //   // },

      
    // }

  }
