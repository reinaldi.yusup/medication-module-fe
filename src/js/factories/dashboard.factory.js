( function(){
	
	"use strict";


	angular
		.module("Biomark")
		.factory('Dashboard',Dashboard);

		Dashboard.inject=["HttpServer","ActionCableChannel","$sessionStorage","$q"];

		function Dashboard(HttpServer,ActionCableChannel,$sessionStorage,$q){
			var f = {
				//release configuration
				data:{
					first_login: false,
					has_patient: false,
					has_patient_invites: false,
					invite_patient_ui:false,
					preview_email: false,
					invitation_sent: false,
					patient_results: false,
					resend_invite_popup: false,
					invitation_cancelled: false,
					invitation_success: false,
					review_settings: false,
					review_setting_ok: false,
					patient_reminder: false,
					patient_reminder_sent : false,
					patient_detail_save: false,
					release_notice: false,
					patient_message:false,
					page:1,
					tab_index:1,
					pagination:1,
					patients:[],
					patientCount:[],
					is_ready: false
				},
				init:init,
			};
			function init( cb ){
				if($sessionStorage.biomark_token != 0 ){
					
					if(!f.data.socket){
						console.log("registering...");
						f.data.socket = new ActionCableChannel("ConnectChannel", {token: $sessionStorage.biomark_token, group: "doctor"});
						f.data.socket.subscribe(f.callback).then(function(){
							console.log("SOCKET IS LIVE");
						});
					}
				}else{
					console.log("disconnect");
				}
				cb(f.data);
			}

			
			return f;
		}

})();