( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('biomarkCountries',{
			bindings:{
				country:"=",
				invalid:"=",
				countryValid:"=",
				countryCode:"="
			},
			controller:"countriesController",
			templateUrl:"/views/countries.html"
		})
})();