( function(){

	"use strict";


	angular
		.module("Biomark")
		.controller("countriesController",countriesController);

		countriesController.$inject = ["Country", "HttpServer", "BiomarkConfig"];

		function countriesController( Country, HttpServer, BiomarkConfig ){
			var vm = this;
			vm.is_visible = false;

			vm.dropdown_toggle = function(){
				// vm.is_visible  = !vm.is_visible;
			}
			Country.init(function( data){
				vm.data = data;
				
				vm.$onInit = function(){
					if(angular.isDefined(vm.country)){
						vm.data.default = vm.data.countries[BiomarkConfig.country.id];	
					}
				}
			});
		
			vm.close = function(){
				vm.is_visible = false;
			}

			vm.onValueChanged = function( country ){
				vm.data.default = country;
				vm.is_visible = false;
				switch (country.dial_code){
					case "+60":
						vm.country = 1;
						vm.country_id = 1;
						vm.country_code= 1;
						HttpServer.set_country_id(1);
						break;
					case "+63":
						vm.country = 3;
						vm.country_id = 3;
						vm.country_code= 3;
						HttpServer.set_country_id(3);
						break;
					case "+65":
						vm.country = 4;
						vm.country_id = 4;
						vm.country_code= 4;
						HttpServer.set_country_id(4);
						break;
					case "+62":
						vm.country = 2;
						vm.country_id = 2;
						vm.country_code= 2;
						HttpServer.set_country_id(2);
						break;
					default:
						break;
				}
				// vm.country = vm.data.default.id;
			}
			vm.keyPress = function($event){
				var keyCode = $event.which || $event.keyCode;

				if (keyCode === 13 || keyCode === 32) {
					$event.preventDefault();
					vm.dropdown_toggle();
				}
				else if(keyCode === 38){
					$event.preventDefault();
					vm.country_counter = angular.copy(vm.data.default.id)
					vm.country_counter--;
					if(vm.country_counter == 0) vm.country_counter = 3
					if(vm.country_counter == 4) vm.country_counter = 1
					vm.data.default = vm.data.countries[vm.country_counter -1]
					vm.country = vm.data.default.id;
				}else if(keyCode === 40){
					$event.preventDefault();
					vm.country_counter = angular.copy(vm.data.default.id)
					vm.country_counter++;
					if(vm.country_counter == 0) vm.country_counter = 3
					if(vm.country_counter == 4) vm.country_counter = 1
					vm.data.default = vm.data.countries[vm.country_counter -1]
					vm.country = vm.data.default.id;
				}

			}
			vm.select = function($event, country){
				var keyCode = $event.which || $event.keyCode;
				if (keyCode === 13 || keyCode === 32) {
					vm.onValueChanged(country);
				}
			}
			
			
		}
})();