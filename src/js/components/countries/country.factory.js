( function(){
	"use strict";

	angular
		.module("Biomark")
		.factory("Country",Country);

		Country.$inject=["BiomarkConfig"];

		function Country(BiomarkConfig){

			var f ={
				data:{
					default:{
						id:1,
						name:"Malaysia",
						code: "MY",
						dial_code:"+93"
					},
					countries : BiomarkConfig.countries
				},
				getCurrentValue:getCurrentValue,
				init:init
			};

			function getCurrentValue(){
				return f.data.default;
			}
			function init(cb ){
				cb(f.data);
			}

			return f;
		}
})();