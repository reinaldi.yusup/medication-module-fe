( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller('timezoneController',timezoneController);

		timezoneController.$inject = ["HttpServer"];

		function timezoneController( HttpServer ){
			var vm = this;

			vm.is_visible = false;

			vm.onValueChanged = function(){
				vm.is_visible = false;
			}
			vm.close = function(){
				vm.is_visible = false;				
			}

			vm.dropdown_toggle = function(){
				vm.is_visible = true;				
			}
			HttpServer
				.get('v1/timezone')
				.response( success );

				function success(data){
					vm.zones = data;
				}
		}	
})();