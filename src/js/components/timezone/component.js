( function(){
	"use strict";


	angular
		.module("Biomark")
		.component("bioTimezone",{
			controller:"timezoneController",
			templateUrl:"/views/timezone.html"
		})
})();