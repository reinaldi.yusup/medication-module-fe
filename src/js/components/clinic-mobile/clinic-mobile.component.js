( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('biomarkClinicMobile',{
			bindings:{
				ccountry:"=",
				cmobile:"=",
				invalid:"="
			},
			controller:"clinicMobileController",
			templateUrl:"/views/clinic-mobile.html"
		})
})();