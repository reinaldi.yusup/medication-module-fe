( function(){
	
	"use strict";


	angular
		.module("Biomark")
		.controller("clinicMobileController",clinicMobileController);

		clinicMobileController.$inject=["BiomarkConfig"];

		function clinicMobileController( BiomarkConfig ){
			var vm = this;

			vm.$onInit = function(){
				vm.is_visible = false;
				

				var opt = vm.ccountry || 0;
				vm.countries = BiomarkConfig.countries;
				if(!vm.ccountry){
					vm.ccountry = 1;
					vm.default = BiomarkConfig.countries[opt];
				}else{
					vm.default  = BiomarkConfig.countries[opt - 1];
				}
				
				vm.ccountry = vm.default.id;
				// vm.mobile_placeholder = "2 1234 5678";
				// vm.mobile_regex = "^[0-9]{1,9}$";
				// vm.mobile_max = 9;
				vm.select_dialcode = function(){
					vm.is_visible = !vm.is_visible;
				}
				vm.onValueChanged = function( data ){
					vm.default = data;
					vm.ccountry = data.id;
					// vm.default.clinic_country = data.id;
 						switch(data.code){
						case "PH":
							vm.mobile_placeholder = "915 123 4567";
							vm.mobile_regex = "^9[0-9]{1,9}$";
							vm.mobile_max = 10;
						break;
						case "SG":
							vm.mobile_placeholder = "1234 4567";
							vm.mobile_regex = "^[0-9]{1,8}$";
							vm.mobile_max = 8;

						break;
						case "MY":
							vm.mobile_placeholder = "2 1234 5678";
							vm.mobile_regex = "^[0-9]{1,9}$";
							vm.mobile_max = 9;
						break;
						case "ID":
							vm.mobile_placeholder = "12 1234 5678";
							vm.mobile_regex = "(^[0-9]{1,9})|(^[0-9]{1,10})";
							vm.mobile_max = 12;
						break;
					}
				}

				switch(vm.default.code){
					case "PH":
						vm.mobile_placeholder = "915 123 4567";
						vm.mobile_regex = "^9[0-9]{1,9}$";
						vm.mobile_max = 10;
					break;
					case "SG":
						vm.mobile_placeholder = "1234 4567";
						vm.mobile_regex = "^[0-9]{1,8}$";
						vm.mobile_max = 8;

					break;
					case "MY":
						vm.mobile_placeholder = "2 1234 5678";
						vm.mobile_regex = "^[0-9]{1,9}$";
						vm.mobile_max = 9;
					break;
					case "ID":
						vm.mobile_placeholder = "12 1234 5678";
						vm.mobile_regex = "(^[0-9]{1,9})|(^[0-9]{1,10})";
						vm.mobile_max = 12;
					break;
				}
			}
		}


})();