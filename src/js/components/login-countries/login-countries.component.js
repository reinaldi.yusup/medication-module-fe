( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('biomarkLoginCountries',{
			controller:"loginCountriesController",
			templateUrl:"/views/login-countries.html"
		})
})();