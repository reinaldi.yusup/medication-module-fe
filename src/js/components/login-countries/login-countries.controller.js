( function(){

	"use strict";


	angular
		.module("Biomark")
		.controller("loginCountriesController",loginCountriesController);

		loginCountriesController.$inject = ["Country"];

		function loginCountriesController( Country ){
			var vm = this;

			vm.is_visible = false;

			vm.dropdown_toggle = function(){
				vm.is_visible  = !vm.is_visible;
			}
			Country.init(function( data){
				vm.data = data;
				
				vm.$onInit = function(){
					if(angular.isDefined(vm.country)){
						vm.data.default = vm.data.countries[vm.country-1];	
					}
				}
			});
		
			vm.close = function(){
				vm.is_visible = false;
			}

			vm.onValueChanged = function( country ){
				vm.data.default = country;
				vm.is_visible = false;
				vm.country = vm.data.default.id;
			}

			
			
		}
})();