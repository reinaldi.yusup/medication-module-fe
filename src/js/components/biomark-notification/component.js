( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("biomarkNotification",{
			templateUrl:"/views/biomark-notifications.html",
			controller:"biomarkNotificationController",
		})
})();