( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("biomarkNotificationController",biomarkNotificationController);

		biomarkNotificationController.$inject=["HttpServer","Personal","Dashboard","$state","$timeout","Country"];

		function biomarkNotificationController( HttpServer , Personal, Dashboard, $state, $timeout, Country){
			
			var vm = this;
			Country.init(function( data){
				vm.data = data;
				
				vm.$onInit = function(){
					if(angular.isDefined(vm.country)){
						vm.data.default = vm.data.countries[vm.country-1];	
					}
				}
			});
			vm.accept = function(){
				Dashboard.data.socket.send({
					action:"doctor_accept",
					patient_id:vm.patient_data.id,
					access_token: Dashboard.data.socket.channelParams.token,
					group: Dashboard.data.socket.channelParams.group
				})
				vm.popup_type = 3;
				$timeout(Dashboard.data.reloadCount, 500)
				$timeout(Dashboard.data.reloadPending, 1000)
				if(Dashboard.data.remote_function) Dashboard.data.remote_function(); 
			}
			vm.reject = function(){
				Dashboard.data.socket.send({
					action:"doctor_reject",
					patient_id:vm.patient_data.id,
					access_token: Dashboard.data.socket.channelParams.token,
					group: Dashboard.data.socket.channelParams.group
				})
				$timeout(Dashboard.data.reloadCount, 500)
				$timeout(Dashboard.data.reloadPending, 1000)
				vm.popup_type = 2;
			}
			if(!Dashboard.callback){
				
				Dashboard.callback = function(message){
					switch(message.action){
						case "patient_connect":
							vm.patient_data = message.payload;
							vm.popup_type = 1;
							break;	
					}
				}
			}
			
			
			
			vm.view_record = function( id ){
				vm.popup_type = 0;
				$state.go("patient",{id:id});
			}
			vm.close = function(){
				$timeout(Dashboard.data.reloadCount, 500)
				$timeout(Dashboard.data.reloadPending, 1000)
				vm.popup_type = 0;
				//
			}
			
			

		}
})();