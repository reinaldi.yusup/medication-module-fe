( function(){
	"use strict";


	angular
		.module("Biomark")
		.factory("Personal", Personal);

		Personal.$inject = [];


		function Personal(){

			var f = {};

			f.countries =  [
				{ "id":1,"name": "Malaysia", "dial_code": "+60", "code": "MY" },
				{ "id":5,"name": "Indonesia", "dial_code": "+62", "code": "ID" },
				{ "id":2,"name": "Philippines", "dial_code": "+63", "code": "PH" },
				{ "id":3,"name": "Singapore", "dial_code": "+65", "code": "SG" },
				
			]
			f.data = {
        id: 0,
				tab_position: 0,
				level:0,
				profile:{
					status: false,
					data:{
						country_id:1
					}
				},
				contact:{
					data:{},
					status: false
				},
        doctor_code: '',
			}
			f.reset = {
        id: 0,
				tab_position: 0,
				profile:{
					status: false,
					data:{
						country_id:1
					}
				},
				contact:{
					data:{},
					status: false
				},
				permissions: {}
			}
			f.init = function( cb ){
				cb(f.data);
			}
			return f;
		}
})();