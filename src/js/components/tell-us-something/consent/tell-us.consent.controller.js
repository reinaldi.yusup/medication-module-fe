( function(){

	"use strict";

	angular
		.module('Biomark')
        .controller('tellUsSomethingConsentCtrl',tellUsSomethingConsentCtrl);

        tellUsSomethingConsentCtrl.$inject = ["Personal","$state","$sessionStorage","HttpServer"];


        function tellUsSomethingConsentCtrl( Personal, $state, $sessionStorage, HttpServer ){

            var vm = this;
            vm.checked = false;
            vm.country_id = $sessionStorage.personal.country_id;
			Personal.init( function( _data ){
                vm.personal = _data;
			});

            vm.back = function(){
                Personal.tabChanged(0);
            }
            vm.goToDashboard = function(){
               

                HttpServer
                    .post("v1/doctor/profile",{profile:$sessionStorage.personal})
                    .response(success);
                    function success(data){

                    }
                switch ($sessionStorage.contact.country_id) {
                    case 2:
                        $sessionStorage.contact.country_id= 5;
                        break;
                    case 3:
                        $sessionStorage.contact.country_id = 2;
                        break;
                    case 4:
                        $sessionStorage.contact.country_id = 3;
                        break;
                    case 5:
                        $sessionStorage.contact.country_id = 5;
                        break;
                    default:
                        $sessionStorage.contact.country_id = 1;
                        break;
                }
                HttpServer
                    .post("v1/doctor/contact",{contact:$sessionStorage.contact})
                    .response(success);
                    function success(data){
                        console.log(data);
                    }

                vm.personal.profile_status = true;
                
            }

        }


})();