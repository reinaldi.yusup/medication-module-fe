( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('biomarkRegisterHeader',{
			controller:"biomarkRegisterMainHeaderController",
			templateUrl:"/views/register-header.html",
			controllerAs:"mh"
		})
})();