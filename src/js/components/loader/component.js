( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("biomarkLoader",{
			templateUrl:"/views/loader.html"
		})
})();