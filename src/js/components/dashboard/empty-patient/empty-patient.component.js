( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('emptyPatient',{
			controller:"emptyPatientController",
			templateUrl:"/views/dashboard/empty-patient.html"
		})
})();