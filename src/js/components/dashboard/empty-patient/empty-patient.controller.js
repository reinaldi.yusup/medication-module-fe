( function(){
	
	"use strict";


	angular
		.module("Biomark")
		.controller("emptyPatientController",emptyPatientController);

		emptyPatientController.$inject=["Dashboard","Personal"];

		function emptyPatientController( Dashboard, Personal ){
			var vm = this;

			Dashboard.init( function( data ){

				vm.portal = data;
		
			});

			Personal.init( function(data){
				vm.personal = data;
			});

			vm.invite_patient = function(){
				
				vm.portal.invite_patient_ui = true;
			}
			

		}

})();