( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('dashboardPopup',{
			controller:"popupController",
			templateUrl:"/views/dashboard/popup.html"
		})
})();