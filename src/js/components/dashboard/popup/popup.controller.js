( function(){
	
	"use strict";


	angular
		.module("Biomark")
		.controller("popupController",popupController);

		popupController.$inject=["Dashboard","HttpServer"];

		function popupController( Dashboard,HttpServer){
			var vm = this;
			
			vm.step = 1;

			Dashboard.init( function(data){
				vm.portal = data;
			});
			vm.finish = function(){
				HttpServer
					.post("v1/doctor/user/popup_close",{})
					.response( success );

					function success(res){
						vm.portal.first_login = false;
					}

				
			}

			
			vm.continue = move; 
			vm.back = move;

			function move(tag){
				vm.step = tag;
			}


		}

})();