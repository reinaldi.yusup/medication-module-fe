( function(){

	"use strict";

	angular
		.module("Biomark")
		.controller('biomarkDateController',biomarkDateController);

		biomarkDateController.$inject = ["DateDropdown","moment", "DateService"];

		function biomarkDateController( DateDropdown, moment, DateService ){
			var vm = this;

			vm.dateValid = false;


			DateDropdown.init( function(_data){

				vm.default = _data;
				vm.months = DateDropdown.months;

				//reassuring that the default already set
				vm.$onInit = function(){
					if(angular.isDefined(vm.date)){
						vm.date = moment(vm.date).format('YYYY-MM-DD');
						var _date = new Date(vm.date) //.toISOString();

						vm.default.month 	= _date.getMonth();
						vm.default.day 		= _date.getDate();
						vm.default.year 	= _date.getFullYear();
					} else {
            vm.default.month 	= '';
            vm.default.day 		= '';
            vm.default.year 	= '';
            DateService.update = function(date) {
              if(angular.isDefined(date) && date !== null){
                vm.date = moment(date).format('YYYY-MM-DD');
                var _date = new Date(vm.date)
                vm.default.month 	= _date.getMonth();
                vm.default.day 		= _date.getDate();
                vm.default.year 	= _date.getFullYear();
              } else {
                vm.default.month 	= '';
                vm.default.day 		= '';
                vm.default.year 	= '';
              }
            }
          }
					vm.check_validity();
				}
			});

			vm.is_visible = false;

			vm.onValueChanged = function( month ){
				vm.default.month = month;
				vm.check_validity();
			}

			vm.selectMonth = function(){
				vm.is_visible = !vm.is_visible;
			}
			vm.keyPress = function($event){
				vm.check_validity()
				if(isNaN(vm.default.month)) vm.default.month = -1;
				var keyCode = $event.which || $event.keyCode;

				if (keyCode === 13 || keyCode === 32) {
					$event.preventDefault();
					vm.selectMonth();
				}else if(keyCode === 38){
					$event.preventDefault();
					vm.default.month++;
					if(vm.default.month == 12){
						vm.default.month = 0;
					}else if(vm.default.month < 0){
						vm.default.month = 11;
					}

				}else if(keyCode === 40){
					$event.preventDefault();
					vm.default.month--;
					if(vm.default.month == 12){
						vm.default.month = 0;
					}else if(vm.default.month < 0){
						vm.default.month = 11;
					}

				}
			}
			vm.select = function($event,month){
				vm.check_validity();
				var keyCode = $event.which || $event.keyCode;
				if (keyCode === 13 || keyCode === 32) {
					vm.onValueChanged(month);
				}
			}
			vm.check_validity = function(){
				if(vm.default.month != -1 && vm.default.year && vm.default.day) vm.dateValid = true;
			}
		}
})();