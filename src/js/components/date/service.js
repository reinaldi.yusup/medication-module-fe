( function(){

	"use strict";

	angular
		.module("Biomark")
        .factory("DateService",DateService);
        
        DateService.$inject = ["HttpServer","$state"];

        function DateService(HttpServer,$state){
            var f = {};
            f.data = {};
            return f;
        }
})();