( function(){
	"use strict";


	angular
		.module("Biomark")
		.factory("DateDropdown", DateDropdown);

		DateDropdown.$inject=[];


		function DateDropdown(){
			
			var f ={
				data:{
					month:"-1",
					day:"",
					year:""
				},
				getCurrentDate :getCurrentDate,
				months:[
					{
						name: "January",
						short: "Jan",
						num: 1,
						index: 0,
						days: 31
					},
					{
						name: "February",
						short: "Feb",
						num: 2,
						index: 1,
						days: 28
					},
					{
						name: "March",
						short: "Mar",
						num: 3,
						index: 2,
						days: 31
					},
					{
						name: "April",
						short: "Apr",
						num: 4,
						index: 3,
						days: 30
					},
					{
						name: "May",
						short: "May",
						num: 5,
						index: 4,
						days: 31
					},
					{
						name: "June",
						short: "Jun",
						num: 6,
						index: 5,
						days: 30
					},
					{
						name: "July",
						short: "Jul",
						num: 7,
						index: 6,
						days: 31
					},
					{
						name: "August",
						short: "Aug",
						num: 8,
						index: 7,
						days: 31
					},
					{
						name: "September",
						short: "Sept",
						num: 9,
						index: 8,
						days: 30
					},
					{
						name: "October",
						short: "Oct",
						num: 10,
						index: 9,
						days: 31
					},
					{
						name: "November",
						short: "Nov",
						num: 11,
						index: 10,
						days: 30
					},
					{
						name: "December",
						short: "Dec",
						num: 12,
						index: 11,
						days: 31
					}
				],
				init:init
			};
			
			function getCurrentDate(){
				if(f.data.year == "" || f.data.month == -1 || f.data.day == "" ){
					return "";
				}else{
					return new Date( f.data.year ,f.data.month, f.data.day );	
					// return new Date( f.data.year+"-"+(f.data.month+1)+"-"+f.data.day );	
				}
			}
			
			//callback function with options
			function init( cb ){
				cb( f.data );
			}


			return f;
		}

})();