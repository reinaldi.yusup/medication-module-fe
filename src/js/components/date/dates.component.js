( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('biomarkDate',{
			bindings:{
				date:"=",
				invalid:"=",
				dateValid: "="
			},
			controller:"biomarkDateController",
			templateUrl:"/views/date.html"
		})
})();