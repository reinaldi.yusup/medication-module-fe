( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("healthSummaryController",healthSummaryController);

		healthSummaryController.$inject = ["Dashboard","$window"];

		function healthSummaryController( Dashboard , $window){
			var vm = this;

			//set true when you want card open
			vm.accordion={
				//framingham:true,
				// diabetes:true
				// bp:false,
				// bmi:false,
				// drinking:false,
				// smoking:false
			}
			
			Dashboard.init( function(data){
				vm.portal = data;
			});
			vm.back = function(){
				$window.history.back();
			}
		}
})();