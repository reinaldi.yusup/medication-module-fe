( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("bodyMassIndexController",bodyMassIndexController);

		bodyMassIndexController.$inject = ["HttpServer","$state"];

		function bodyMassIndexController( HttpServer, $state ){
			var vm = this;

			vm.processing = true;
			vm.$onInit = function(){
				HttpServer
					.post('v1/doctor/patient/bmi',{id:$state.params.id})
					.response( success );

					function success( res ){
						vm.bmi  = res.message;
						vm.processing = false;
					}
			}
			vm.accordion_toggle = function( tag ){
				if(!vm.bmi.read) return false;
				vm.accordion[tag] = !vm.accordion[tag];
			}
		}
})();