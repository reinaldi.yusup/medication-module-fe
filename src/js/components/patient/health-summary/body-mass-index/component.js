( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("bodyMassIndex",{
			bindings:{
				accordion:"="
			},
			templateUrl:"/views/patient/health-summary/bmi.html",
			controller:"bodyMassIndexController"
		})
})();