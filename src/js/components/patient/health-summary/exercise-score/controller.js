( function(){
	"use strict";


	angular
		.module("Biomark")
		.controller("exerciseScoreController",exerciseScoreController);

		exerciseScoreController.$inject = ["HttpServer", "$state"];


		function exerciseScoreController( HttpServer, $state ){
			var vm = this;

			vm.processing = true;
			vm.$onInit = function(){
				HttpServer
					.post("v1/doctor/patient/exercise",{id:$state.params.id})
					.response( success );

					function success(res){
						vm.data = res;
						vm.processing = false;
					}
			}
			vm.accordion_toggle = function( tag ){
				if(!vm.data.status){
					return false;
				}
				vm.accordion[tag] = !vm.accordion[tag];
			}
		}
})();