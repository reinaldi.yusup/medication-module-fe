( function(){
	"use strict";


	angular
		.module("Biomark")
		.component("exerciseScore",{
			bindings:{
				accordion:"="
			},
			templateUrl:"/views/patient/health-summary/exercise.html",
			controller:"exerciseScoreController"
		})
})();