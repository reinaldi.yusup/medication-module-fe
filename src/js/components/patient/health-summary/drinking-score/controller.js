( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("drinkingScoreController",drinkingScoreController);

		drinkingScoreController.$inject = ["HttpServer","$state"];

		function drinkingScoreController( HttpServer, $state){

			var vm = this;

			vm.processing = true;
			vm.accordion_toggle = function( tag ){
				if(vm.data.field.is_drinking) vm.accordion[tag] = !vm.accordion[tag];
			}
			vm.$onInit = function(){
				HttpServer
					.post('v1/doctor/patient/drinking',{id:$state.params.id})
					.response( success );

					function success(res){
						//console.log(res);
						vm.processing = false;
						vm.data = res;
					}
			}
		}
})();
