( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("drinkingScore",{
			bindings:{
				accordion:"="
			},
			templateUrl:'/views/patient/health-summary/drinking.html',
			controller:"drinkingScoreController"
		})
})();