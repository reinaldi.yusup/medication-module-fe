( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("framinghamScoreController",framinghamScoreController)

		framinghamScoreController.$inject = ["HttpServer","$state"];

		function framinghamScoreController(HttpServer, $state){
			var vm = this;
			//set initial to processing
			vm.processing = true;
			vm.accordion_toggle = function( tag ){
				if(!vm.data.read) return false;
				vm.accordion[tag] = !vm.accordion[tag];
			}
			vm.$onInit = function(){
				HttpServer
					.post("v1/doctor/patient/framingham_score",{id:$state.params.id})
					.response( success );

					function success(res){
						vm.processing = false;
						vm.data = res;
					}
			}
		}
})();