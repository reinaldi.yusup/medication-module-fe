( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("framinghamScore",{
			bindings:{
				accordion:"="
			},
			templateUrl:'/views/patient/health-summary/framingham-score.html',
			controller:'framinghamScoreController'
		})
})();