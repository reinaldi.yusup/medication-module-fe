( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("sleepingScoreController",sleepingScoreController);

		sleepingScoreController.$inject = ["HttpServer","$state"];

		function sleepingScoreController( HttpServer, $state ){
			var vm = this;
			vm.processing = true;
			vm.accordion_toggle = function( tag ){
				if(!vm.data.status){
					return false;
				}
				vm.accordion[tag] = !vm.accordion[tag];
			}
			vm.$onInit = function(){
				HttpServer
					.post("v1/doctor/patient/sleeping",{id:$state.params.id})
					.response( success );

					function success(res){
						
						vm.data = res.message;
						vm.processing = false;
						console.log("sleeping", vm.data)
					}

			}
		}
})();