( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("sleepingScore",{
			bindings:{
				accordion:"="
			},
			templateUrl:"/views/patient/health-summary/sleeping.html",
			controller:"sleepingScoreController"
		})
})();