( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("smokingScoreController",smokingScoreController);

		smokingScoreController.$inject = ["HttpServer","$state"];

		function smokingScoreController( HttpServer, $state){
			var vm = this;
			vm.processing = true;
			vm.accordion_toggle = function( tag ){
				if(vm.data.field.is_smoking == "No"){
					return false;
				}else if(vm.data.field.is_smoking == "-"){
					return undefined;
				}else{
				vm.accordion[tag] = !vm.accordion[tag];
				}
			}
			vm.$onInit = function(){
				HttpServer
					.post('v1/doctor/patient/smoking',{id:$state.params.id})
					.response( success);

					function success(res){
						//console.log(res);
						vm.processing = false;
						vm.data = res;
					}
			}
		}

})();
