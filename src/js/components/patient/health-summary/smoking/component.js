( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("smokingScore",{
			bindings:{
				accordion:"="
			},
			templateUrl:"/views/patient/health-summary/smoking.html",
			controller:"smokingScoreController"
		})
})();