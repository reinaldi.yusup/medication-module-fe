( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("healthSummary",{
			templateUrl:'/views/patient/health-summary.html',
			controller:'healthSummaryController'
		})

})();