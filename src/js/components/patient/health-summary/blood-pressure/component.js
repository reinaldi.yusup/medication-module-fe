( function(){
	"use strict";


	angular
		.module("Biomark")
		.component("bloodPressure",{
			bindings:{
				accordion:"="
			},
			templateUrl:"/views/patient/health-summary/blood-pressure.html",
			controller:"bloodPressureController"
		})
})();