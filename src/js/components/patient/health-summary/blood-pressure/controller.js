( function(){

	"use strict";


	angular
		.module("Biomark")
		.controller("bloodPressureController",bloodPressureController);

		bloodPressureController.$inject = ["HttpServer","$state"];

		function bloodPressureController( HttpServer , $state ){
			var vm = this;

			vm.processing = true;

			vm.$onInit = function(){
				HttpServer
					.post("v1/doctor/patient/blood_pressure",{id:$state.params.id})
					.response( success);

					function success(res){
						vm.bp = res;
						vm.processing = false;
					}
			}
			vm.accordion_toggle = function( tag ){
				if(!vm.bp.read) return false;
				vm.accordion[tag] = !vm.accordion[tag];
			}
		}
})();