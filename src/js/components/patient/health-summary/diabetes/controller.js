( function(){
	"use strict";


	angular
		.module("Biomark")
		.controller("diabetesScoreController",diabetesScoreController);

		diabetesScoreController.$inject = ["Dashboard","HttpServer","$state"];

		function diabetesScoreController(Dashboard,HttpServer, $state){
			var vm = this;

			vm.processing = true;
			vm.accordion_toggle = function( tag ){
				if(!vm.data.read) return false;
				vm.accordion[tag] = !vm.accordion[tag];
			}
			vm.$onInit = function(){
				HttpServer
					.post('v1/doctor/patient/diabetest',{id:$state.params.id})
					.response( success);

					function success(res){
						vm.processing = false;
						vm.data = res;
					}
			}

		}
})();
