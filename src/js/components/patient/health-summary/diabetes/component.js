( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("diabetesScore",{
			bindings:{
				accordion:"="
			},
			templateUrl:"/views/patient/health-summary/diabetes.html",
			controller:"diabetesScoreController"
		})
})();