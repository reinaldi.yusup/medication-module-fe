( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("patientMessageController",patientMessageController)

		patientMessageController.$inject = ["Dashboard","$state","HttpServer"];

		function patientMessageController( Dashboard, $state, HttpServer ){
			var vm = this;
			
			vm.processing = true;
			vm.window = 1;
			Dashboard.init( function( fdata ){
				vm.portal = fdata;
				
			});
			vm.$onInit = function(){
				
				var patient_id = $state.params.id || vm.portal.patient_id;
				HttpServer
					.get('v1/doctor/patient/'+patient_id)
					.response( success );
					function success( res ){
						vm.processing = false;
						vm.patient = res;
					}
			}
			
			var chat_size = 321;
			vm.textbox = "";

			vm.close = function(){
				vm.portal.patient_message = false;
			}
			vm.limit  = chat_size;
			var sms = "";

			// vm.filter_message = function(){
			// 	var str_len = vm.textbox.length;
			// 	if(str_len >= chat_size ){
			// 		return false;
			// 	}else{
			// 		vm.limit = chat_size - str_len;
			// 		sms = vm.textbox;
			// 	}
			// }



			vm.send_message = function(){
				var patient_id = $state.params.id || vm.portal.patient_id;
				HttpServer
					.post("v1/doctor/patient/send_message",{id:patient_id,message:vm.textbox})
					.response( function( res ){
						if(res.status){
							vm.window = 2;
						}else{
							alert(res.message);
						}
					});
			}
		}
})();