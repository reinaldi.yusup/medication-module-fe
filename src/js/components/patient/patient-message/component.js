( function(){
	"use strict";


	angular
		.module("Biomark")
		.component("patientMessage",{
			bindings:{
				patientId:"="
			},	
			templateUrl:"/views/patient/patient-message.html",
			controller:"patientMessageController"
		})
})();