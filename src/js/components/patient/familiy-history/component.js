( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("familyHistory",{
			templateUrl:"/views/patient/family-history.html",
			controller:"familyHistoryController"
		})
})();