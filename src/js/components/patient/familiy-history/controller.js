( function(){
	"use strict";


	angular
		.module("Biomark")
		.controller("familyHistoryController",familyHistoryController);

		familyHistoryController.$inject = ["HttpServer", "$state", "Dashboard","$window"];

		function familyHistoryController( HttpServer, $state , Dashboard, $window){
			var vm = this;

			Dashboard.init( function(data){
				vm.portal = data;
			});
			vm.back = function(){
				$window.history.back();
			}
			vm.$onInit = function(){

				HttpServer
					.post("v1/doctor/patient/family_history",{id:$state.params.id})
					.response(success);

					function success(res){
						vm.patient = res;
					}
			}

		}
})();
