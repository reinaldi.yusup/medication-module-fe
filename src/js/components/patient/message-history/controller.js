( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("messageHistoryController",messageHistoryController);

		messageHistoryController.$inject = ["Dashboard","HttpServer","$state"];

		function messageHistoryController( Dashboard , HttpServer, $state ){
			var vm = this;

			vm.readMessage = function( sms_id ){
				vm.processing = true;
				HttpServer
					.post("v1/doctor/patient/read_message",{id:$state.params.id,message_id:sms_id})
					.response( function(res){
						// vm.messages = res;
						vm.processing = false;
						vm.viewMessage = true;
						if (res.message.message_type == 'sms'){
							res.message.status = 'sent'
						}
						vm.patient = angular.merge(vm.user,res)
					});
					
			}
			function getMessage(){
				HttpServer
					.post("v1/doctor/patient/inbox",{id:$state.params.id})
					.response( function(res){
						vm.messages = res;
						vm.message_type();
					});
			}
			function getUserInfo(){
				HttpServer
					.get('v1/doctor/patient/'+$state.params.id)
					.response( function (res){
						vm.user = res;
					});
			}
			vm.$onInit = function(){
				getMessage();
				getUserInfo();
			}

			vm.open_message = function(){
				vm.portal.patient_message = true;
			}

			Dashboard.init( function( data ){
				vm.portal = data;
				vm.portal.reLoadPatientMessage = getMessage;
				
			});
			vm.close_message = function(){
				vm.viewMessage = false;
			}
			vm.message_type = function(){
				for (var i=0; i<vm.messages.length; i++) {
					if (vm.messages[i].message_type == 'sms'){
						vm.messages[i].status = 'sent';
					}
				}
			}
		}
})();