( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("messageHistory",{
			// bindings:{
			// 	patientId:"="
			// },
			templateUrl:"/views/patient/message-history.html",
			controller:"messageHistoryController"
		})
})();