( function(){
	"use strict";

	angular
		.module("Biomark")
		.component("updateProfile",{
			templateUrl:"/views/patient/update-profile.html",
			controller:"updateProfileController"
		})
})();