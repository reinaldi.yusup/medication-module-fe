( function(){
  "use strict";

  angular
      .module("Biomark")
      .controller("updateProfileController",updateProfileController);

      updateProfileController.$inject = ["$state","HttpServer", "EOrderService"];

      function updateProfileController( $state, HttpServer, EOrderService ){
          var vm = this;

          vm.$onInit = function(){
            vm.patient_data = EOrderService.data.verify_patient_information;
            EOrderService.update_verify_patient_information = function(data) {
              vm.patient_data = data;
            }
            vm.patient_id = $state.params.id
            if($state.params.id != 0){
              //fetch patient_data by id
              HttpServer
                .post("v2/doctor/eorder/patient_data",{patient_id:$state.params.id})
                .response(function(data){
                  vm.is_ready = true;
                  EOrderService.data.verify_patient_information = data;
                  vm.patient_data  = data
                })

              vm.patient_data = EOrderService.data.verify_patient_information;
              console.log("patient data",vm.patient_data);
            }
          }
      }
})();