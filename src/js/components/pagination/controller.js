( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("paginationController",paginationController);

		paginationController.$inject = ["Dashboard"];

		function paginationController(Dashboard){
			var vm = this;
			
			var local_configurations = {
				item_per_page:10,
				padding_per_page:4,
				page_position:1
			};
			vm.go = function(){
				if(vm.pagination.page_position == 0){
					vm.pagination.page_position = 1;
				}
				vm.paginate( vm.pagination.page_position );
				init();
			}
			vm.quantity_changed = function(){
				if(vm.pagination.page_position > vm.pagination.total_pages){
					vm.pagination.page_position = vm.pagination.total_pages;
				}
			}
			vm.first_page = function(){
				vm.paginate( 1 );
				init();
			}
			vm.last_page = function(){
				vm.paginate(vm.pagination.total_pages);
				init();
			}
			Dashboard.init( function(data){
				vm.portal = data;
				vm.portal.pagination = 1;
			});	
			vm.$onInit = function(){
				vm.pagination = angular.merge(local_configurations,vm.config);
				init();
				vm.portal.pagination = vm.pagination.page_position;
			}
			vm.btn_previous = false;
			vm.btn_next = true;
			vm.btn_dotdot = true;
			vm.btn_last = true;
			vm.page_elements = [];
			
			function init(){
				
				var last_show_page = vm.pagination.page_position + vm.pagination.padding_per_page-1;
				vm.btn_dotdot = (last_show_page >= vm.pagination.total_pages) ? false : true;
				vm.btn_previous = (vm.pagination.page_position > 1 );
				vm.btn_next = (vm.pagination.page_position != vm.pagination.total_pages );

				if(last_show_page <= vm.pagination.total_pages){
					vm.page_elements.length = 0;
					if(vm.pagination.total_pages > vm.pagination.padding_per_page){
						for( var i = 1;i <= vm.pagination.total_pages; i++ ){
							if(i >= vm.pagination.page_position && i <= last_show_page){
								vm.page_elements.push(i);
							}
						}

					}else{
						for(var i = 1;i <= vm.pagination.total_pages; i++ ){
							vm.page_elements.push(i);
						}
					}
				}else{
					vm.page_elements.length = 0;
					var partial_index = vm.pagination.total_pages - vm.pagination.padding_per_page +1;
					for( var i = 1;i <= vm.pagination.total_pages; i++ ){
						if(i >= partial_index && i <= vm.pagination.total_pages){
							vm.page_elements.push(i);
						}
					}
				}
				
				
			}
			
			
			vm.previous = function(){
				vm.portal.pagination -=1;	
				vm.pagination.page_position -=1;
				vm.paginate( vm.portal.pagination, vm.pagination.type );
				init();
			}
			vm.next = function(){
				vm.portal.pagination +=1;
				vm.pagination.page_position +=1;
				vm.paginate( vm.portal.pagination,vm.pagination.type );
				init();
			}
			vm.page_click = function( page ){
				vm.portal.pagination = page;
				vm.pagination.page_position = page;
				vm.paginate( vm.portal.pagination, vm.pagination.type );
				init();
			}
		}
})();