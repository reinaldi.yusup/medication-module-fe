( function(){
	"use strict";


	angular
		.module("Biomark")
		.component("pagination",{
			bindings:{
				config:"=",
				paginate:"="
			},
			templateUrl:"/views/pagination.html",
			controller:"paginationController"
		})
})();