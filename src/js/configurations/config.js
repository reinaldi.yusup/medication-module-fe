( function(){
	//OEP
	"use strict";

	angular
		.module("Biomark")
		.constant("BiomarkConfig",{
			host:"%HOST_API%",
			socket:"%SOCKET_API%",
			assets:"%PUBLIC_ASSETS%",
			countries: countryList(),
      current_country: currentCountry(),
      mobile_configs: mobileConfigs(),
      portal_type_id: portalTypeId(),
      show_privacy: showPrivacy(),
      group_type: groupType(),
      email_domain: emailDomain(),
      dial_code: dialCode(),
      country_code: countryCode(),
			// assets:"https://demo.biomarking.org/profile/"
			assets:"https://assets.biomarking.com/"
		})

    function currentCountry() {
      var countries = countryList();
      if ("%PORTAL_TYPE%" === 'innoquest') {
        return countries[3];
      } if ("%PORTAL_TYPE%" === 'gribbles') {
        return countries[0];
      }
    }

    function countryList() {
      return [
				{ "id":1,"name": "Malaysia", "dial_code": "+60", "code": "MY" },
				{ "id":5,"name": "Indonesia", "dial_code": "+62", "code": "ID" },
				{ "id":2,"name": "Philippines", "dial_code": "+63", "code": "PH" },
				{ "id":3,"name": "Singapore", "dial_code": "+65", "code": "SG" },
			]
    }

    function mobileConfigs(country_id) {
      return [
        {
          country_id: 2, mobile_placeholder: "915 123 4567",
          mobile_regex: "^9[0-9]{1,9}$",mobile_min: 10, mobile_max: 10
        },
        {
          country_id: 3, mobile_placeholder: "1234 4567",
          mobile_regex: "^[0-9]{1,8}$", mobile_min: 8, mobile_max: 8,
        },
        {
          country_id: 1, mobile_placeholder: "12 1234 5678",
          mobile_regex: "(^[0-9]{1,9})|(^[0-9]{1,10})", mobile_min: 8, mobile_max: 11,
        },
        {
          country_id: 5, mobile_placeholder: "12 1234 5678",
          mobile_regex: "(^[0-9]{1,9})|(^[0-9]{1,10})", mobile_min: 8, mobile_max: 12,
        }
      ]
    }

    function portalTypeId() {
      if ("%PORTAL_TYPE%" === 'innoquest') {
        return 0
      } if ("%PORTAL_TYPE%" === 'gribbles') {
        return 1
      }
    }

    function showPrivacy() {
      if ("%PORTAL_TYPE%" === 'innoquest') {
        return false;
      } if ("%PORTAL_TYPE%" === 'gribbles') {
        return true;
      }
    }

    function groupType() {
      if ("%PORTAL_TYPE%" === 'innoquest') {
        return 'quest-doctor'
      } if ("%PORTAL_TYPE%" === 'gribbles') {
        return 'doctor'
      }
    }

    function emailDomain() {
      if ("%PORTAL_TYPE%" === 'innoquest') {
        return '@iqlab.com.sg'
      } if ("%PORTAL_TYPE%" === 'gribbles') {
        return '@bmgribbles.com.my'
      }
    }

    function dialCode() {
      if ("%PORTAL_TYPE%" === 'innoquest') {
        return "+65"
      } if ("%PORTAL_TYPE%" === 'gribbles') {
        return "+60"
      }
    }

    function countryCode() {
      if ("%PORTAL_TYPE%" === 'innoquest') {
        return "SG"
      } if ("%PORTAL_TYPE%" === 'gribbles') {
        return "MY"
      }
    }
})();
