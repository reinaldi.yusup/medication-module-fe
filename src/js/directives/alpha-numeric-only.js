

( function(){

	"use strict";

	angular
        .module('Biomark')
        .directive('onlyAlphaNumeric', function() {
            return {
              require: 'ngModel',
              link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                  var transformedInput = text.replace(/[^A-Za-z0-9 ]/gi, '');

                  if(transformedInput.startsWith(" ")){
                    transformedInput = '';
                  }
                  // if(transformedInput.length > 16){
                  //   transformedInput = transformedInput.slice(0, -1)
                  // }
                  if(transformedInput !== text) {
                      ngModelCtrl.$setViewValue(transformedInput);
                      ngModelCtrl.$render();
                  }
                  return transformedInput;
                }
                ngModelCtrl.$parsers.push(fromUser);
              }
            };
          });
})();
