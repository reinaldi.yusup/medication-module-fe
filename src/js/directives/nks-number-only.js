( function(){

	"use strict";

	angular
		.module('Biomark')
			.directive('nksOnlyNumber', function () {
			return {
				restrict: 'EA',
				require: 'ngModel',
				link: function (scope, element, attrs, ngModel) {   
					scope.$watch(attrs.ngModel, function(newValue, oldValue) {
					if (!isNaN(newValue)) {
						
						var spiltArray = String(newValue).split("");
						
						if(attrs.allowNegative == "false") {
							if(spiltArray[0] == '-') {
								newValue = newValue.replace("-", "");
								ngModel.$setViewValue(newValue);
								ngModel.$render();
							}
						}

						if(attrs.allowDecimal == "false") {
							newValue = parseInt(newValue);
							ngModel.$setViewValue(newValue);
							ngModel.$render();
						}

						if(attrs.allowDecimal != "false") {
							if(attrs.decimalUpto) {
								var n = String(newValue).split(".");
								if(n[1]) {
									var n2 = n[1].slice(0, attrs.decimalUpto);
									newValue = [n[0], n2].join(".");
									ngModel.$setViewValue(newValue);
									ngModel.$render();
								}
							}
						}
						if (spiltArray.length === 0) return;
						if (spiltArray.length === 1 && (spiltArray[0] == '-' || spiltArray[0] === '.' )) return;
						if (spiltArray.length === 2 && newValue === '-.') return;
						
						if(attrs.maximumValue){
							var _newValue =  parseInt(newValue);
							var maximumValue 	= parseInt(attrs.maximumValue)
							if(_newValue <= maximumValue){
								ngModel.$setViewValue(_newValue);
								ngModel.$render();
							}else{
								ngModel.$setViewValue(oldValue);
								ngModel.$render();
							}
						}
						
					/*Check it is number or not.*/
						if (isNaN(newValue)) {
							// console.log(newValue);
							
						}
					  
					}else{
						if(oldValue && !isNaN(oldValue)){
							// if (!isNaN(newValue)) {
							ngModel.$setViewValue(oldValue);
							ngModel.$render();
						}
						
					}
				});
			}
		};
	});
})();
