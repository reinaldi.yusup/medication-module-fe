( function(){

	"use strict";


	angular
		.module("Biomark")
		.controller("patientController",patientController);

		patientController.$inject=["Dashboard","resolve_userinfo","Personal","$state","HttpServer", "$stateParams", "$localStorage"];

		function patientController(Dashboard,resolve_userinfo, Personal, $state ,HttpServer,$stateParams, $localStorage){
			var pc = this;
			resolve_userinfo.response( init ); 
			function init( res ){
				Personal.init( function( data ){
					angular.merge(data,res.personal);
				});
			}
		}
})();