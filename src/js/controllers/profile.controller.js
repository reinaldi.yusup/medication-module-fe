( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("profileController",profileController);

		profileController.$inject = ["resolve_userinfo","Personal","DateDropdown","Country","HttpServer","$timeout","BiomarkConfig","$state"];

		function profileController( resolve_userinfo,Personal,DateDropdown,Country,HttpServer,$timeout,BiomarkConfig, $state){
			var pc = this;		
			pc.boolean_country_id = false;
			resolve_userinfo.response( init );
			pc.tabs = [
				{id:0,label:"Personal",model:"profile"},
				{id:1,label:"Contact",model:"contact"},
				// {id:2,label:"Notification",model:"notification"},
				// {id:2,label:"QR",model:"qr"}
			];
			pc.temp={};
			pc.save_ok = false;
			pc.prefixes= ["Mr","Mrs","Ms","Dr","Nurse","Prof"];
			pc.is_date_valid = true;

			pc.isFormValid = function(validity){
				pc.validity = !validity;
				$timeout(function(){
					pc.validity = false;
				},3200);
				pc.error_message = false;
			}
			pc.close = function(){
				pc.validity = false;
				pc.save_ok = false;
			}
			pc.fileDetected = function(){
				var valid = false;
				$timeout( function(){
					if(pc.temp.picture.filesize > 300000){
						var valid = false;
						alert("maximum file size is 300Kb");
						return false;
					}
					switch(pc.temp.picture.filetype){
						case 'image/png':
							pc.temp.prefix = "data:image/png;base64,"
							var valid = true;
							break;
						case 'image/jpeg':
							pc.temp.prefix = "data:image/jpeg;base64,"
							var valid = true;
							break;
						case 'image/jpg':
							pc.temp.prefix = "data:image/jpg;base64,"
							var valid = true;
							break;
						default:
							var valid = false
							alert('unsupported format');
							break;
			        }
			        if(valid){
			        	pc.has_picture = false;
			        	pc.data.picture = pc.temp.picture;	
			        }
					
				},1000);
			}
			function init( data ){
				var session_country_id = HttpServer.get_country_id();
				pc.session_id = 1;
				if(session_country_id != undefined){
					pc.session_id = session_country_id;
				}	
				Personal.init( function( _data ){
					pc.has_picture = (data.personal.profile.data.picture == null) ? false : true;
					pc.personal = angular.merge(_data,data.personal);

					if(angular.isDefined(pc.personal.profile.data.prefix)){
						pc.default_prefix = pc.personal.profile.data.prefix;
					}			

				});
				pc.pp_hover = false;

				pc.profile_picture_toggle = function( state ){
					pc.pp_hover = state;
				}
				pc.tab_position = 0;

				pc.update_notification = function(){
					HttpServer
						.post("v1/doctor/notification",{notification:pc.data})
						.response( success);

						function success(res){
							pc.save_ok = true;
						}
				}
				pc.update_contact = function(){
					HttpServer
						.post("v1/doctor/contact/update",{contact:pc.data})
						.response( success );

						function success(res){
							pc.save_ok = true;
							$timeout(function() {
								pc.update_message = true;
							}, 3000);
						}
						pc.update_message = false;
				}
				pc.update_profile = function(){	
					
					var dateNow = new Date();
					var inputtedDate = DateDropdown.getCurrentDate();

					if(inputtedDate > dateNow){
						pc.validity = true;
						pc.is_date_valid = false;
						return false; 
					}
					pc.is_date_valid = true;
					
					var parseDate = moment(DateDropdown.getCurrentDate()).format('YYYY-MM-DD');
					pc.data.birth_date = parseDate;

					pc.data.prefix = pc.default_prefix;
					if (pc.data.country_id == 2) {
						pc.data.country_id = 5;
					}else if(pc.data.country_id == 1){
						pc.data.country_id = 1;
					}else if(pc.data.country_id == 3){
						pc.data.country_id = 2;
					}else if(pc.data.country_id == 4){
						pc.data.country_id = 3;
					}
					HttpServer
						.post("v1/doctor/profile",{profile:pc.data})
						.response( success );

						function success(res){
							pc.save_ok = true;
							$timeout(function() {
								pc.update_message = true;
							}, 3000);
							Personal.data.profile.data.country_id = pc.data.country_id;
							$state.reload();
							
						}
						pc.update_message = false;
				}

				pc.tabChanged = function( id ){
					pc.data = {};
					pc.tab_position = id;
					var session_country_id = HttpServer.get_country_id();	
					switch( id ){
						case 0:
							pc.data = angular.copy(data.personal[pc.tabs[id].model].data);
							if (pc.data.country_id == 5) {
								pc.data.country_id = 2;
							}else if(pc.data.country_id == 1){
								pc.data.country_id = 1;
							}else if(pc.data.country_id == 2){
								pc.data.country_id = 3;
							}else if(pc.data.country_id == 3){
								pc.data.country_id = 4;
							}
							break;
						case 1:
							pc.data = angular.copy(data.personal[pc.tabs[id].model].data);
							pc.session_id = session_country_id;
							
							if(pc.data.mobile.startsWith("+60")){
								pc.data.country_id = 1;
								pc.data.mobile = pc.data.mobile.replace("+60","");
							}
							if(pc.data.mobile.startsWith("+63")){
								pc.data.country_id = 2;
								pc.data.mobile = pc.data.mobile.replace("+63","");
							}
							if(pc.data.mobile.startsWith("+65")){
								pc.data.country_id = 4;
								pc.data.mobile = pc.data.mobile.replace("+65","");
							}
							if(pc.data.mobile.startsWith("+62")){
								pc.data.country_id = 3;
								pc.data.mobile = pc.data.mobile.replace("+62","");
							}

							if (pc.data.country_id == 5) {
								pc.data.country_id = 2;
							}else if(pc.data.country_id == 1){
								pc.data.country_id = 1;
							}else if(pc.data.country_id == 2){
								pc.data.country_id = 3;
							}else if(pc.data.country_id == 3){
								pc.data.country_id = 4;
							}

							break;
						// case 2:
						// 	pc.data = angular.copy(data.personal[pc.tabs[id].model].data);
						// 	break;
						case 2:
							pc.data =	 angular.copy(data.personal[pc.tabs[id].model].data);
							break;
					}	
						
				}
				pc.tabChanged(pc.tab_position);
				
				// if(data != undefined){
				// 	pc.data_mobile = angular.copy(data.personal[pc.tabs[1].model].data);
				// 	if (data.personal.profile.data.country_id == 5) {
				// 		pc.data.country_id = 2;
				// 	}else if(data.personal.profile.data.country_id == 1){
				// 		pc.data.country_id = 1;
				// 	}else if(data.personal.profile.data.country_id == 2){
				// 		pc.data.country_id = 3;
				// 	}else if(data.personal.profile.data.country_id == 3){
				// 		pc.data.country_id = 4;
				// 	}
				// }
			}
			pc.select_prefix = function(){
                pc.is_visible = !pc.is_visible;
            }
            pc.onValueChanged = function( prefix ){
				pc.default_prefix = pc.prefixes[prefix];
			}
			pc.select_gender = function($event,gender){
				var keyCode = $event.which || $event.keyCode;
				if(keyCode === 9){
					pc.addFocus($event)
				}
                if(keyCode === 13 || keyCode === 32){
					$event.preventDefault();
                    pc.data.gender_id = gender;
                }
			}

			var i = 0;
			pc.validate_country_id = function (id_country) {
				if(i == 0){
					if (id_country == 5) {
						pc.data.country_id = 2;
					}else if(id_country == 1){
						pc.data.country_id = 1;
					}else if(id_country == 2){
						pc.data.country_id = 3;
					}else if(id_country == 3){
						pc.data.country_id = 4;
					}
					i++;
				}
				return true;
			}
		}
})();