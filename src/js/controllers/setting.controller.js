(function () {
	"use strict";


	angular
		.module("Biomark")
		.controller("settingController", settingController)

	settingController.$inject = ["resolve_userinfo", "Personal", "HttpServer", "$timeout", "User", "$state"];


	function settingController(resolve_userinfo, Personal, HttpServer, $timeout, User, $state) {
		var sc = this;
		sc.confirmPass = false;
		sc.processing = false;
		sc.showPasswordCurrent = false;
		sc.showPasswordNew = false;
		sc.showPasswordVerify = false;
		sc.tab_position = 0;
		sc.language = false;
		sc.QRPrint = false;
		sc.isEdit = false;
		sc.innoquest = "innoquest";
		sc.gribbles = "gribbles";
		sc.sample_data = {};
		sc.checkDisabled = true;
		sc.result_sent_modal = false;
		sc.result_show_modal = false;
		sc.tenCharPass = new RegExp("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])([a-zA-Z0-9@$!%*?&]{10,})$");
		sc.sixteenCharPass = new RegExp("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])([a-zA-Z0-9@$!%*?&]{16,})$");
		sc.varifyPassword = true;
		sc.tabs = [
			// {id:0, label:"Account"},
			// {id:1, label:"Password"},
			{ id: 0, label: "General Settings" },
			{ id: 2, label: "User Permissions" },
			{ id: 3, label: "COVID-19 Sampling Price"}
			//{id:2, label:"Notification"} //deprecated functions
		];
		sc.itests = [
			{ id:1, test_name: "Rapid Antigent Test (Blood)", sample_charge: "10 SGD", status: "Active", action: "Edit"},
			{ id:2, test_name: "Rapid Antigen Test (Swab)", sample_charge: "30 SGD", status: "Inactive", action: "Edit"},
			{ id:3, test_name: "Rapid Antigent Test (Saliva)", sample_charge: "40 SGD", status: "Active", action: "Edit"},
			{ id:4, test_name: "Rapid RT-PCR Test (Swab) ", sample_charge: "40 SGD", status: "Active", action: "Edit"},
			{ id:5, test_name: "RT-PCR (Saliva)", sample_charge: "30 SGD", status: "Active", action: "Edit"},
			{ id:6, test_name: "RT-PCR (Swab <24hr) ", sample_charge: "20 SGD", status: "Active", action: "Edit"},
			{ id:7, test_name: "RT-PCR (Swab <3hr) ", sample_charge: "40 SGD", status: "Active", action: "Edit"},
			{ id:8, test_name: "Total Qualitative Antibody", sample_charge: "30 SGD", status: "Active", action: "Edit"},
			{ id:9, test_name: "Total Quantitative Antibody", sample_charge: "30 SGD", status: "Active", action: "Edit"}
		];
		sc.gtests = [
			{ id:1, test_name: "Rapid Antigent Test (Blood)", sample_charge: "40 MYR", status: "Active", action: "Edit"},
			{ id:2, test_name: "Rapid Antigen Test (Swab)", sample_charge: "20 MYR", status: "Inactive", action: "Edit"},
			{ id:3, test_name: "Rapid Antigent Test (Saliva)", sample_charge: "10 MYR", status: "Active", action: "Edit"},
			{ id:4, test_name: "Rapid RT-PCR Test (Swab) ", sample_charge: "50 MYR", status: "Active", action: "Edit"},
			{ id:5, test_name: "RT-PCR (Saliva)", sample_charge: "20 MYR", status: "Active", action: "Edit"},
			{ id:6, test_name: "RT-PCR (Swab <24hr) ", sample_charge: "10 MYR", status: "Active", action: "Edit"},
			{ id:7, test_name: "RT-PCR (Swab <3hr) ", sample_charge: "30 MYR", status: "Active", action: "Edit"},
			{ id:8, test_name: "Total Qualitative Antibody", sample_charge: "60 MYR", status: "Active", action: "Edit"},
			{ id:9, test_name: "Total Quantitative Antibody", sample_charge: "20 MYR", status: "Active", action: "Edit"}
		]
		sc.close_release_notice = function () {
			sc.result_sent_modal = false;
			sc.result_show_modal = false;
			console.log("sc.result_sent_modal -> "+sc.result_sent_modal);
		}
		sc.openAddSamplingPopup = function() {
			sc.result_sent_modal = true;
			sc.isEdit = false;
			sc.result_show_modal = false;
			console.log("sc.result_sent_modal -> "+sc.result_sent_modal);
		}
		sc.openUpdatePopups = function() {
			sc.isEdit = true;
			sc.result_sent_modal = true;
			sc.result_show_modal = false;
		}
		sc.addSample = function() {
			sc.result_sent_modal = false;
			sc.result_show_modal = true;
			console.log("sc.testName ->"+sc.testName);
			console.log("sc.testCharge ->"+sc.testCharge);
			console.log("sc.isActive ->"+sc.isActive);
			console.log("user id ->"+sc.personal.id);
			var active = new Boolean(sc.isActive);

			sc.sample_data.user_id = sc.personal.id;
			sc.sample_data.test_name = sc.testName;
			sc.sample_data.sampling_price = sc.testCharge;
			sc.sample_data.status = active.toString();;
			console.log(sc.sample_data);
			// HttpServer
			// 		.post_v2('api/v2/doctor/bookings', { user: sc.sample_data })
			// 		.then(success)

			// function success(success) {
			// 	console.log(success);
			// }
			// function error(error) {
			// 	console.log(error);
			// }
			
		}
		sc.updateSample = function() {
			sc.result_sent_modal = false;
			sc.result_show_modal = true;
		}
		sc.checkActive = function() {
			sc.isActive=!sc.isActive;
			if(sc.testName != undefined && sc.testName != "" && sc.testCharge != undefined && sc.testCharge != "" && sc.isActive === true){
				sc.checkDisabled = false;
			}else {
				sc.checkDisabled = true;
			}
		}
		sc.checkTestProvider = function() {
			if(sc.testName != undefined && sc.testName != "" && sc.testCharge != undefined && sc.testCharge != "" && sc.isActive === true){
				sc.checkDisabled = false;
			}else {
				sc.checkDisabled = true;
			}
		}
		sc.checkTestCharge = function() {
			if(sc.testName != undefined && sc.testName != "" && sc.testCharge != undefined && sc.testCharge != "" && sc.isActive === true){
				sc.checkDisabled = false;
			}else {
				sc.checkDisabled = true;
			}
		}
    sc.data = {};
		sc.passwordStrength = "";
		sc.emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
		sc.save_ok = false;
		resolve_userinfo.response(init);

		sc.togglePassword = function (toggle) {
			switch (toggle) {
				case 'current':
					sc.showPasswordCurrent = !sc.showPasswordCurrent;
					break;
				case 'new':
					sc.showPasswordNew = !sc.showPasswordNew;
					break;
				case 'verify':
					sc.showPasswordVerify = !sc.showPasswordVerify;
					break;
			}
		}
		sc.close = function () {
			sc.save_ok = false;
		}
		function reset() {
			$timeout(function () {
				sc.processing = false;
				delete sc.error;
			}, 3000)

		}
		function resetPopupError() {
			$timeout(function () {
				sc.pass_invalid = false;
				delete sc.error_message;
			}, 3000)

		}
		sc.update_password = function (credentials) {
			sc.processing = true;
			// if(credentials.new_password != credentials.verify_password){
			// 	sc.error = "Please verify Password correctly.";
			// 	reset();
			// 	return false;
			// }

			if (sc.validate(credentials)) {
				credentials.password = angular.copy("0000" + credentials._password);
				credentials.previous_password = angular.copy("0000" + credentials._password);
				credentials.new_password = angular.copy("0000" + credentials._new_password);
				credentials.verify_password = angular.copy("0000" + credentials._verify_password);
				HttpServer
					.post_v2('v1/users/change_password_doctor', { user: credentials })
					.then(success)
					.finally(reset);

				function success(res) {
					if (res.data.message == "Update successful") {
						HttpServer.post_v2("v1/sessions/destroy").then(
							function (res) {
								User.logout()
								User.set_token(0);
								$state.go("login");
							}
						);
					} else {
						sc.error_message = res.data.message;
						sc.pass_invalid = true;
						resetPopupError();
					}
				}
			}
		}
		sc.validate = function (data) {
			if (sc.passwordStrength == 'LOW') {
				sc.error_message = "Weak password. Use a stronger password.";
				sc.pass_invalid = true;
				resetPopupError();
				return false;
			}
			if (data == undefined) {
				sc.error_message = "Both Fields cannot be empty.";
				sc.pass_invalid = true;
				resetPopupError();
				return false;
			}
			if (data._password == undefined || data._password == '') {
				sc.error_message = "Please input current password.";
				sc.pass_invalid = true;
				resetPopupError();
				return false;
			}
			if (data._new_password == undefined || data._new_password == '') {
				sc.error_message = "Please input new password.";
				sc.pass_invalid = true;
				resetPopupError();
				return false;
			}
			if (data._new_password != data._verify_password) {
				sc.error_message = "Please verify Password correctly.";
				sc.varifyPassword = false;
				sc.pass_invalid = true;
				resetPopupError();
				return false;
			}
			if (!sc.tenCharPass.test(data._new_password)) {
				sc.error_message = "Your password must be at least 10 characters, include one lowercase letter, one uppercase letter, one number and one special character.";
				sc.pass_invalid = true;
				resetPopupError();
				return false;
			}
			if (data._password == data._new_password) {
				sc.error_message = "Please enter a new password.";
				sc.pass_invalid = true;
				resetPopupError();
				return false;
			}
			return true;
		}
		sc.validateEmail = function () {
			
			if (sc.personal.contact.data.email_address != undefined) {
				sc.emailStatus = sc.emailPattern.test(sc.personal.contact.data.email_address);
				if(sc.emailStatus == false){
					sc.email = sc.personal.contact.data.email_address;
				}
			} else {
				sc.emailStatus = false;
			}
		}
		sc.save = function () {
			sc.settings.globals = sc.setting;
			HttpServer
				.post('v1/doctor/setting', { setting: sc.settings })
				.response(success)
			function success(res) {
				sc.save_ok = true;
			}
			//console.log(sc.settings,sc.setting);
		}
		function init(data) {
			HttpServer
				.get("v1/doctor/setting")
				.response(function (data) {
					sc.settings = data.settings;
          sc.child_users = data.child_users;
          sc.permission_data = data.child_users_permissions;
          sc.permissions = data.permissions;
					angular.forEach(sc.settings.global, function (_cdata) {
						if (_cdata.status) {
							sc.setting = _cdata.id;
						}
					});
				});
      
			Personal.init(function (_data) {
				sc.has_picture = (_data.profile.data.picture == null) ? false : true;
				sc.personal = angular.merge(_data, data.personal);
				sc.email = sc.personal.contact.data.email_address;
				sc.mobile = sc.personal.contact.data.mobile;
			});
		}
		sc.update_email = function (account) {
			HttpServer
				.post('v1/doctor/contact', { contact: account })
				.response(success);

			function success(res) {
				if (res.status) {
					sc.save_ok = true;
					$timeout(function () {
						sc.update_message = true;
					}, 3000);
					sc.update_message = false;
				} else {
					// alert(res.message);
				}
			}

		}

    sc.addPermission = function(data, user_id) {
      HttpServer.post_v2("/v2/doctor/setting/update_permission", {permission: data, user_id: user_id}).then(function(res){
        console.log(res)
        location.reload();
      });
    }

		sc.isFormValid = function (validity) {
			sc.validity = !validity;
			$timeout(function () {
				sc.validity = false;
			}, 3000)
		}
		sc.tabChanged = function (id) {
			sc.tab_position = id;
		}

    sc.loadChildUsers = function () {
      HttpServer.get_v2("/v2/doctor/setting/child_users").then(function(res){
          sc.users = res.data;
        }
      );
    }

		sc.toggle_Language = function () {
			sc.language = !sc.language;
		}

		sc.openCloseQRPrint = function () {
			sc.QRPrint = !sc.QRPrint;
		}

		sc.passwordStrengthSetter = function () {
			if (sc.temp2._new_password == '') {
				sc.passwordStrength = "";
				return true;
			}
			if (sc.tenCharPass.test(sc.temp2._new_password) && !sc.sixteenCharPass.test(sc.temp2._new_password)) {
				sc.passwordStrength = "MEDIUM";
				return true;
			}
			if (sc.sixteenCharPass.test(sc.temp2._new_password)) {
				sc.passwordStrength = "HIGH";
				return true;
			}
			sc.passwordStrength = "LOW"
		}

		sc.updateContact = function () {
			sc.validateEmail();
			if (!sc.emailStatus) {
				sc.error_message = "Please enter valid email.";
				sc.pass_invalid = true;
				resetPopupError();
				return false;
			}

			if (sc.email != sc.personal.contact.data.email_address || sc.mobile !=sc.personal.contact.data.mobile ) {
				HttpServer
					.post("/v1/doctor/contact/update",
						{ mobile: sc.personal.contact.data.mobile, email_address: sc.personal.contact.data.email_address })
					.response(function success(res) {
						sc.email = sc.personal.contact.data.email_address;
						sc.save_ok = true;
						if (sc.temp2 == undefined) {
							$timeout(function () {
								sc.update_message = true;
							}, 3000);
						}
					});
			}

			if (sc.temp2 != undefined) {
				if (sc.email != sc.personal.contact.data.email_address ||  sc.mobile != sc.personal.contact.data.mobile) {
					setTimeout(() => {

					}, 1000)
				}
				sc.update_password(sc.temp2)
			}
			// $state.reload();
		}

		sc.printQR = function () {
			var tempQRElement = document.getElementById('QRCode').innerHTML;
			document.getElementById('QRData').style.marginLeft = "25%";
			document.getElementById('QRTitle').style.marginLeft = "40%";

			document.getElementById('QRCode').querySelector('img').style.marginLeft = "25%";
			var canvas = document.getElementsByClassName('ng-hide');
			var qr = document.querySelector('img');
			if (canvas[0] != undefined) {
				canvas[0].remove();
			}
			var contentToPrint = document.getElementById('QRCode').innerHTML;
			var windowPopup = window.open('', '_blank', 'width=1000,height=1000');
			windowPopup.document.open();
			windowPopup.document.write('<html><head></head><body style="border: 1px solid black;padding: 10px;height:550px;width:550px">' +
				contentToPrint + '</body></html>');
			windowPopup.print();
			setTimeout(function () { windowPopup.close(); }, 500);

			windowPopup.onafterprint = function () {
				document.getElementById('QRCode').innerHTML = tempQRElement;
			};
			windowPopup.document.close();

		}


	}
})();