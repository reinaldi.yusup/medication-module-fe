( function(){
	"use strict";

	angular
		.module("Biomark")
		.controller("reportBugController",reportBugController)

		reportBugController.$inject =["Personal","resolve_userinfo","$timeout", "HttpServer"];

		function reportBugController(Personal, resolve_userinfo, $timeout, HttpServer){

			resolve_userinfo.response(init)

			var rb = this;

			rb.sent_popup = false

			// rb.report = function(){
			// 	rb.sent_popup = true;
			// }
			rb.fileDetected = function(){
				var valid = false;
				$timeout( function(){
					if(rb.temp.picture.filesize > 300000){
						rb.temp.picture = {}
						var valid = false;
						alert("maximum file size is 300Kb");
						return false;
					}
					switch(rb.temp.picture.filetype){
						case 'image/png':
							rb.temp.prefix = "data:image/png;base64,"
							var valid = true;
							break;
						case 'image/jpeg':
							rb.temp.prefix = "data:image/jpeg;base64,"
							var valid = true;
							break;
						case 'image/jpg':
							rb.temp.prefix = "data:image/jpg;base64,"
							var valid = true;
							break;
						default:
							rb.temp.picture = {}
							var valid = false
							alert('unsupported format');
							break;
			        }
			        if(valid){
			        	rb.report.picture = rb.temp.picture;	
			        }
					
				},1000);
		    }
			rb.close = function(){
				rb.sent_popup = false;
			}
			function init( res ){

				Personal.init( function( data ){
					rb.personal = angular.merge(data,res.personal);
				});
			}
			rb.send = function(){
				HttpServer
					.post("v1/doctor/report",{report:rb.report})
					.response(success)
					
					function success(res){
						rb.sent_popup = true;
					}
			}
		}
})();