( function(){
    "use strict"


    angular
        .module("Biomark")
        .controller("policiesController",policiesController);

    
    policiesController.$inject = ["Personal","resolve_userinfo","$state","BiomarkConfig"];


    function policiesController(Personal,resolve_userinfo, $state, BiomarkConfig){

        resolve_userinfo.response( init );

        var hc = this;

        hc.tab_position = 0;

        hc.tabs = [
            {id:0,label:"Terms of Service",model:"profile"},
            // {id:1,label:"Data Privacy",model:"contact"},
        ]

        hc.tabChanged = function(tab){
            hc.tab_position = tab;
            hc.policy = tab == 0 ? BiomarkConfig.tos : BiomarkConfig.privacy;
        }

        function init( res ){

            Personal.init( function( data ){
                hc.personal = angular.merge(data,res.personal);
                hc.country = $state.params.country;
                hc.policy = BiomarkConfig.tos;
				BiomarkConfig.meta_listener = function(){
					hc.policy = BiomarkConfig.tos;
				}
            });
        }
    }
})();