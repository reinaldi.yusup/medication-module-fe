( function(){
    "use strict";

    angular
        .module("Biomark")
        .controller('verifyController',verifyController);

        verifyController.$inject = ["$state","HttpServer"];

        function verifyController( $state,HttpServer ){
            var vc = this;

            vc.loading = true;
            HttpServer
                .post('v1/doctor/user/verify',{code:$state.params.code})
                .response( success, error );

                function success(res){
                    vc.loading = false;
                }

                function error(err){
                    vc.loading = false;
                }
        }
    
})();