(function(){
    "use strict";

    angular
        .module("Biomark")
        .controller("doctorQrCodeController",doctorQrCodeController);
        
        doctorQrCodeController.$iject = ["resolve_userinfo","Personal","HttpServer","Dashboard","BiomarkConfig",];

        function doctorQrCodeController( HttpServer, Dashboard ,BiomarkConfig){

            var vm = this;
            
            //initialize pending connection request
            vm.$onInit = function(){
                HttpServer
                    .get("v1/doctor/user")
                    .response( success );

                    function success(res){
                        vm.qr = res.personal.qr;
                    }
            }

        }
})();