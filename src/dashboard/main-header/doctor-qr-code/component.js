(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("doctorQrCode",{
            bindings: {
				qr: '='
			},
            templateUrl:"/dashboard/main-header/doctor-qr-code/view.html",
            controller:"doctorQrCodeController",
            controllerAs:"vm"
        })
})();