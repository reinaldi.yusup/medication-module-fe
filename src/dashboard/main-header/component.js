( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('biomarkMainHeader',{
			bindings: {
				indexId: '='
			},
			controller:"biomarkMainHeaderController",
			templateUrl:"/dashboard/main-header/view.html"
		})
})();