( function(){

	"use strict";

	angular
		.module("Biomark")
		.controller('biomarkMainHeaderController',biomarkMainHeaderController);

		biomarkMainHeaderController.$inject=[ "Personal","$state","User","Dashboard", "$sessionStorage", "HttpServer","$timeout","Country","Idle", "ConsumableService", "BiomarkConfig"];

		function biomarkMainHeaderController( Personal, $state, User, Dashboard, $sessionStorage, HttpServer, $timeout, Country,Idle, ConsumableService, BiomarkConfig){
			var vm = this;
			vm.version = HttpServer.app_version;
			
			vm.side_menu = false;
			vm.toggle_side_menu = function(){
				vm.side_menu = !vm.side_menu;
			}
			vm.close_mobile_menu = function(){
				$timeout(function(){
					vm.side_menu = false;
				},100);
			}
			vm.auto_logout = false;
			vm.toggle_auto_logout = function(){
				vm.user.profile.auto_logout = !vm.user.profile.auto_logout;
				HttpServer.post_v2("v2/doctor/setting/toggle_auto_logout",{state:vm.user.profile.auto_logout}).then(
					function(res){
						//re watch
						Idle.watch();
					}
				);
			}
			vm.menu_navigation = function(menu){
				vm.side_menu = !vm.side_menu;
				switch(menu.name){
					case "logout":
						vm.logout();
						break;
					case "policies":
						$state.go(menu.name,{country:"EN"})
						break;
					default:
						$state.go(menu.route);
						break;

				}
			}
			 function mobileMenus() {
        return [
          {
            label:"My Account",
            name:"my_account",
            route:"profile",
            show: true
          },
          {
            label:"eResults",
            name:"my_patient",
            route:"dashboard",
            show: true
          },
          {
            label:"eConsumables",
            name:"consumable",
            route:"consumable",
            show: (vm.user.profile.has_enconsumable && vm.portal_type !== 'gribbles')
          },
          {
            label:"Policies",
            name:"policies",
            route:"policies",
            show: true
          },
          {
            label:"Settings",
            name:"settings",
            route:"settings",
            show: true
          },
          {
            label:"Logout",
            name:"logout",
            show: true
          }
        ]
      }
			vm.pending = {
				count:0,
				patients:[]
			}
			Country.init(function( data){
				vm.data = data;
				
				vm.$onInit = function(){
					if(angular.isDefined(vm.country)){
						vm.data.default = vm.data.countries[vm.country-1];	
					}
				}
			});
			vm.redirect = function (id){
				if(id == 1){
					$state.go("dashboard");
				} else if(id == 2){
					$state.go("orders");
				}else if(id == 3){
					$state.go("consumable");
				}
			}
			vm.$onInit = function(){
        $sessionStorage.biomark_token = "eyJraWQiOiI5SSt2Z016TWwxa1l1WGlQb2Z2c08wM1NhQ1pZZWxcL0R5aVdWelRrRmU4UT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI0ZmY5ZGYxMy0wMjJiLTQyMDUtYWEwNC1lMThiMzQ5YmZmZjUiLCJjb2duaXRvOmdyb3VwcyI6WyJxdWVzdC1kb2N0b3IiXSwiZXZlbnRfaWQiOiIzNTYwNjE1OS00Y2JlLTRjMzQtYTcxZS1iOTYwZTM4Y2U4NWEiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6ImF3cy5jb2duaXRvLnNpZ25pbi51c2VyLmFkbWluIiwiYXV0aF90aW1lIjoxNjM1NzU0MDc4LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuYXAtc291dGhlYXN0LTEuYW1hem9uYXdzLmNvbVwvYXAtc291dGhlYXN0LTFfZnhuWXhvalIxIiwiZXhwIjoxNjM1NzU3Njc4LCJpYXQiOjE2MzU3NTQwNzgsImp0aSI6IjI5ZGE1YmRhLTNjMTctNDBmZC1iMmRhLWNmZjE4MGM4MGFjMCIsImNsaWVudF9pZCI6IjJ0b2dhZmh2dmdjMm90dHV1YnUwZXV1dGZ0IiwidXNlcm5hbWUiOiI0ZmY5ZGYxMy0wMjJiLTQyMDUtYWEwNC1lMThiMzQ5YmZmZjUifQ.XsX3gXAaaslm3-4-0A4O4TJ5IKNQBOtPgs_Gyl6bmetn6g0oDQAIMeErrr7FRnJUr6pr8U_LzX6H3rc4IOiFQJITGyFbE2nJqPeH0HHWM3rv44WsBEahQOqh7T9MzmHRgXxNuowKXLW8squUl3gx-Bah3hZ3wSTTXvOQ3iv7OObQgHt2AuGvxhOUgXTJF4MJ8r78QK8JLSz870itAJH7xRzbt5pC6K_yLORk-krym_cNbSMoCd9QatTBK3IliEQJMbJUgkjj7PiV-W10WpjdvZys93Gzux9F5m8Gxa6Yj2pgU4hpj5SlkoT6gAYTwUgd6vFESywdLkqplXN4B5hD6w"
				vm.index = 1;
        vm.portal_type_id = BiomarkConfig.portal_type_id
        vm.portal_type = "%PORTAL_TYPE%"

				HttpServer
					.post("v1/doctor/patient/pending_results")
					.response( success )
					function success(res){
						vm.pending.count = res.message.pending.counts;
						vm.pending.patients = res.message.pending.patients;
						Personal.init( function(data){
							vm.user = data;
              vm.user_permissions = data.permissions;
              vm.mobile_menus = mobileMenus()
						});
					}
				vm.url_state = $state.current.name;
				console.log(vm.url_state)
			}
			Dashboard.init( function(data){
				vm.portal = data;
				data.reloadPending = function(){
					HttpServer
					.post("v1/doctor/patient/pending_results")
					.response( success )
					function success(res){
						vm.pending.count = res.message.pending.counts;
						vm.pending.patients = res.message.pending.patients;
					}
				}
				
			});
			
			vm.logout= function(){
				
				User.logout()
				User.set_token(0);
				$sessionStorage.contact = {}; $sessionStorage.level = 0; $sessionStorage.personal = {country_id: 1}; $sessionStorage.order_history = {};$sessionStorage.patient_data = null;
				ConsumableService.clear();
				Personal.reset = {
					tab_position: 0,
					profile:{
						status: false,
						data:{
							country_id:1
						}
					},
					contact:{
						data:{},
						status: false
					}
				}
				$state.go("login");
			}
			vm.accept = function(patient){
				vm.patient = patient;
				Dashboard.data.socket.send({
					action:"doctor_accept",
					patient_id:vm.patient.id,
					access_token: Dashboard.data.socket.channelParams.token,
					group: Dashboard.data.socket.channelParams.group
				})
				vm.popup_type = 3;
				// Dashboard.data.reloadPending();
				$timeout(Dashboard.data.reloadPending, 500)
				$timeout(Dashboard.data.reloadCount, 1000)
				Dashboard.paginate(vm.portal.pagination);
				
				
			}
			vm.reject = function(patient){
				vm.patient = patient;
				Dashboard.data.socket.send({
					action:"doctor_reject",
					patient_id:vm.patient.id,
					access_token: Dashboard.data.socket.channelParams.token,
					group: Dashboard.data.socket.channelParams.group
				})
				vm.popup_type = 2;
				$timeout(Dashboard.data.reloadPending, 500)
				$timeout(Dashboard.data.reloadCount, 1000)
				Dashboard.paginate(vm.portal.pagination);

			}
			vm.view_record = function( id ){
				vm.popup_type = 0;
				$state.go("patient",{id:id});
			}
			vm.close = function(){
				vm.popup_type = 0;
			}
			vm.goToDashboard = function(){
				Dashboard.init(function(data){
					data.tab_index = 1;
					data.pagination = 1;
				})
				$state.go("dashboard")
			}
			// vm.getPendingList = function(){
			// 	HttpServer
			// 		.post("v1/doctor/patient/pending_results")
			// 		.response(
			// 			function(res){
			// 				vm.pending.count = res.message.pending.counts;
			// 				vm.pending.patients = res.message.pending.patients;
			// 		});
			// }
		}
})();