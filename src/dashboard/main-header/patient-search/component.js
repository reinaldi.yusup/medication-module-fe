(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("patientSearch",{
            templateUrl:"/dashboard/main-header/patient-search/view.html",
            controller:"patientSearchController"
        })
})();