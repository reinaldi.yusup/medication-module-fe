(function(){
    "use strict";

    angular
        .module("Biomark")
        .controller("patientSearchController",patientSearchController);

        patientSearchController.$inject = ["HttpServer"];

        function patientSearchController(HttpServer){
            var vm = this;

            vm.$onInit = function(){
                
            }
            vm.search = function( keyString ){
                HttpServer.cancel_request();
                if(vm.search_result) vm.search_result.length = 0;
                if(typeof keyString == "undefined"){
                    return false;
                } else{
                    console.log("continue....");
                    HttpServer.post_with_abort("v2/doctor/patient/search",{keywords:keyString})
                    .then( success, error );

                    function success(res){
                        vm.search_result = res.data;
                    }

                    function error(){
                        console.clear()
                    }
                }
                
            }
            vm.hide_search_result = function(){
                vm.search_result.length = 0;
                vm.searchStr = "";
            }
        }
})();