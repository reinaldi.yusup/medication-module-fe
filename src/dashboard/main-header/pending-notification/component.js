(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("pendingNotification",{
            templateUrl:"/dashboard/main-header/pending-notification/view.html",
            controller:"pendingNotificationController"
        })
})();