(function(){
    "use strict";

    angular
        .module("Biomark")
        .controller("pendingNotificationController",pendingNotificationController);
        
        pendingNotificationController.$iject = ["HttpServer","Dashboard","BiomarkConfig"];

        function pendingNotificationController( HttpServer, Dashboard ,BiomarkConfig){

            var vm = this;
            //initialize dashboard instance objects
            Dashboard.init(function(data){	
				vm.portal = data;
            });

            //initialize configuration objects
            vm.config = BiomarkConfig;

            //header subcription adhoc method
            Dashboard.subscribe_patient_connection_header = function(){
                vm.$onInit();
            }

            //approve connection notification
            vm.approve = function(patient){
                vm.modal_type = 1;
                vm.modal_data = patient;
				vm.portal.socket.send({
					action:"doctor_accept",
					patient_id:patient.id,
					access_token: Dashboard.data.socket.channelParams.token,
					group: Dashboard.data.socket.channelParams.group
                })
            }

            //close modal
            vm.close_modal = function(){
                vm.modal_type = 0;
                delete vm.modal_data;
                vm.$onInit();
                //on close execute adhoc method
                Dashboard.subscribe_patient_connection();               
            }

            //reject connection
            vm.reject = function(patient){
                vm.modal_type = 2;
                vm.modal_data = patient;
				vm.portal.socket.send({
					action:"doctor_reject",
					patient_id:patient.id,
					access_token: Dashboard.data.socket.channelParams.token,
					group: Dashboard.data.socket.channelParams.group
				})
                
            }

            //initialize pending connection request
            vm.$onInit = function(){
                
                HttpServer
                    .get("v2/doctor/patient/pending_notifications")
                    .response( success );

                    function success(res){
                        vm.data = res;
                    }

            }

        }
})();