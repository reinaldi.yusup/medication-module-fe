(function () {
    "use strict";

    angular
        .module("Biomark")
        .controller("eorderNewPatientController",eorderNewPatientController);

        eorderNewPatientController.$inject = ["Country","HttpServer","EOrderService", "DateService","$state"];

        function eorderNewPatientController(Country,HttpServer, EOrderService, DateService, $state){
            var vm = this;

            vm.patient_data = {};
            vm.add_patient = function(data){
                let date = new Date(data.birth_date);
                data.birth_date =  date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
                
                var patient_data = {
                    first_name: data.patient_name, 
                    gender:{
                        id: data.gender_id
                    } ,
                    birth_date: data.birth_date,
                     
                    calc_birthday: data.age, 
                    mobile: data.mobile, 
                    email_address: data.email
                };
                EOrderService.verify_information(patient_data);
                vm.closeModal();
            }
            vm.$onInit = function(){
                vm.close_modal = vm.closeModal;
                console.log(vm.request);
                vm.label = vm.isEdit ? "Edit Profile" : "Add Patient"
                if(vm.isEdit){
                    //fill patient information
                    vm.patient_data.patient_name = vm.editData.demographic.first_name;
                    vm.patient_data.ic_number = vm.editData.demographic.ic_number;
                    vm.patient_data.gender_id = vm.editData.demographic.gender == "Male" ? 1:2
                    vm.patient_data.birth_date = new Date(vm.editData.demographic.date_of_birth);
                    
                    vm.patient_data.age = vm.editData.demographic.age;
                    vm.patient_data.age = vm.editData.demographic.age;
                    if(vm.editData.demographic.contact){
                        vm.patient_data.mobile = vm.editData.demographic.contact.mobile;
                        vm.patient_data.email = vm.editData.demographic.contact.email_address;
                    }
                }else{
                    vm.patient_data.birth_date = new Date();
                }
            } 
        
            vm.validate_fields = function () {
                var state = true
                angular.forEach(vm.fields, function (state, key) {
                    vm.fields[key] = false;
                });

                if (vm.patient_data.patient_name == "" || angular.isUndefined(vm.patient_data.patient_name)) {
                    vm.fields.patient_name = true;
                    state = false;
                }
                if (vm.patient_data.ic_number == "" || angular.isUndefined(vm.patient_data.ic_number)) {
                    vm.fields.ic_number = true;
                    state = false;
                }
                if (vm.patient_data.gender_id == "" || angular.isUndefined(vm.patient_data.gender_id)) {
                    vm.fields.gender_id = true;
                    state = false;
                }
                if (vm.patient_data.mobile == "" || angular.isUndefined(vm.patient_data.mobile)) {
                    vm.fields.mobile = true;
                    state = false;
                }
                if (vm.patient_data.birth_date == "" || angular.isUndefined(vm.patient_data.birth_date)) {
                    vm.fields.birth_date = true;
                    state = false;
                }
                if (vm.patient_data.age == "" || angular.isUndefined(vm.patient_data.age)) {
                    vm.fields.age = true;
                    state = false;
                }
                if (vm.patient_data.email == "" || angular.isUndefined(vm.patient_data.email)) {
                    vm.fields.email = true;
                    state = false;
                }

                return state

            }
        
            vm.update_profile = function (profile) {
                if (vm.validate_fields()) {
                    HttpServer.post_v2("v2/doctor/patient/" + $state.params.id + "/update_patient_profile", { profile }).then(
                        function (res) {
                            vm.closeModal();
                            $state.reload();
                        }
                    );
                }
            }

            vm.create_profile = function (profile) {
                if (vm.validate_fields()) {
                    HttpServer.post_v2("v2/doctor/patient/create_patient_profile", { profile }).then(
                        function (res) {
                            vm.closeModal();
                            $state.reload();
                        }
                    );
                }
            }
        }
})();