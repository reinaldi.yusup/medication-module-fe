(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("eorderNewPatient",{
            templateUrl:"/dashboard/eorders/eorder-patient-search/new-patient/view.html",
            controller:"eorderNewPatientController",
            bindings:{
                closeModal:"=",
                isEdit:"=",
                editData:"=",
                patientData:"=",
                request: "="
            }
        })
})();