(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("eorderPatientSearch",{
            templateUrl:"/dashboard/eorders/eorder-patient-search/view.html",
            controller:"eorderPatientSearchController"
        })
})();