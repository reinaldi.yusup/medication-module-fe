(function(){
  "use strict";

  angular
      .module("Biomark")
      .component("eordersTestSelection",{
          templateUrl:"/dashboard/eorders/eorders-test-selection/view.html",
          controller:"eordersTestSelectionController"
      })
})();