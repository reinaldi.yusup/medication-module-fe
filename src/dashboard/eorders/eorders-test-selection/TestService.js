( function(){

	"use strict";

	angular
		.module("Biomark")
        .factory("TestService", TestService);
        
        TestService.$inject = ["$http","$state"];

        function TestService($http,$state){
            //handle track eorder data changes
            var f = {};
            f.data = {};
            f.getJsonData = function(jsonName){
                console.log(jsonName);
               return $http.get("/images/json/" + jsonName );
            }
            return f;
        }
})();