(function () {

    "use strict";

    angular
        .module("Biomark")
        .controller('eordersTestSelectionController', eordersTestSelectionController);

    eordersTestSelectionController.$inject = ["HttpServer", "Personal", "$state", "$sessionStorage", "TestService", "$scope","$timeout"];

    function eordersTestSelectionController(HttpServer, Personal, $state, $sessionStorage, TestService, $scope, $timeout) {
        var vm = this;
        vm.currentDate = null;
        vm.orderData = {};
        vm.patient_data = {};
        vm.specialities = [];
        vm.departments = [];
        vm.change_specimen = [];
        vm.isLabSelected = false;
        vm.filter = '';
     
        vm.autoFillResult = [];
        vm.openDrugSuggestion = false;
        vm.openDiagnoSuggestion = false;

        
        vm.order = {
            custom_specimen: [],
            is_urgent: false,
            packages: [],
            not_fasting: true,
            collection_date: {},
            discount: 0,
            cc: [],
            subject: '',
            test_schedule: '',
            selectedDrugs: [],
            selectedDiagnosis: [],
            menstrual:'',
            clinical_indication: ''
        };
        vm.schedule = false;
        vm.dateShowFlag = false;
        vm.dateShowShcFlag = false;
        vm.dateOptions = {
            maxDate: new Date()
        }
        vm.order.is_urgent = false;
        vm.order.abnormal_biomarker = [{ name: "Platelets", normal_range: "140-440(10^9/L)", result: 138 },
        { name: "HDL Cholesterol", normal_range: "14.0-18.0 (g/dl)", result: 0.09 },
        { name: "WBC", normal_range: "140-440(10^9/L)", result: 11.8 },
        { name: "Platelets", normal_range: "140-440(10^9/L)", result: 11.8 },
        { name: "RBC", normal_range: "140-440(10^9/L)", result: 10.22 }];
        vm.patient_menstrual = "";
        vm.diagnosis = [];
        vm.drugs = [];
        vm.tempDrug = "";
        vm.tempCC = "";
        vm.tempDigno = ""; 
        vm.emailInvalid = false;
        vm.diagnosisValue = "";
        vm.packageIds = [];

        vm.order.selectedDiagnosis.forEach((value) => vm.diagnosisValue = vm.diagnosisValue + value + ",");
        vm.order.selectedDrugs.forEach((value) => vm.drugsValue.concat(value));

        vm.diagnosisJsonData = function() {
            TestService.getJsonData('diagnosis.json').then(function (res) {
                vm.diagnosis = res.data;
            })
        }

        vm.drugsJsonData = function() {
            TestService.getJsonData('drugs.json').then(function (res) {
                vm.drugs = res.data;
            })
        }

        vm.$onInit = function () {
            vm.requirements = {};
            vm.patient_data = $sessionStorage.patient_data;
            vm.requirements.laboratories = [
              { id: 1, name: "Innoquest Diagnostics" },
              { id: 2, name: "Gribbles Pathology" }
              // {id:31, name:"Innoquest"}
            ];
            // HttpServer.get_v2("v2/doctor/order/requirement").then(
            //     function (res) {
            //         vm.requirements = res.data;
            //         vm.requirements.laboratories = [
            //             { id: 1, name: "Innoquest" },
            //             { id: 2, name: "Gribbles" }
            //             // {id:31, name:"Innoquest"}
            //         ];
            //         // vm.order.custom_specimen = [];
            //     }
            // );
            HttpServer.post_v2("v2/doctor/eorder/search_test", { provider_id: 1 }).then(
                function (res) {
                    console.log('data from new api', res);
                    // if(cb) cb(); 
                }
            );
            
            console.log("initialized");
            vm.diagnosisJsonData();
            vm.drugsJsonData();
            // if($sessionStorage.patient_data.order != null){
            //     vm.order = $sessionStorage.patient_data.order;
            // }
            if (vm.patient_data.order !== undefined) {
              vm.requirements.packages = vm.patient_data.order.packages
              vm.order.packages = vm.patient_data.order.packages
            }
        }

        vm.filterDiagnosisSearch = function () {
            
            vm.autoFillResult = [];
            for (let digno of vm.diagnosis) {

                if (digno.diagnosis.toLowerCase().includes(vm.tempDigno.toLowerCase())) {
                    vm.autoFillResult.push(digno.diagnosis);
                }
            }
            vm.openDiagnoSuggestion = true;
            return vm.autoFillResult;
        }
        vm.filterDrugSearch = function () {
            vm.autoFillResult = [];
            for (let drug of vm.drugs) {

                if (drug.drug.toLowerCase().includes(vm.tempDrug.toLowerCase())) {

                    vm.autoFillResult.push(drug.drug);
                }
            }
            vm.openDrugSuggestion = true;
            return vm.autoFillResult;
        }
        vm.moveTosampleCollection = function () {
            if (vm.validateMandatoryFields()) {
                $sessionStorage.patient_data.order = vm.order;
                $scope.$emit('collectionActive', true);
                $state.go('eorders.sample-collection');
            }
        }

        vm.scheduleCollection = function() {
            // {{::$ctrl.patient_data.birth_date | date:"MMM"}} {{::$ctrl.patient_data.birth_date | date:"dd"}},
            //     {{::$ctrl.patient_data.birth_date | date:"yyyy"}}
            vm.currentDate = new Date();
            let scheduleDate = vm.currentDate.toLocaleString('en-us', { month: 'short' })+" "+vm.currentDate.getDate()+","+vm.currentDate.getFullYear()+" "+vm.currentDate.getHours()+":"+vm.currentDate.getMinutes();  
            console.log("Scedule Date -> "+scheduleDate);
            vm.packageIds = vm.order.packages.map((value) => value.id);
            vm.urgent = vm.order.is_urgent ? "URGENT" : "ROUTINE";
            vm.orderData = {
                package_ids:vm.packageIds,
                patient_id: vm.patient_data.user_id,
                current_and_previous_diagnosis: vm.order.selectedDiagnosis.join(),
                concurrent_drug_therapy: vm.order.selectedDrugs.join(),
                priority: vm.urgent,
                email_list:vm.order.cc.join(),
                subject:vm.order.subject,
                dateOfTest : scheduleDate,
                clinical_indications : vm.order.clinical_indication,
                order_status : 0
            }
            console.log(vm.orderData);
            if (vm.validateMandatoryFields()) {
                // console.log(vm.order);
                HttpServer.post_v2("v2/doctor/order/schedule_order", { order : vm.orderData }).then(
                    function (res) {
                        console.log(res);
                        if(res.status == 200) {
                            console.log("Success");
                            $state.go('eorders.worklist');
                        }
                    }
                )   
            }
        }

        vm.moveToEorders = function () {
            $state.go('eorders');
        }

        vm.labSelected = function () {
            vm.isLabSelected = true;
            console.log(vm.order.clinic, vm.speciltyFilter, vm.deptFilter);
            HttpServer.post_v2("v2/doctor/order/packages", { provider_id: vm.order.clinic, speciality: vm.speciltyFilter, department: vm.deptFilter }).then(
                function (res) {
                   
                    vm.requirements.packages = res.data.packages;
                    vm.specialities = res.data.specialities;
                    vm.departments = res.data.departments;
                    vm.order.packages = [];
                    // if (vm.patient_data.order !== undefined) {
                    //   let packages = vm.patient_data.order.packages
                    //   if (packages !== undefined) {
                    //     angular.forEach(packages, function(obj) {
                    //       angular.forEach(vm.requirements.packages, function(obj2) {
                    //         if (obj.id === obj2.id) {
                    //           obj2.is_order = obj.is_order
                    //         }
                    //       })
                    //     })
                    //   }
                    // }
                }
            );
        }
        vm.toggle_urgency = function () {
            vm.order.is_urgent = !vm.order.is_urgent;
        }
        vm.add_package = function (pkg, index) {
            if(vm.filter.length != 0){
                vm.filter = "";
            }
            pkg.is_order = true;
            pkg.local_index = pkg.test_code;
            vm.order.packages.push(pkg);
            calculate_specimen_state();
            // calculate_cost();
            sortTestsOnSelect();
        }
        vm.removeOrderedPkg = function (pkg) {
            pkg.is_order = false;
            sortTestsOnSelect();
            vm.order.packages = vm.order.packages.filter(test => test.local_index != pkg.local_index);
        }
        var sortTestsOnSelect = function () {
            vm.requirements.packages.sort((test1, test2) => {
                if (test1.is_order == undefined || test1.is_order == false) {
                    return 1;
                }
                return -1;
            });
        }
        vm.remove_package = function (index, local_code) {
            HttpServer.set_custom_specimen(vm.requirements.specimen_types);
            angular.forEach(vm.requirements.packages, function (t_code) {
                if (t_code.test_code === local_code) {
                    t_code.is_order = false;
                }
            });
            vm.order.packages.splice(index, 1);
            calculate_specimen_state();
            calculate_cost();
        }
        vm.op_close_modal = function () {
            vm.order_package_detail = false;
        }
        function calculate_specimen_state() {
            vm.test_specimen = [];
            vm.test_req = [];
            var is_found;

            angular.forEach(vm.order.packages, function (spec) {
                angular.forEach(spec.specimen_types, function (e) {
                    if (vm.test_specimen.indexOf(e.name) == -1) {
                        vm.test_specimen.push(e);
                    }
                });
                angular.forEach(spec.test_requirements, function (req) {
                    var req_found = vm.test_req.some(function (tr) { return tr.description === req.description });
                    !req_found ? vm.test_req.push(req) : '';
                });
            })
            console.log(vm.test_specimen,);

            angular.forEach(vm.requirements.specimen_types, function (specimen) {
                is_found = vm.test_specimen.some(function (el) { return el.name === specimen.name });
                specimen.is_required = is_found;

                if (vm.change_specimen.indexOf(specimen.name) !== -1) {
                    specimen.is_required = true;
                }

                var cacheOrder = HttpServer.get_order_data();
                try {
                    if (cacheOrder.laboratory) {
                        if (cacheOrder.patient_id == $state.params.id) {
                            var change_specimen = HttpServer.get_change_specimen();
                            if (change_specimen != null) {
                                if (change_specimen.indexOf(specimen.name) !== -1) {
                                    specimen.is_required = true;
                                }
                            }

                        }
                    }
                } catch (error) { }

            });

            var cacheOrder = HttpServer.get_order_data();
            try {
                if (cacheOrder.laboratory) {
                    if (cacheOrder.patient_id == $state.params.id) {
                        cacheSpecimen = HttpServer.get_custom_specimen();
                        var change_specimen = HttpServer.get_change_specimen();
                        if (cacheSpecimen != null) {
                            vm.requirements.specimen_types = cacheSpecimen;
                        }

                    }
                }
            } catch (error) { }


        }
        vm.openInput = function (inputId) {
            document.getElementById(inputId).focus();
        }
        vm.removeCC = function (email) {
            var index = vm.order.cc.indexOf(email);
            console.log(email, index);
            if (index > -1) {
                vm.order.cc.splice(index, 1);
            }
        }
        vm.removeDrug = function (drug) {
            var index = vm.order.selectedDrugs.indexOf(drug);
            if (index > -1) {
                vm.order.selectedDrugs.splice(index, 1);
                vm.drugsJsonData();
            }
        }
        vm.removeDiagnosis = function(diagno) {
            var index = vm.order.selectedDiagnosis.indexOf(diagno);
            if (index > -1) {
                vm.order.selectedDiagnosis.splice(index, 1);
                vm.diagnosisJsonData();
            }
        }
        vm.addEmail = function () {
            console.log(vm.tempCC);
            var re = new RegExp("^[a-zA-Z0-9._-]+[@][a-zA-Z0-9.-]+[.][a-zA-Z]{2,4}$");
            // var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (re.test(vm.tempCC)) {
                vm.order.cc.push(vm.tempCC);
                vm.tempCC = '';
                vm.emailInvalid = false;
                console.log(vm.order.cc);
            } else {
                vm.emailInvalid = true;
            }

        }
        vm.addDrug = function (drug) {
            if (drug.length > 1) {
                vm.order.selectedDrugs.push(drug);
                var index = vm.drugs.findIndex(data => data.drug == drug);

                if (index > -1) {
                    vm.drugs.splice(index, 1);
                }
                vm.tempDrug = '';
            }
        }
        vm.addDiagno = function (diagno) {
            if (diagno.length > 1) {
                vm.order.selectedDiagnosis.push(diagno);
                var index = vm.diagnosis.findIndex(data => data.diagnosis == diagno);

                if (index > -1) {
                    vm.diagnosis.splice(index, 1);
                }
                vm.tempDigno = '';
            }
        }

        vm.specFilter = function (spec) {
            vm.speciltyFilter = spec;
           // vm.labSelected();
        }
        vm.department = function (dept) {
            console.log("filter-->",dept);
            vm.deptFilter = dept;
            //vm.labSelected();
        }
        vm.show_popup = function (test_code) {
            vm.order_package_detail = true;
            vm.test_included = [];
            vm.modal_data = [];
            angular.forEach(vm.requirements.packages, function (data) {
                if (data.test_code == test_code) {
                    vm.modal_init_data = data;
                }
            });
            vm.modal_init_data.test_included !== "" ? vm.test_included = JSON.parse(vm.modal_init_data.test_included) : '';
        }
        vm.validateMandatoryFields = function () {
            if (vm.order.packages.length == 0) {
                vm.error_message = "please select test";
                vm.pass_invalid = true;
                resetPopupError();
                return false
            }
            // if (vm.order.selectedDiagnosis.length == 0) {
            //     vm.error_message = "please select diagnosis";
            //     vm.pass_invalid = true;
            //     resetPopupError();
            //     return false
            // }
            // if (vm.order.selectedDrugs.length == 0) {
            //     vm.error_message = "please select Drug Therapy";
            //     vm.pass_invalid = false;
            //     resetPopupError();
            //     return false
            // }
            if(vm.order.clinical_indication == null || 
               vm.order.clinical_indication == undefined ||
               vm.order.clinical_indication == "") {
                vm.error_message = "please enter clinical indication";
                vm.pass_invalid = true;
                resetPopupError();
                return false
            }

            return true;

        }

        function resetPopupError() {
            $timeout(function () {
                vm.pass_invalid = false;
                delete vm.error_message;
            }, 3000)
        }
    }
})();