( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('eOrders',{
			controller:"eOrdersController",
			templateUrl:"/dashboard/eorders/view.html"
		})
})();