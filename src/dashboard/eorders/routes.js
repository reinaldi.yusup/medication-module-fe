( function(){

	"use strict";

	angular
		.module('Biomark')
		.config(['$stateProvider','$urlRouterProvider','$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
	
    		$urlRouterProvider.otherwise('login');

    		$stateProvider
                .state("eorders",{
                    url:"/eorders/:id",
                    template:"<e-orders></e-orders>",
                })
                .state("eorders.histories",{
                    url:"/histories",
                    views:{
                        'order-main-view':{
                            template:"<order-history></order-history>",
                        }
                    }
                })
                .state("eorders.worklist",{
                    url:"/worklist",
                    views:{
                        'order-main-view':{
                            template:"<order-worklist></order-worklist>",
                        }
                    }
                })
                .state("eorders.histories.view-order",{
                    url:"/view-order/:order_id",
                    template:"<eorder-history-view></eorder-history-view>",
                })
                .state("eorders.test-selection",{
                    url:"/test-selection",
                    views: {
                        'order-child-view': {
                            template:"<eorders-test-selection></eorders-test-selection>",
                        }
                    }
                })
                .state("eorders.sample-collection",{
                    url:"/sample-collection",
                    views: {
                        'order-child-view': {
                            template:"<eorder-sample-collection></eorder-sample-collection>",
                        }
                    }
                })
                .state("eorders.confirmation",{
                    url:"/confirmation",
                    views: {
                        'order-child-view': {
                            template:"<eorders-confirmation></eorders-confirmation>",
                        }
                    }
                })

                
		}])

	
})();

