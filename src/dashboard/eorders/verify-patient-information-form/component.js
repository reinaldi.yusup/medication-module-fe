(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("verifyPatientInformationForm",{
            templateUrl:"/dashboard/eorders/verify-patient-information-form/view.html",
            controller:"verifyPatientInformationFormController"
        })
})();