(function(){
    "use strict";

    angular 
        .module("Biomark")
        .controller("eorderPatientMobileNumberController",eorderPatientMobileNumberController);

        eorderPatientMobileNumberController.$inject = ["Country","HttpServer","Personal"];

        function eorderPatientMobileNumberController(Country,HttpServer,Personal){
            var vm = this;
            
            vm.dp_list = false;
            vm.has_contact = false;
            vm.is_invalid = true;
            vm.invalid_mobile = false;
            vm.active.code = "+91";
            vm.numChange = function(a){
                console.log("num change", vm.active)
                if(a == undefined){
                    vm.is_invalid = true;
                    vm.invalid_mobile = false;
                    document.getElementById("patient-mobile-wrapper").style.border = "1px solid #d9dfeb"
                }else{
                    if(vm.validate_mobile(a, vm.active.dial_code)){
                        vm.invalid_mobile = true;
                        vm.is_invalid = true;
                        document.getElementById("patient-mobile-wrapper").style.border = "1px solid #dc3545"
                    }else{
                        vm.invalid_mobile = false;
                        vm.is_invalid = false
                        document.getElementById("patient-mobile-wrapper").style.border = "1px solid #d9dfeb"
                    }
                }
            }
            vm.add_number = function(){
                if(vm.is_invalid){
                    return false;
                }
                vm.is_updating = true;
                var mobile_number = vm.active.dial_code+vm.active_number;
                HttpServer
                    .post_v2("v2/doctor/patient/"+vm.patientId+"/add_mobile",{mobile:mobile_number,country_id:vm.active.id})
                    .then(function(res){
                        vm.is_updating = false;
                        vm.add_contact_field = false;
                        vm.has_contact = true;
                        vm.active_number = vm.active.dial_code+vm.active_number;
                        vm.smsNotification = res.data.sms_notification;
                        vm.hasNumber = true;
                    });
            }
            vm.validate_mobile = function(num, dial_code){
                if(num.length >= 8 && num.length <= 12){
                    if(!num.startsWith("1") && dial_code == "+60"){
                        vm.invalid_mobile = true;
                        vm.is_invalid = true;
                        document.getElementById("patient-mobile-wrapper").style.border = "1px solid #dc3545"
                        return true;
                    }

                    if(dial_code == "+60" && num.length > 10){
                        vm.invalid_mobile = true;
                        vm.is_invalid = true;
                        document.getElementById("patient-mobile-wrapper").style.border = "1px solid #dc3545"
                        return true;
                    }
                }

                vm.invalid_mobile = false;
                    vm.is_invalid = false;
                document.getElementById("patient-mobile-wrapper").style.border = "1px solid #d9dfeb"
                return false;
            }
            vm.edit_contact = function(){
                vm.active_number = vm.active_number.substring(3,vm.active_number.length);
                vm.add_contact_field = true;
            }
            vm.add_contact = function(){
                vm.add_contact_field = true;
            }
            vm.on_item_clicked = function(a){
                console.log(a)
                vm.dp_list = false;
                if(vm.active_number == undefined){
                    vm.is_invalid = true;
                }else{
                    vm.validate_mobile(vm.active_number, a.dial_code);
                }
                vm.active = a;
            }
            vm.close_dp = function(){
                vm.dp_list = false;
            }
            vm.open_dp_list = function(){
                vm.dp_list = true;
            }
            
            vm.$onInit = function(){
                
            }
        }
})();