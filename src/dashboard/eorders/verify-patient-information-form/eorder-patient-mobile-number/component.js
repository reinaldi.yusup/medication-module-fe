(function(){
    "use strict"

    angular
        .module("Biomark")
        .component("eorderPatientMobileNumber",{
            bindings:{
                patientId:"=",
                dashboard:"=",
                smsNotification:"=",
                hasNumber:"="
            },
            controller:"eorderPatientMobileNumberController",
            templateUrl:"/dashboard/eorders/verify-patient-information-form/eorder-patient-mobile-number/view.html"
        })
})();