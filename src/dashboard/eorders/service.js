( function(){

	"use strict";

	angular
		.module("Biomark")
        .factory("EOrderService",EOrderService);
        
        EOrderService.$inject = ["$http","$state"];

        function EOrderService($http,$state){
            //handle track eorder data changes
            var f = {};
            f.data = {};
            f.getCountryCode = function(country){
               return $http.get('../images/json/list_of_countries.json');
         
            }
            
            return f;
           
        }
        
})();