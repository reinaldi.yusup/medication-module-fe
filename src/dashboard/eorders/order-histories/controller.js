( function(){

	"use strict";

	angular
		.module("Biomark")
		.controller('eorderHistoryController',eorderHistoryController);

		eorderHistoryController.$inject=["HttpServer","Personal","$state"];

		function eorderHistoryController(HttpServer,Personal, $state){
			var vm = this;
			vm.histories = {};
            vm.$onInit = function(){
				HttpServer.get_v2("v2/doctor/order/get_order_worklist").then(
                    function (res) {
                        console.log(res);
                        if(res.status == 200) {
                            // console.log("Success");
							for(let i = 0; i< res.data.data_history.length; i++) {
								console.log("Data -> "+i);
								vm.historyData = {
									// test_entered_date_time: res.data.data_worklist[i].order.dateOfTest,
									first_name: res.data.data_history[i].patient.first_name,
									last_name: res.data.data_history[i].patient.last_name,
									ic_number: res.data.data_history[i].patient.ic_number,
									date_of_birth: res.data.data_history[i].patient.birth_date,
									test_to_be_obtained: res.data.data_history[i].test_to_be_obtained,
									status: res.data.data_history[i].order.order_status
								}
								console.log(vm.historyData);
								vm.worklistDatas.push(vm.historyData);
							}
                        }
                    }
                )     
            }

                vm.histories.data = [
					{
						first_name: "Connie",
						last_name: "Smith",
						barcode:"TEST-00",
						created_at: "2021-07-01T09:56:28.000+08:00",
						updated_at: "2021-07-02T09:56:28.000+08:00",
						status: "Processing",
						is_urgent: false,
						packages: [
							{test_code: "P.WWF", test_name: "Urine Test Culture"}, 
							{test_code: "P.URICULT", test_name: "Renal Function Test"}
						]
					}
				];
            }

			// vm.search_order = function(keyword){
		// }
})();