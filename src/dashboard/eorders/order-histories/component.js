( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('orderHistory',{
			controller:"eorderHistoryController",
			templateUrl:"/dashboard/eorders/order-histories/view.html"
		})
})();