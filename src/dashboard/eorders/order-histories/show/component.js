(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("eorderHistoryView",{
            templateUrl:"/dashboard/eorders/order-histories/show/view.html",
            controller:"eorderHistoryViewController"
        })
})();