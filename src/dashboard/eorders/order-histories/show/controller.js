( function(){

	"use strict";

	angular
		.module("Biomark")
		.controller('eorderHistoryViewController',eorderHistoryViewController);

		eorderHistoryViewController.$inject=["HttpServer","Personal","$state","$timeout"];

		function eorderHistoryViewController(HttpServer,Personal, $state, $timeout){
			var vm = this;
			
            vm.$onInit = function(){
                console.log("view initialized");
            }

			vm.print = function(){
                $('#printable-element').printThis();
            }
		}
})();