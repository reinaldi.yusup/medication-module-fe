( function(){

	"use strict";

	angular
		.module("Biomark")
        .factory("PopupModalService",PopupModalService);
        PopupModalService.$inject = [];

        function PopupModalService(){
            var f = {};
            f.data = {
              modal_state: false,
              booking_details: {},
              declaration_details: {},
              patient_details: {},
              schedule_details: {},
              controls: [],
              image_data: ''
            };

            f.init = function( cb ){
              console.log(f.data)
              cb(f.data);
            }
            return f;
        }
})();