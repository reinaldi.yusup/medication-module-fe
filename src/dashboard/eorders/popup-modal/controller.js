( function(){

	"use strict";


	angular
		.module("Biomark")
		.controller("popupModalController",popupModalController);

		popupModalController.$inject=['PopupModalService'];

		function popupModalController(PopupModalService){
      var vm = this;

      vm.hideModal = function() {
        PopupModalService.notification_modal_state(false, {})
      }

      vm.showAddPatientFormModal = function() {
        vm.hideModal()
        PopupModalService.patient_form_modal_state(true, {})
      }

      this.$onInit = function(){
        vm.modalState = false
        PopupModalService.notification_modal_state = function(modal_state ,other_data){
          vm.modalState = modal_state;
          vm.data = other_data;
        }
      }
		}
})();