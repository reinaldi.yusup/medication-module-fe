( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('popupModal',{
			templateUrl:"/dashboard/eorders/popup-modal/view.html",
			controller:"popupModalController"
		})
})();