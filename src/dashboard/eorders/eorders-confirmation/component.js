(function(){
  "use strict";

  angular
      .module("Biomark")
      .component("eordersConfirmation",{
          templateUrl:"/dashboard/eorders/eorders-confirmation/view.html",
          controller:"eordersConfirmationController"
      })
})();
