(function () {

	"use strict";

	angular
		.module("Biomark")
		.controller('eordersConfirmationController', eordersConfirmationController);

	eordersConfirmationController.$inject = ["HttpServer", "Personal", "$state", "$timeout", "$sessionStorage", "BiomarkConfig"];

	function eordersConfirmationController(HttpServer, Personal, $state, $timeout, $sessionStorage, BiomarkConfig) {
		var vm = this;
		vm.tests = [{"name":"Urine Culture","specimen": "Mid Stream Urine Specimen"},
		{"name":"Urine Culture","specimen": "Mid Stream Urine Specimen"}];
		vm.cc = ["abc@gmail.com","bbbbbb@gmail.com"];
    Personal.init( function(data){
      vm.user = data;
    });
		//initial popup state
		vm.approve = false;
		vm.doctor = {};
		vm.continue = false;
		vm.patient_data = "";
    vm.barcode = generateBarcode();
		vm.openTestReqDialog = function () {
			$timeout(function () {
				JsBarcode("#barcode", vm.barcode, {
					format: "CODE128",
					lineColor: "#000000",
					width: 1.1,
					height: 50,
					displayValue: true
				});
			});
			vm.approve = true;
		}
		vm.close_modal = function () {
			vm.approve = false;
		}
		vm.$onInit = function () {
			vm.patient_data = $sessionStorage.patient_data;

			$timeout(function () {
				JsBarcode("#barcode", vm.barcode, {
					format: "CODE128",
					lineColor: "#000000",
					width: 1.1,
					height: 50,
					displayValue: true
				});
			});

			vm.order_confirmation = $sessionStorage.patient_data.order_data;
			vm.patient_data = $sessionStorage.patient_data;
			vm.order_date = new Date(vm.order_confirmation).toLocaleDateString().replace(/\//g, "-");
			vm.order_time = new Date(vm.order_confirmation).toLocaleString('en-US', { hour: 'numeric', hour12: true })
		}

    function generateBarcode() {
      var randNumber = Math.floor(Math.random() * (99 - 10)) + 10;
      var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var randomString = ''
      for ( var i = 0; i < 5; i++ ) {randomString += randomChars.charAt(Math.floor(Math.random() * randomChars.length))}
      return (vm.user.doctor_code + '-' + BiomarkConfig.country_code + '-' + randNumber + randomString)
    }
		vm.correct = function () {
			$sessionStorage.patient_data = null;
			vm.continue = true;
		}
		vm.stepBack = function () {
			$state.go('eorders.sample-collection');
		}

		vm.printBarCode = function() {
			var contentToPrint = document.getElementById('barCode').innerHTML;
			var windowPopup = window.open('', '_blank', 'width=1000,height=1000');
			windowPopup.document.open();
			windowPopup.document.write('<html><head></head><body style="border: 1px solid black;padding: 10px;">' +
				contentToPrint + '</body></html>');
			windowPopup.print();
			setTimeout(function () { windowPopup.close(); }, 500);
			windowPopup.document.close();
		}
	
	}
})();