(function () {

	"use strict";

	angular
		.module("Biomark")
		.controller('eOrdersController', eOrdersController);

	eOrdersController.$inject = ["HttpServer", "Personal", "$state", "EOrderService", "$sessionStorage","$scope"];

	function eOrdersController(HttpServer, Personal, $state, EOrderService, $sessionStorage, $scope) {
		var vm = this;
		vm.is_ready = false;
		vm.change_specimen = [];
		vm.test_selction_isActive = false;
		vm.order_confirmation_isActive = false;
		vm.sample_collection_isActive = false;
		$scope.$on('dataActive',function(data) {
			vm.test_selction_isActive = true;
		});
		$scope.$on('collectionActive',function(data) {
			vm.sample_collection_isActive = true;
		});
		$scope.$on('confirmation',function(data) {
			vm.order_confirmation_isActive = true;
		});
		vm.$onInit = function () {
			vm.active_step = $state.current.name
			vm.state_url = $state.current.name;
			if($sessionStorage.patient_data != null){
				vm.test_selction_isActive = true;
			}
			if($sessionStorage.patient_data && $sessionStorage.patient_data.tests != null){
				vm.sample_collection_isActive = true;
			}
			if($sessionStorage.patient_data && $sessionStorage.patient_data.tests != null && $sessionStorage.patient_data.order_data ){
				vm.order_confirmation_isActive = true;
			}
			HttpServer
				.get("v1/doctor/user")
				.response(function (data) {
					Personal.init(function (_data) {
            _data.doctor_code = data.doctor_code;
						angular.merge(_data, data.personal);
					});
				})
			if (vm.state_url == "eorders") {
				//service to call user permissions

				//pre checks if prefill data is need or not
				if ($state.params.id != 0) {
					//fetch patient_data by id
					HttpServer
						.post("v2/doctor/eorder/patient_data", { patient_id: $state.params.id })
						.response(function (data) {
							vm.is_ready = true;
							EOrderService.data.verify_patient_information = data;
						})
				} else {
					EOrderService.data.verify_patient_information = {
						contact: {},
						gender: {}
					}
					vm.is_ready = true;
				}
			} else {
				vm.is_ready = true;
			}
		}
		vm.changeSteps = function (stepName) {
		
			if ($sessionStorage.patient_data != null) {
				switch (stepName) {
					case 'eorders.test-selection':
						vm.active_step = stepName;
						$state.go(stepName);
						break;
					case 'eorders.confirmation':
						vm.active_step = stepName;
						$state.go(stepName);
						break;
					default :
						vm.active_step = stepName;

				}

			}
		}
	}
})();