(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("eorderSampleCollection",{
            templateUrl:"/dashboard/eorders/eorder-sample-collection/view.html",
            controller:"eorderSampleCollectionController"
        })
})();