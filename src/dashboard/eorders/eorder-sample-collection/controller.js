(function () {
    "use strict";

    angular
        .module("Biomark")
        .controller("eorderSampleCollectionController", eorderSampleCollectionController);

    eorderSampleCollectionController.$inject = ["Country", "HttpServer", "$sessionStorage", "$state", "$scope"];

    function eorderSampleCollectionController(Country, HttpServer, $sessionStorage, $state, $scope) {
        var vm = this;
        vm.patient_data = {};
        vm.tests = {}
        vm.dateShowFlag = false;
        vm.fasting = true;
        vm.nonFasting = false;
        vm.specimenList = [];
      
        // vm.specimenList = [{ "collect": false, "name": "Urine sample bottle", "color": "red", "min_volume": "300ml" },
        // { "collect": false, "name": "xyz", "color": "green", "min_volume": "300ml" },
        // { "collect": false, "name": "abc", "color": "yellow", "min_volume": "300ml" },
        // { "collect": false, "name": "Stool sample bottle", "color": "pink", "min_volume": "300ml" },
        // { "collect": false, "name": "EDTA 1", "color": "black", "min_volume": "300ml" },
        // { "collect": false, "name": "EDTA 2", "color": "white", "min_volume": "300ml" }];
        vm.swabList = []
        // vm.swabList = [{ "collect": false, "name": "Swabs #1", "color": "black", "inputSwab": "a" },
        // { "collect": false, "name": "Swabs #2", "color": "black", "inputSwab": "a" }]
        vm.openConfirm = false;
      
        vm.$onInit = function () {
          $scope.dateRangeStart = new Date();
          vm.patient_data = $sessionStorage.patient_data;
          vm.patient_data.order.note = '';
          vm.patient_data.mobile_number = "999999999";
          vm.patient_data.contact = "XYZ 123 456";
          vm.tests = vm.patient_data.order.packages;
          console.log(vm.patient_data.order)
          for (var i in vm.tests) {
            var test = vm.tests[i]
            console.log(test);
            for(var ii in test.specimen_types) {
              var spc_name = test.specimen_types[ii]
              vm.specimenList.push(
                { "collect": false, "name": spc_name, "color": "", "min_volume": test.requirement, "test_id": test.id}
              )
            }

            for(var ii in test.swab_type) {
              spc_name = test.swab_type[ii]
              vm.swabList.push(
                { "collect": false, "name": "Swabs #" + (vm.swabList.length+1), "color": "", "inputSwab": '', "test_id": test.id }
              )
            }
          }

          console.log(vm.specimenList)
          console.log(vm.swabList)
        }

        vm.checkCompleteContinue = function () {
            var totalSample = vm.specimenList.length + vm.swabList.length;
            var temp = 0;
            for (var spc in vm.specimenList) {
                console.log(vm.specimenList[spc] );
                if (vm.specimenList[spc].collect) {
                    temp++;
                }
            }
            for (let spc in vm.swabList) {
                if (vm.swabList[spc].collect) {
                    temp++;
                }
            }
          
            if(temp == totalSample){
                $state.go('eorders.confirmation');
                return;
            }
            vm.openConfirm = true;
            
        }
        vm.nextStep = function() {
            $scope.$emit('confirmation', true);
            $sessionStorage.patient_data.order_data = new Date();
            $sessionStorage.patient_data.order.specimenList = vm.specimenList;
            $sessionStorage.patient_data.order.swabList = vm.swabList;
            $state.go('eorders.confirmation');
        }
        vm.backStep = function() {
            $state.go('eorders.test-selection');
        }
    }
})();