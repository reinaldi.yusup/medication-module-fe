( function(){

	"use strict";

	angular
		.module("Biomark")
		.controller('eorderWorklistController',eorderWorklistController);

		eorderWorklistController.$inject=["HttpServer","Personal","$state", "$scope"];

		function eorderWorklistController(HttpServer,Personal, $state, $scope){
			var vm = this;
			vm.worklistData = {};
			vm.worklistDatas = [];

			vm.moveSampleCollection = function() {
				$scope.$emit('collectionActive', true);
				$state.go('eorders.sample-collection');
			}

            vm.$onInit = function(){
				HttpServer.get_v2("v2/doctor/order/get_order_worklist").then(
                    function (res) {
                        console.log(res);
						console.log(res.status);
                        if(res.status == 200) {
                            console.log("get_order_worklist");
							for(let i = 0; i< res.data.data_worklist.length; i++) {
								console.log("Data -> "+i);
								vm.worklistData = {
									test_entered_date_time: res.data.data_worklist[i].order.dateOfTest,
									first_name: res.data.data_worklist[i].patient.first_name,
									last_name: res.data.data_worklist[i].patient.last_name,
									ic_number: res.data.data_worklist[i].patient.ic_number,
									date_of_birth: res.data.data_worklist[i].patient.birth_date,
									test_to_be_obtained: res.data.data_worklist[i].test_to_be_obtained,
									status: res.data.data_worklist[i].order.order_status
								}
								console.log(vm.worklistData);
								vm.worklistDatas.push(vm.worklistData);
							}
                        }
                    }
                )  
                
            }

			// vm.search_order = function(keyword){
				
			// }
		}
})();

// vm.worklist.data = [
							// 	{
							// 		test_entered_date_time: res.data[0].order.dateOfTest,
							// 		first_name: res.data[0].patient.first_name,
							// 		last_name: res.data[0].patient.last_name,
							// 		ic_number: res.data[0].patient.ic_number,
							// 		date_of_birth: res.data[0].patient.birth_date,
							// 		test_to_be_obtained: "FBC, LFT, HbA1c, Iron Studies",
							// 		status: res.data[0].order.order_status
							// 	}
							// ]