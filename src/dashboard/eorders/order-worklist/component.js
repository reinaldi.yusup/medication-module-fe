( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('orderWorklist',{
			controller:"eorderWorklistController",
			templateUrl:"/dashboard/eorders/order-worklist/view.html"
		})
})();