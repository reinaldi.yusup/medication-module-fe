( function(){
	
	"use strict";


	angular
		.module("Biomark")
		.controller("dashboardController",dashboardController);

		dashboardController.$inject=["Dashboard","resolve_userinfo","Personal", "$state"];

		function dashboardController( Dashboard, resolve_userinfo, Personal, $state){
			
			resolve_userinfo.response( init );
		
			var dc = this;
			dc.is_ready = false;
			dc.personal = {};
			function init( res ){
				dc.is_ready = true;
				Personal.init( function( data ){
					dc.personal = angular.merge(data,res.personal);
					// /*** MERGE CONDITION ***/
					if(!res.personal.profile.status){
						data.profile.data = Personal.reset.profile.data;
					}
				});
				

				Personal.tabChanged = function( index ){
					dc.personal.tab_position = index;
				}


				Dashboard.init( function( data ){
          if (res.personal.quest.e_consent === true) {
            var permission = res.personal.permissions
            if (permission.patient !== undefined) {
              console.log(permission.patient )
            } else if (permission.booking !== undefined) {
              $state.go("bookings");
            } else if (permission.order !== undefined) {
              $state.go("eorders/0");
            } else if (permission.consumable !== undefined) {
              $state.go("consumable");
            } else {
              $state.go("settings");
            }
          }

					// console.log(data);
					dc.portal = data;
					dc.portal.first_login 			= res.first_login;
					dc.portal.has_patient 			= res.patients;
					dc.portal.has_patient_invites 	= res.patient_invites;
					dc.portal.page					= 1;
					dc.portal.is_master				= res.is_master;
				});
				
			}


			dc.close_reminder = function(){
				dc.portal.patient_reminder = false;
			}
			dc.remind = function(){
				dc.portal.patient_reminder = false;
				dc.portal.patient_reminder_sent = true;
			}
			dc.close_reminder = function(){
				dc.portal.patient_reminder = false;
				dc.portal.patient_reminder_sent = false;
			}
		}

})();