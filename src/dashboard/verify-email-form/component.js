( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('verifyEmailForm',{
			controller:"verifyEmailFormController",
			templateUrl:"/public/%PORTAL_TYPE%/dashboard/verify-email-form/view.html"
		})
})();