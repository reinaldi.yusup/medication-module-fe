( function(){

	"use strict";

	angular
		.module('Biomark')
        .controller('verifyEmailFormController',verifyEmailFormController);

        verifyEmailFormController.$inject = ["HttpServer","User", 'BiomarkConfig'];

        function verifyEmailFormController( HttpServer, User,BiomarkConfig ){

            var vm = this;
            vm.sent = false;
            vm.has_error = false;
            vm.user = {};
            var letterNumber = /^[\w\-\s]+$/;
            var capitalLetters = /^[A-Z0-9]+$/;
            var numbers = /^[0-9]+$/;
            var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

            vm.update_and_verify_email = function( user ){
                if( validate()){
                    HttpServer.post_v2("v2/doctor/setting/update_verify_email",{user:user}).then(
                        function(res){
                            if(res.data.status){
                                vm.has_error = false;
                                vm.sent = true;
                                User.logout()
                                User.set_token(0);
                            }else{
                                setState("email_address");
                                vm.email_error = res.data.message;
                            }
                            
                        },
                        function(err){
                            vm.has_error = true;
                            vm.error_message =err.data.message
                        }
                    );
                }
            }
            var state;
            //field validator
            function validate(){
                vm.$onInit();
                state = true;
                var mobile_config = BiomarkConfig.mobile_configs.filter(mc => mc.country_id === BiomarkConfig.current_country.id)[0]
                if(!vm.user.doctor_in_charge){
                    setState("doctor_in_charge");
                }else{
                    if (vm.user.doctor_in_charge.length < 5 || vm.user.doctor_in_charge.length > 50) {
                        setState("doctor_in_charge");
                    }
                    if (!vm.user.doctor_in_charge.match(letterNumber)) {
                        setState("doctor_in_charge");
                    }
                }
                if (!vm.user.clinic_mobile_number) {
                    setState("clinic_mobile_number");
                } else {
                    if (vm.user.clinic_mobile_number.length < mobile_config.mobile_min || vm.user.clinic_mobile_number.length > mobile_config.mobile_max ) {
                        setState("clinic_mobile_number");
                    }
                    if (!vm.user.clinic_mobile_number.match(numbers)) {
                        setState("clinic_mobile_number");
                    }
                }
                if (!vm.user.email_address) {
                    vm.email_error = "Please enter valid email address.";
                    setState("email_address");
                } else {
                    if(!vm.user.email_address.match(emailRegex)){
                        setState("email_address");
                        vm.email_error = "Please enter valid email address.";
                    }
                }
                return state;
            }
            //shorten value setter
            function setState(field){
                vm.fields[field] = true;
                state = false
            }
            vm.$onInit = function(){
                vm.fields = {
                    doctor_in_charge: false,
                    clinic_mobile_number: false,
                    doctor_name: false,
                    email_address: false
                }
            }

        }
})();