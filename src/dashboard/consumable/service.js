(function(){
    "use strict";

    angular
        .module("Biomark")
        .factory("ConsumableService",ConsumableService);

        ConsumableService.$inject = ["$localStorage", "HttpServer"];

        function ConsumableService( $localStorage, HttpServer){
            var f = {};
            f.data = {};
            f.clear = function(){
                $localStorage.consumable_data = {};
            }
            f.save =  function(){
                $localStorage.consumable_data = f.data;
                HttpServer
                    .post_v2("v2/doctor/consumable/update_cart", {order: f.data})
            }
            f.get_data = function(){
                return $localStorage.consumable_data || {};
            }
            f.get_cart = function(){
                return HttpServer.get_v2("v2/doctor/consumable/get_cart")
            }
            return f;
        }
})();