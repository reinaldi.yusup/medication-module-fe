( function(){
	
	"use strict";


	angular
		.module("Biomark")
		.controller("subHeaderController",subHeaderController);

		subHeaderController.$inject=["Personal","$timeout","$sessionStorage","DateDropdown"];

		function subHeaderController( Personal ,$timeout, $sessionStorage, DateDropdown){
			var vm = this;
			vm.$onInit = function(){
				Personal.init( function(data){
					vm.tab =  data;
					// if(!data.notification.status){
					// 	vm.tab.tab_position = 2;
					// }
					// if(!data.contact.status){
					// 	vm.tab.tab_position = 1;
					// }
					// if(!data.profile.status){
					// 	vm.tab.tab_position = 0;
					// }
					if(angular.isUndefined($sessionStorage.level)){
						vm.tab.tab_position = 0;
						$sessionStorage.level = vm.tab.tab_position;
					}else{
						vm.tab.tab_position = $sessionStorage.level 
					}
				});	
			}
			
			
			vm.select_tab = function( index ){
				
				// call listener
				if (Personal.validate() == false) {
					vm.validity = true;
					$timeout(function(){
						vm.validity = false;
					},3000);
					vm.error_message = false;
					return false;
				}
				

				if(index > $sessionStorage.level){
					vm.validity = true;
					$timeout(function(){
						vm.validity = false;
					},3000);
					vm.error_message = false;
					return false;
				}else{
					if (Personal.tabChanged) Personal.tabChanged( index )
					vm.tab.tab_position =  index;
				}
			}


			vm.navigations = [
				{
					id:0,
					label:"1. Personal",
					caption:"Tell us something about you",
					caption_description:""
				},
				{
					id:1,
					label:"2. Consent",
					caption:"Lastly, we need your consent to provide our services",
					caption_description:"Please read and provide your consent to the collection of your information according to the Data Privacy Policy below."
				}
				// {
				// 	id:2,
				// 	label:"Notification",
				// 	caption:"Let's notify your patients"
				// },
			]
			vm.close = function(){
				vm.validity = false;
			}
		}

})();