( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('biomarkSubHeader',{
			controller:"subHeaderController",
			templateUrl:"/dashboard/biomark-sub-header/view.html"
		})
})();