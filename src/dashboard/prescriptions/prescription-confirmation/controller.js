(function () {

	"use strict";

	angular
		.module("Biomark")
		.controller('prescriptionConfirmationController', prescriptionConfirmationController);

	prescriptionConfirmationController.$inject = ["HttpServer", "$sessionStorage", "$state", "$localStorage"];

	function prescriptionConfirmationController(HttpServer, $sessionStorage, $state, $localStorage) {
		var vm = this;
    vm.remarks = ''
		vm.medication_data = [];
		vm.durationTypes = [
			{
				id: 0,
				name: "Days"
			},
			{
				id: 1,
				name: "Weeks"
			},
			{
				id: 2,
				name: "Months"
			}
		]
    vm.mealOptions = [
			{
				id: 0,
				name: "Before Meal"
			},
			{
				id: 1,
				name: "After Meal"
			},
			{
				id: 2,
				name: "Before/After Meal"
			}
		]
		vm.$onInit = function () {
			vm.medication_data = $localStorage.medication_data;
      vm.prescription =  $localStorage.prescription_data;
      vm.patient = $sessionStorage.patient_data;
      vm.image_files = $localStorage.prescription_files;

		}
    vm.hideErrorPopup = function() {
      vm.has_error = false;
    }

		vm.back = function () {
			$state.go("prescription.medication");
		}

    vm.submit = function () {
      let payload = {
        patient_id: $sessionStorage.patient_data.user_id, remarks: vm.remarks,
        files: vm.image_files,
        medication: {
          allergies: vm.prescription.selectedAllergies.toString(), health_condition: vm.prescription.selectedConditions.toString(),
          allergies_remarks: vm.prescription.allergies_remarks, health_condition_remarks: vm.prescription.condition_remarks,
          list: vm.medication_data
        }
      }
      vm.send_data_to_api({prescription: payload})
    }

    vm.send_data_to_api = function(prescription_payload) {
      HttpServer.post_v2("v2/doctor/prescriptions", prescription_payload).then(
        function(res){
          console.log('response')
          console.log(res)
          console.log(res.status)
          if(res.status === 200){
            $localStorage.medication_data = null
			      $localStorage.prescription_data = null
            $sessionStorage.patient_data = null
			      $localStorage.prescription_files = null
            $state.go("prescription.list",{},{reload: "prescription"});
          }else{
            vm.has_error = true;
            vm.error_message = res.data.message
          }
        },
        function(err){
          console.log('error')
          console.log(err)
            vm.has_error = true;
            vm.error_message =  err.data.error
          }
      );
    }


	}
})();