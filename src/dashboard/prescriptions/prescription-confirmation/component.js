( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('prescriptionConfirmation',{
			controller:"prescriptionConfirmationController",
			templateUrl:"/dashboard/prescriptions/prescription-confirmation/view.html"
		})
})();
