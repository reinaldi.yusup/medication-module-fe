( function(){

	"use strict";

	angular
		.module("Biomark")
		.controller('prescriptionListController',prescriptionListController);

		prescriptionListController.$inject=["HttpServer","Personal","$state", "$scope"];

		function prescriptionListController(HttpServer,Personal, $state, $scope){
			var vm = this;
      vm.durationTypes = [
        {
          id: 0,
          name: "Days"
        },
        {
          id: 1,
          name: "Weeks"
        },
        {
          id: 2,
          name: "Months"
        }
      ]
      vm.prescriptions = [];
      vm.keyword = '';
      vm.$onInit = function(){
        loadPrescriptionData()
      }

      vm.search = function() {
        loadPrescriptionData()
      }

      function loadPrescriptionData(){
        HttpServer.get_v2("/v2/doctor/prescriptions?keyword=" + vm.keyword).then(function(res){
          vm.prescriptions = res.data.data
          console.log(vm.prescriptions)
        });
      }
		}
})();