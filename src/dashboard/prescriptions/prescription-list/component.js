( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('prescriptionList',{
			controller:"prescriptionListController",
			templateUrl:"/dashboard/prescriptions/prescription-list/view.html"
		})
})();