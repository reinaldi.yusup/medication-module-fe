( function(){

	"use strict";

	angular
		.module("Biomark")
        .factory("PrescriptionService",PrescriptionService);
        PrescriptionService.$inject = [];

        function PrescriptionService(){
            var f = {};
            f.data = {};
            return f;
        }
})();