(function () {

	"use strict";

	angular
		.module("Biomark")
		.controller('prescriptionMedicationController', prescriptionMedicationController)
		.directive('disableArrows', function () {
			function disableArrows(event) {
				if (event.keyCode === 38 || event.keyCode === 40) {
					event.preventDefault();
				}
			}
			return {
				link: function (scope, element, attrs) {
					element.on('keydown', disableArrows);
				}
			};
		});

	prescriptionMedicationController.$inject = ["HttpServer", "Personal", "$localStorage", "$state", "$scope", "TestService"];

	function prescriptionMedicationController(HttpServer, Personal, $localStorage, $state, $scope, TestService) {
		var vm = this;
		vm.medicine_data = [];
		vm.medicine_prescribed = [];
		vm.durationTypes = [
			{
				id: 0,
				name: "Days"
			},
			{
				id: 1,
				name: "Weeks"
			},
			{
				id: 2,
				name: "Months"
			}
		]
		vm.mealOptions = [
			{
				id: 0,
				name: "Before Meal"
			},
			{
				id: 1,
				name: "After Meal"
			},
			{
				id: 2,
				name: "Before/After Meal"
			}
		]
		vm.openAllergiesSuggestion = false;
		vm.openConditionSuggestion = false;
		vm.allergies = [];
		vm.conditions = [];
		vm.tempAllergies = "";
		vm.tempConditions = "";
		vm.allergiesValue = "";
		vm.conditionValue = "";
		vm.prescription = {
			selectedAllergies: [],
			selectedConditions: []
		};

		vm.prescription.selectedAllergies.forEach((value) => vm.allergiesValue = vm.diagnosisValue + value + ",");
		vm.prescription.selectedConditions.forEach((value) => vm.conditionValue = vm.diagnosisValue + value + ",");
		vm.allergiesJsonData = function () {
			TestService.getJsonData('drugs.json').then(function (res) {
				vm.allergies = res.data;
			})
		}
		vm.tabletChange = function (index, value) {
			vm.medicine_prescribed[index].morning = 0;
			vm.medicine_prescribed[index].afternoon = 0;
			vm.medicine_prescribed[index].evening = 0;
		}
		vm.conditionJsonData = function () {
			TestService.getJsonData('diagnosis.json').then(function (res) {
				vm.conditions = res.data;
			})
		}

		vm.filterAllergiesSearch = function () {
			vm.autoFillResult = [];
			for (let allergy of vm.allergies) {
				if (allergy.drug.toLowerCase().includes(vm.tempAllergies.toLowerCase())) {
					vm.autoFillResult.push(allergy.drug);
				}
			}
			vm.openAllergiesSuggestion = true;
			return vm.autoFillResult;
		}

		vm.filterConditionSearch = function () {
			vm.autoFillResult = [];
			for (let condition of vm.conditions) {
				if (condition.diagnosis.toLowerCase().includes(vm.tempConditions.toLowerCase())) {
					vm.autoFillResult.push(condition.diagnosis);
				}
			}
			vm.openConditionSuggestion = true;
			return vm.autoFillResult;
		}

		vm.addAllergy = function (allergy) {
			if (allergy.length > 1) {
				console.log(vm.pres)
				vm.prescription.selectedAllergies.push(allergy);
				var index = vm.allergies.findIndex(data => data.drug == allergy);

				if (index > -1) {
					vm.allergies.splice(index, 1);
				}
				vm.tempAllergies = '';
			}
		}
		vm.addCondition = function (condition) {
			if (condition.length > 1) {
				vm.prescription.selectedConditions.push(condition);
				var index = vm.conditions.findIndex(data => data.diagnosis == condition);

				if (index > -1) {
					vm.conditions.splice(index, 1);
				}
				console.log(vm.prescription.selectedConditions)
				vm.tempConditions = '';
			}
		}
		vm.openInput = function (inputId) {
			document.getElementById(inputId).focus();
		}

		vm.removeAllergies = function (allergy) {
			var index = vm.prescription.selectedAllergies.indexOf(allergy);
			if (index > -1) {
				vm.prescription.selectedAllergies.splice(index, 1);
				vm.allergiesJsonData();
			}
		}
		vm.removeCondition = function (condition) {
			var index = vm.prescription.selectedConditions.indexOf(condition);
			if (index > -1) {
				vm.prescription.selectedConditions.splice(index, 1);
				vm.conditionJsonData();
			}
		}
		vm.$onInit = function () {
			if ($localStorage.medication_data) {
				vm.medicine_prescribed = $localStorage.medication_data;
			}
			if ($localStorage.prescription_data) {
				vm.prescription = $localStorage.prescription_data;
			}
			vm.allergiesJsonData()
			vm.conditionJsonData()
      loadMedicineData();
		}

    function loadMedicineData() {
      HttpServer.get_v2("/v2/doctor/prescriptions/medicine_list").then(function(res){
        vm.medicines = res.data.data;
      });
    };

		vm.update = function (index, value) {
			vm.medicine_prescribed[index].duration_value = value;
		}

		vm.updateMeal = function (index, value) {
			vm.medicine_prescribed[index].eat_instruction = value;
		}

		vm.increment = function (index, key) {
			if (key === 'morning') {
				if (vm.medicine_prescribed[index].half_tablet) {
					if (vm.medicine_prescribed[index].morning == 0) {
						vm.medicine_prescribed[index].morning = 0.5;
					} else {
						vm.medicine_prescribed[index].morning++;
					}
				} else {
					vm.medicine_prescribed[index].morning++;
				}
			} else if (key === 'afternoon') {
				if (vm.medicine_prescribed[index].half_tablet) {
					if (vm.medicine_prescribed[index].afternoon == 0) {
						vm.medicine_prescribed[index].afternoon = 0.5;
					} else {
						vm.medicine_prescribed[index].afternoon++;
					}
				} else {
					vm.medicine_prescribed[index].afternoon++;
				}
			} else {
				if (vm.medicine_prescribed[index].half_tablet) {
					if (vm.medicine_prescribed[index].evening == 0) {
						vm.medicine_prescribed[index].evening = 0.5;
					} else {
						vm.medicine_prescribed[index].evening++;
					}
				} else {
					vm.medicine_prescribed[index].evening++;
				}
			}
		};
		vm.decrement = function (index, key) {
			if (vm.medicine_prescribed[index].morning > 0) {
				if (key === 'morning') {
					if (vm.medicine_prescribed[index].morning == 0.5) {
						vm.medicine_prescribed[index].morning = 0;
					} else {
						vm.medicine_prescribed[index].morning--;
					}
				}
			}
			if (vm.medicine_prescribed[index].afternoon > 0) {
				if (key === 'afternoon') {
					if (vm.medicine_prescribed[index].afternoon == 0.5) {
						vm.medicine_prescribed[index].afternoon = 0;
					} else {
						vm.medicine_prescribed[index].afternoon--;
					}
				}
			}
			if (vm.medicine_prescribed[index].evening > 0) {
				if (key === 'evening') {
					if (vm.medicine_prescribed[index].evening == 0.5) {
						vm.medicine_prescribed[index].evening = 0;
					} else {
						vm.medicine_prescribed[index].evening--;
					}
				}
			}
		};
		vm.search_result_dropdown = false;
		vm.search_medicine = function (medicineName) {
			// call api here and pass search str
      vm.medicine_data = vm.medicines;
			vm.searchFlag = false;
			vm.search_result_dropdown = false;
			if (medicineName.length <= 0) {
				vm.search_result_dropdown = false;
				vm.searchFlag = true;
				vm.medicine_data = null;
				vm.searchErrorMessage = "Please enter valid value";
			} else {
				const output = [];
				angular.forEach(vm.medicine_data, function (medicine) {
					if (medicine.medicine_name.toLowerCase().includes(medicineName.toLowerCase())) {
						output.push(medicine);
					}
				});
				vm.medicine_data = output;
				vm.search_result_dropdown = true
			}
		}
		vm.add_medicine = function (selected_medicine) {
			if (vm.medicine_prescribed.length > 0) {
				let medicine = _.find(vm.medicine_prescribed, { id: selected_medicine.id });
				if (_.isEmpty(medicine)) {
					vm.medicine_prescribed.push({
						id: selected_medicine.id,
						medicine_name: selected_medicine.medicine_name,
						morning: 0,
						afternoon: 0,
						evening: 0,
						half_tablet: false,
						eat_instruction: "",
						duration_count: 0,
						duration_value: ""
					})
				}
			} else {
				vm.medicine_prescribed.push({
					id: selected_medicine.id,
					medicine_name: selected_medicine.medicine_name,
					morning: 0,
					afternoon: 0,
					evening: 0,
					half_tablet: false,
					eat_instruction: "",
					duration_count: 0,
					duration_value: ""
				})
			}
			vm.search_result_dropdown = false;
			vm.searchStr = "";
		}

		vm.remove_medicine = function (selected_medicine) {
			_.remove(vm.medicine_prescribed, { id: selected_medicine.id })
		}

		vm.validate_data = function () {
			if (vm.medicine_prescribed.length > 0) {
				return false;
			}
			return true;
		};
		vm.back = function () {
			$state.go("prescription.new");
		}
		vm.submit_medication = function () {
			$localStorage.medication_data = vm.medicine_prescribed;
			$localStorage.prescription_data = vm.prescription;
			$state.go("prescription.confirmation");
		}
	}
})();
