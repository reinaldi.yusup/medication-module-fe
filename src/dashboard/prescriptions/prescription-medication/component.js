( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('prescriptionMedication',{
			controller:"prescriptionMedicationController",
			templateUrl:"/dashboard/prescriptions/prescription-medication/view.html"
		})
})();
