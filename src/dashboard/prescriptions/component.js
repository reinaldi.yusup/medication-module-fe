( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('prescription',{
			controller:"prescriptionController",
			templateUrl:"/dashboard/prescriptions/view.html"
		})
})();
