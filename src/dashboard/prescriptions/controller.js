(function () {

	"use strict";

	angular
		.module("Biomark")
		.controller('prescriptionController', prescriptionController);

	prescriptionController.$inject = ["HttpServer", "Personal", "$state", "$sessionStorage","$scope"];

	function prescriptionController(HttpServer, Personal, $state, $sessionStorage, $scope) {
    var vm = this;
		vm.$onInit = function () {
			vm.state_url = $state.current.name;
      HttpServer
				.get("v1/doctor/user")
				.response(function (data) {
					Personal.init(function (_data) {
            _data.doctor_code = data.doctor_code;
						angular.merge(_data, data.personal);
					});
				})
		}
	}
})();