( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('prescriptionNew',{
			controller:"prescriptionNewController",
			templateUrl:"/dashboard/prescriptions/prescription-new/view.html"
		})
})();