(function () {

	"use strict";

	angular
		.module("Biomark")
		.controller('prescriptionNewController', prescriptionNewController);

	prescriptionNewController.$inject = ["HttpServer", "Personal", "$state", "$sessionStorage","$scope"];

	function prescriptionNewController(HttpServer, Personal, $state, $sessionStorage, $scope) {
    var vm = this;
    vm.is_creation = true;
		vm.$onInit = function () {
      HttpServer
        .get("v1/doctor/user")
        .response(function (data) {
          Personal.init(function (_data) {
            _data.doctor_code = data.doctor_code;
            angular.merge(_data, data.personal);
          });
        })
	  	}
	  }
})();