(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("prescriptionPatientSearch",{
            templateUrl:"/dashboard/eorders/eorder-patient-search/view.html",
            controller:"prescriptionPatientSearchController"
        })
})();