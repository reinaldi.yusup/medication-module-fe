(function(){
    "use strict";

    angular
        .module("Biomark")
        .controller("prescriptionPatientSearchController",prescriptionPatientSearchController);

        prescriptionPatientSearchController.$inject = ["Country","HttpServer","EOrderService", "DateService", "PopupModalService", "$sessionStorage"];

        function prescriptionPatientSearchController(Country,HttpServer, EOrderService, DateService, PopupModalService, $sessionStorage){
            var vm = this;

            vm.patient_selected = false;
            vm.search_result_dropdown = false;
            vm.open_add_patient_modal = function(){
                vm.add_patient_modal = true;
            }
            vm.close_add_patient_modal = function(){
                vm.add_patient_modal = false;
            }
            vm.$onInit = function() {
                if($sessionStorage.patient_data != null){
                    vm.patient_selected = true;
                }
            }
            vm.search_patient = function( str ){
                vm.searchFlag = false;
                vm.search_result_dropdown = false;
                let search_regex = new RegExp("^[0-9a-zA-Z-\/@ ]{0,50}$");
                if(!search_regex.test(str)) {
                  vm.search_result_dropdown = false;
                  vm.searchFlag = true;
                  vm.searchErrorMessage = "Please enter valid value";
                }
                else {
                  HttpServer.cancel_request();
                  var keywords = str ? "" : "keywords="+str;
                  HttpServer
                    .post_with_abort("v2/doctor/eorder/search_patient",{keywords:str})
                    .then(function(res){
                        vm.search_data = res.data;
                        vm.search_result_dropdown = true;
                    },function(){
                  });
                }

            }
            vm.showModal = function(){
              PopupModalService.notification_modal_state(true, {})
              vm.hide_search_result();
            }
            vm.load_patient_info = function(patient_data){
                if(!patient_data.is_account_active){
                    return false;
                }
                vm.search_result_dropdown = false;
                vm.patient_selected = true;
                vm.searchStr = "";
                EOrderService.verify_information(patient_data)
            }

            vm.showAddPatientFormModal = function() {
                PopupModalService.patient_form_modal_state(true, { isEdit : false })
            }

        }   
})();