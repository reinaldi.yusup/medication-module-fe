(function(){
    "use strict";

    angular
        .module("Biomark")
        .controller("prescriptionAttachImageController",prescriptionAttachImageController);

        prescriptionAttachImageController.$inject = ["$sessionStorage", "$localStorage", "$timeout"];

        function prescriptionAttachImageController($sessionStorage, $localStorage, $timeout){
            var vm = this;
		        vm.image_files = [];
            vm.file = {}

            vm.$onInit = function() {
              if($sessionStorage.patient_data != null){
                vm.patient = $sessionStorage.patient_data ;
              }

              if($localStorage.prescription_files != null){
                vm.image_files = $localStorage.prescription_files;
              }
            }

            vm.addImages = function() {
              $timeout( function(){
                if(vm.image_files.some(file => file.base64 === vm.file.base64)){
                  console.log("same file found")
                } else{
                  vm.image_files.push(vm.file)
                  $localStorage.prescription_files = vm.image_files;
                }
              },1000);

            }
        }
})();