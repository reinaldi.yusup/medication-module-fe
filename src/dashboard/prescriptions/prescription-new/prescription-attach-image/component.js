(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("prescriptionAttachImage",{
            templateUrl:"/dashboard/prescriptions/prescription-new/prescription-attach-image/view.html",
            controller:"prescriptionAttachImageController"
        })
})();