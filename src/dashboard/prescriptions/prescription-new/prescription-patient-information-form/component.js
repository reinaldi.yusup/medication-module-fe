(function(){
    "use strict";

    angular
        .module("Biomark")
        .component("prescriptionPatientInformationForm",{
            templateUrl:"/dashboard/prescriptions/prescription-new/prescription-patient-information-form/view.html",
            controller:"prescriptionPatientInformationFormController"
        })
})();