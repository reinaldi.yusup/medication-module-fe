(function () {
  "use strict";

  angular
    .module("Biomark")
    .controller("prescriptionPatientInformationFormController", prescriptionPatientInformationFormController);

  prescriptionPatientInformationFormController.$inject = ["HttpServer", "EOrderService", "$state", "$sessionStorage", "$scope", "BiomarkConfig", "PopupModalService"];

  function prescriptionPatientInformationFormController(HttpServer, EOrderService, $state, $sessionStorage, $scope, BiomarkConfig, PopupModalService) {
    var vm = this;
    vm.lock_form = true;
    vm.selected_data = false;
    vm.patient_data = {};
    vm.correct_data = false;
    vm.edit_patient = false;
    vm.selected_data1 = false;
    var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

    vm.add_patient_modal = false;
    vm.continue = function () {
      $scope.$emit("dataActive", true);
      $state.go("prescription.medication");
    }
    vm.correct = function () {
      vm.correct_data = true;
      vm.selected_data1 = true;
    }
    vm.openEditModal = function () {
      vm.add_patient_modal = true;
    }


    vm.loadPatientData = function (data) {
      console.log(data);
      vm.patient_data.patient_name = data.patient_name;
      vm.patient_data.country_id = data.country_id;
      vm.patient_data.age = data.calc_birthday;
      vm.patient_data.birth_date = data.birth_date;
      vm.patient_data.picture = data.picture;
      vm.patient_data.gender = data.gender.name;
      vm.selected_data = true;
      vm.patient_data.ic = data.ic_number;
      vm.patient_data.ethinic = data.ethnic;
      vm.patient_data.country_name = data.country_name;
      // vm.patient_data.ethinic = "India";
      vm.getCode(vm.patient_data.country_name);
      // vm.patientData = {};


      $sessionStorage.patient_data = data;
      // let date = new Date(vm.patient_data.birth_date);
      // vm.patient_data.birth_date =  date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();

      if (data.contact) {
        vm.patient_data.contact = {
          email_address: data.contact.email_address,
          mobile: data.contact.mobile
        }
      }
      vm.getPatientData();
    }

    vm.getPatientData = function () {
      vm.patient_id = $sessionStorage.patient_data.user_id;
      console.log("vm.patient_id -> "+vm.patient_id);
      HttpServer
        .get_v2('v2/doctor/patient/'+vm.patient_id+'/detail/')
        .then( success , error);
        function success( res ){
          console.log(res);
          vm.patientData = res.data.demographic;
        }
        function error(err) {
          console.log(err);
        }
    }

    vm.$onInit = function () {
      console.log("Verify patient information");
      EOrderService.verify_information = function (data) {
        console.log("verify_information Data ------>");
        console.log(data);
        vm.loadPatientData(data);
        // vm.getPatientData();
      }

      EOrderService.verify_new_information = function (data) {
        console.log("new patient data in verify-->", data);
      }

      if ($sessionStorage.patient_data != null) {
        vm.getPatientData();
        vm.loadPatientData($sessionStorage.patient_data);
        // console.log($sessionStorage.patient_data);
      }

    };
    vm.openEditForm = function () {
      // vm.editFlag = "true";
      PopupModalService.patient_form_modal_state(true, { isEdit : true })

    }

    vm.getCode = function (country) {
      if (country != null) {
        EOrderService.getCountryCode(country).then(res => {
          vm.country_code = res.data.filter(data => data.Country.toLowerCase() === country.toLowerCase()).map(data => data.Code);
          console.log(vm.country_code);
        });
      }
    }


  }
})();