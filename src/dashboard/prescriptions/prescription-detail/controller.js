(function () {

	"use strict";

	angular
		.module("Biomark")
		.controller('prescriptionDetailController', prescriptionDetailController);

	prescriptionDetailController.$inject = ["HttpServer", "$state", "$sce", "$window"];

	function prescriptionDetailController(HttpServer, $state, $sce, $window) {
		var vm = this;
    vm.remarks = ''
		vm.medication_data = [];
    vm.image_files = []
		vm.durationTypes = [
			{
				id: 0,
				name: "Days"
			},
			{
				id: 1,
				name: "Weeks"
			},
			{
				id: 2,
				name: "Months"
			}
		]
    vm.mealOptions = [
			{
				id: 0,
				name: "Before Meal"
			},
			{
				id: 1,
				name: "After Meal"
			},
			{
				id: 2,
				name: "Before/After Meal"
			}
		]
		vm.$onInit = function () {
      HttpServer.get_v2("/v2/doctor/prescriptions/" + $state.params.id).then(function(res){
        vm.patient = res.data.data.patient_details;
        vm.prescription =  res.data.data.prescription;
			  vm.medications = vm.prescription.medications;
        vm.image_files = vm.prescription.images
      })
		}

    vm.fileLoad = function() {
      $window.document.title = 'BioMark';
    }

    vm.trustSrc = function(src) {
      return $sce.trustAsResourceUrl(src);
    }

    vm.close_modal = function(){
      vm.view_file_modal = false;
    }

    vm.view_file = function(file){
      vm.file = file
      vm.view_file_modal = true;
      vm.is_ready = true;
      vm.is_pdf = file.url.includes(".pdf")
      if (vm.is_pdf == true) {
        vm.file_url = vm.trustSrc(file.url);
      }
    }
	}
})();