( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('prescriptionDetail',{
			controller:"prescriptionDetailController",
			templateUrl:"/dashboard/prescriptions/prescription-detail/view.html"
		})
})();
