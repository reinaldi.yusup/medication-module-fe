(function () {

  "use strict";

  angular
    .module("Biomark")
    .controller('prescriptionViewFileController', prescriptionViewFileController)

  prescriptionViewFileController.$inject = ["$sce", "$window"];

  function prescriptionViewFileController($sce, $window) {
    var vm = this;
    vm.file = {};
    vm.file_url = '';
    this.$onInit = function () {}
  
  
    vm.fileLoad = function() {
      $window.document.title = 'BioMark';
    }

    vm.trustSrc = function(src) {
      return $sce.trustAsResourceUrl(src);
    }

    vm.close_modal = function(){
      vm.view_file_modal = false;
    }

    vm.view_file = function(file){
      vm.file = file
      vm.view_file_modal = true;
      vm.is_ready = true;
      vm.is_pdf = file.url.includes(".pdf")
      if (vm.is_pdf == true) {
        vm.file_url = vm.trustSrc(file.url);
      }
    }
  }
})();