( function(){

	"use strict";

	angular
		.module("Biomark")
		.component('prescriptionViewFile',{
			bindings:{
				fileurl: '=',
				file: '=',
			},
			controller:"prescriptionViewFileController",
			templateUrl:"/dashboard/prescriptions/prescription-view-file/view.html"
		})
})();