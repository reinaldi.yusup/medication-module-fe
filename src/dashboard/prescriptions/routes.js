( function(){

	"use strict";

	angular
		.module('Biomark')
		.config(['$stateProvider','$urlRouterProvider','$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
	
    		$urlRouterProvider.otherwise('login');

    		$stateProvider
                .state("prescription",{
                    url:"/prescription",
                    template:"<prescription></prescription>",
                })
                .state("prescription.list",{
                    url:"/list",
                    views:{
                        'prescription-main-view':{
                            template:"<prescription-list></prescription-list>",
                        }
                    }
                })
                .state("prescription.detail",{
                  url:"/:id",
                  views:{
                      'prescription-main-view':{
                          template:"<prescription-detail></prescription-detail>",
                      }
                  }
              })
                .state("prescription.new",{
                    url:"/new",
                    // template:"<prescription-new></prescription-new>",
                    views:{
                        'prescription-main-view':{
                          template:"<prescription-new></prescription-new>",
                        }
                    }
                })
                .state("prescription.medication",{
                  url:"/medication",
                  views:{
                      'prescription-main-view':{
                        template:"<prescription-medication></prescription-medication>",
                      }
                  }
                })
                .state("prescription.confirmation",{
                  url:"/confirmation",
                  views:{
                      'prescription-main-view':{
                        template:"<prescription-confirmation></prescription-confirmation>",
                      }
                  }
                })
                .state("prescription.summary",{
                  url:"/summary",
                  views:{
                      'prescription-child-view':{
                        template:"<prescription-summary></prescription-summary>",
                      }
                  }
                })
		}])
})();

