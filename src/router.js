( function(){

	"use strict";

	angular
		.module('Biomark')
		.config(['$stateProvider','$urlRouterProvider','$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
			
			// var environment = "%ENVIRONMENT%";
			// if(environment =='development'){
			// 	$locationProvider.html5Mode(false).hashPrefix('');
			// }else{
			// 	$locationProvider.html5Mode(true);
			// }

    		$urlRouterProvider.otherwise('login');

    		$stateProvider
				.state("verify",{
					url:"/verify/:code",
					controllerAs:'vc',
		            controller:'verifyController',
		            templateUrl:"/views/verify.html"
		        })
		        .state("dashboard",{
		            url:"/dashboard?:keywords&:sort_by&:child_user_id",
		            controller:"dashboardController",
		            controllerAs:"dc",
		            templateUrl:"/dashboard/view.html",
		            resolve:{
		            	resolve_userinfo: function( HttpServer ){
		            		return HttpServer.get("v1/doctor/user");
		            	}
					}
				})
				.state("dashboard.for-review",{
		            url:"/for-review/:page",
		            template:"<patient-for-review></patient-for-review>"
				})
				.state("dashboard.pending-connection",{
		            url:"/pending-connection/:page",
		            template:"<patient-pending-connection><patient-pending-connection>"
				})
				.state("dashboard.all-patient",{
		            url:"/all-patient/:page",
		            template:"<all-patient><all-patient>"
				})
				.state("dashboard.pending-result",{
		            url:"/pending-result/:page",
		            template:"<patient-pending-result><patient-pending-result>"
		        })
		        .state("login",{
		        	url:"/login",
		        	controller:"loginController",
		        	controllerAs:"lc",
		        	templateUrl:"/public/%PORTAL_TYPE%/login/view.html",
		        	resolve:{
		        		userstate:function($sessionStorage, $state ){
		        			if($sessionStorage.biomark_token != 0){
		        				$state.go("dashboard");
		        			}
		        		}
		        	}
		        })
		        .state("register",{
		        	url:"/register",
		        	template:"<doctor-register></doctor-register>",
		        	resolve:{
		        		userstate:function($sessionStorage, $state ){
		        			if($sessionStorage.biomark_token != 0){
		        				$state.go("dashboard");
		        			}
		        		}
		        	}
		        })
				.state("update-password",{
		        	url:"/update-password/:token",
		        	template:"<update-password></update-password>",
		        })

		        .state("forgot",{
		        	url:"/forgot-password",
					template:"<forgot-password-form></forgot-password-form>"
		        	// controller:"forgotController",
		        	// controllerAs:"fc",
		        	// templateUrl:"/views/forgot-password.html"
		        })
		        .state("patient",{
		            url:"/patient/:id",
		            controller:"patientController",
		            controllerAs:"pc",
		            templateUrl:"/views/patient.html",
		            resolve:{
		            	resolve_userinfo: function( HttpServer ){
		            		return HttpServer.get("v1/doctor/user");
		            	}
					},
					params:{
						page: null,
						tab: null,
					}
				})
				.state("patient.biomarker",{
					url:"/biomarker/:code",
					controller:"patientBiomarkerController",
					controllerAs:"bc",
					templateUrl:"/dashboard/patients/biomarker/view.html"	
				})
				.state("patient.result-summary",{
					url:"/result-summary",
					views:{
						'patient':{
							template:'<result-summary></result-summary>'
						}
					}
		            // templateUrl:"/dashboard/patients/medical-history/view.html",
				})
				.state("patient.new-order",{
					url:"/new-order",
					views:{
						'patient':{
							template:"<eorders-test-selection></eorders-test-selection>",
						}
					}
		            // templateUrl:"/dashboard/patients/medical-history/view.html",
				})
				.state("consumable",{
					url:"/consumable",
					template:"<consumable></consumable>",
				})
				.state("consumable.cart",{
					url:"/cart",
					template:"<consumable-view-cart></consumable-view-cart>",
				})
				.state("consumable.order-history",{
					url:"/order-history",
					template:"<consumable-order-history></consumable-order-history>",
				})
				.state("consumable.order-history-detail",{
					url:"/detail/:id",
					template:"<consumable-order-history-detail></consumable-order-history-detail>",
				})
				
				.state("patient.orders",{
		            url:"/orders",
					views:{
						message:{
							controller:"biomarkOrdersController",
							controllerAs:"vm",
							templateUrl:"/dashboard/orders/view.html",
						}
					}
					
				})
				.state("patient.orders.review",{
		            url:"/review",
					controller:"orderReviewController",
					controllerAs:"vm",
					templateUrl:"/dashboard/orders/review/view.html",
				})
				.state("patient.orders.view-order",{
		            url:"/view-order/:order_id",
					controller:"orderViewController",
					controllerAs:"vm",
					templateUrl:"/dashboard/orders/view-order/view.html",
				})
				.state("patient.orders.history",{
		            url:"/history",
					controller:"orderHistoryController",
					controllerAs:"vm",
					templateUrl:"/dashboard/orders/history/view.html",
				})
				.state("patient.message",{
					url:"/message",
					views:{
						'message':{
							template:'<message-history></message-history>'
						}
					}
		            // templateUrl:"/dashboard/patients/medical-history/view.html",
				})
				.state("patient.medical-history",{
					url:"/medical-history",
					template:'<medical-history></medical-history>'
		            // templateUrl:"/dashboard/patients/medical-history/view.html",
				})
				.state("patient.family-history",{
					url:"/family-history",
					template:'<family-history></family-history>'
		            // templateUrl:"/dashboard/patients/family-history/view.html",
				})
        // .state("patient.update-profile",{
				// 	url:"/update-profile",
				// 	views:{
				// 		'patient':{
				// 			template:'<update-profile></update-profile>'
				// 		}
				// 	}})
        .state("patient.update-profile",{
					url:"/update-profile",
					template:'<update-profile></update-profile>'
		            // templateUrl:"/dashboard/patients/family-history/view.html",
				})
				.state("patient.clinical-notes",{
					url:"/clinical-notes",
					template:'<clinical-notes></clinical-notes>'
				})
				.state("patient.diabetes-report",{
		            url:"/diabetes-report",
		            template:"<diabetes-report></diabetes-report>",
				})
				.state("supported-biomarkers",{
		            url:"/supported-biomarkers",
		            controller:"supportedBiomarkersController",
		            controllerAs:"vm",
					templateUrl:"/dashboard/supported-biomarkers/view.html",
					resolve:{
		            	resolve_userinfo: function( HttpServer ){
		            		return HttpServer.get("v1/doctor/user");
		            	}
		            }
				})
				.state("patient.health-summary",{
					url:"/health-summary",
					template:'<health-summary></health-summary>'
		            // templateUrl:"/dashboard/patients/health-summary/view.html",
		        })
		        .state("biomarker",{
		            url:"/biomarker/:id/:code",
		            controller:"biomarkerController",
		            controllerAs:"bc",
		            templateUrl:"/views/biomarker.html",
		            resolve:{
		            	resolve_userinfo: function( HttpServer ){
		            		return HttpServer.get("v1/doctor/user");
		            	}
					},
					params:{
						code: null,
						name: null
					}
		        })
		        .state("help-center",{
		            url:"/help-center",
		            // controller:"helpCenterController",
		            // controllerAs:"hc",
					templateUrl:"/views/help-center.html",
					resolve:{
		            	resolve_userinfo: function( HttpServer ){
		            		return HttpServer.get("v1/doctor/user");
		            	}
		            }
		        })
		        .state("report-bug",{
		            url:"/report-bug",
		            controller:"reportBugController",
		            controllerAs:"rb",
					templateUrl:"/views/report-bug.html",
					resolve:{
		            	resolve_userinfo: function( HttpServer ){
		            		return HttpServer.get("v1/doctor/user");
		            	}
		            }
		        })
		        .state("about-us",{
		            url:"/about-us",
		            //controller:"helpCenterController",
		            //controllerAs:"hc",
		            templateUrl:"/views/about-us.html"
		        })
		        .state("policies",{
		            url:"/policies/:country",
		            controller:"policiesController",
		            controllerAs:"hc",
					templateUrl:"/views/policies.html",
					resolve:{
		            	resolve_userinfo: function( HttpServer ){
		            		return HttpServer.get("v1/doctor/user");
		            	}
		            }
				})
				.state("login-policies",{
		            url:"/terms",
					template:"<biomark-policies></biomark-policies>",
		        })
		        .state("settings",{
		            url:"/settings",
		            controller:"settingController",
		            controllerAs:"sc",
		            templateUrl:"/views/settings.html",
		            resolve:{
		            	resolve_userinfo: function( HttpServer ){
		            		return HttpServer.get("v1/doctor/user");
		            	}
		            }
		        })
		        .state("notifications",{
		        	url:"/notifications",
		        	templateUrl:"/views/notifications.html"
		        })
		        .state("profile",{
		        	url:"/profile",
		        	controller:'profileController',
		        	controllerAs:'pc',
		        	templateUrl:"/views/profile.html",
		        	resolve:{
		            	resolve_userinfo: function( HttpServer ){
		            		return HttpServer.get("v1/doctor/user");
		            	}
		            }
		        })
				
		}])

	
})();