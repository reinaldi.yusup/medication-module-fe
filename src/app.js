( function(){

	"use strict";

	angular
		.module('Biomark',[
			'angularMoment',
			'ui.router',
			'ngStorage',
			'naif.base64',
			'ja.qr',
			// 'btford.socket-io',
			'ui.mask',
			'ngActionCable',
			'ngIdle',
			'pdfjsViewer',
			"ui.bootstrap.datetimepicker",
			"ui.bootstrap",
			"ngTagsInput",
			"daterangepicker"
		]).constant('_', window._)
		.run(["ActionCableConfig","BiomarkConfig","HttpServer",function (ActionCableConfig, BiomarkConfig, HttpServer){
			ActionCableConfig.wsUri= BiomarkConfig.socket;
      var portal_type = BiomarkConfig.portal_type_id

			HttpServer.get_v2("meta_data?portal_type="+ portal_type).then(function(res){
				BiomarkConfig.tos = res.data.tos;
				BiomarkConfig.privacy = res.data.privacy;
				if(BiomarkConfig.meta_listener) BiomarkConfig.meta_listener();
			})
		}])
		.filter("trust", ['$sce', function($sce) {
            return function(htmlCode){
              return $sce.trustAsHtml(htmlCode);
            }
		}])
		.filter("escape", function() {
			return window.encodeURIComponent;
		})
		.config(["IdleProvider","KeepaliveProvider",function(IdleProvider, KeepaliveProvider) {
			// configure Idle settings
			IdleProvider.idle(15 * 60); // in seconds
			IdleProvider.timeout(15 * 60); // in seconds
			// KeepaliveProvider.interval(2); // in seconds
		}])
		.config(function(pdfjsViewerConfigProvider) {
			pdfjsViewerConfigProvider.setWorkerSrc("/lib/pdf.worker.js");
			pdfjsViewerConfigProvider.setImageDir("/lib/images/");
			
			pdfjsViewerConfigProvider.disableWorker();
			pdfjsViewerConfigProvider.setVerbosity("infos");  // "errors", "warnings" or "infos"
		})
		.run(["$rootScope","$state","User","$sessionStorage","Personal",function($rootScope, $state, User, $sessionStorage,Personal) {
			$rootScope._ = window._;
			$rootScope.$on('IdleTimeout', function() {
				if(Personal.data.profile.auto_logout){
					User.logout()
					User.set_token(0);
					$sessionStorage.contact = {}; $sessionStorage.level = 0; $sessionStorage.personal = {country_id: 1}; $sessionStorage.order_history = {};
					$state.go("login");
				}
			});
		}])
		.run(function(Idle){
			// start watching when the app runs. also starts the Keepalive service by default.
			Idle.watch();
		});
		
})();